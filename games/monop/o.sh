#!/bin/sh
ROOT="`pwd |sed -e 's/\/games\/monop$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
MANROFF="nroff -man"
XSTR="$ROOT/cross/usr/ucb/xstr"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" MANROFF="$MANROFF" XSTR="$XSTR" STAGEDIR="$ROOT/stage" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
