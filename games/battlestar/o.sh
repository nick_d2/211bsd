#!/bin/sh
ROOT="`pwd |sed -e 's/\/games\/battlestar$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
MANROFF="nroff -man"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" MANROFF="$MANROFF" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
