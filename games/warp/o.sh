#!/bin/sh
ROOT="`pwd |sed -e 's/\/games\/warp$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
LD="$ROOT/cross/bin/ld"
MANROFF="nroff -man"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" LD="$LD" MANROFF="$MANROFF" STAGEDIR="$ROOT/stage" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
