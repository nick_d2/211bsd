#!/bin/sh
ROOT="`pwd |sed -e 's/\/games\/boggle$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" STAGEDIR="$ROOT/stage" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
