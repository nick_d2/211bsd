#!/bin/sh
ROOT="`pwd |sed -e 's/\/games\/pdp\/zork$//'`"
AS="$ROOT/cross/bin/as"
CC="$ROOT/cross/bin/cc"
CPP="$ROOT/cross/lib/cpp"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
LD="$ROOT/cross/bin/ld"
MANROFF="nroff -man"
NM="$ROOT/cross/bin/nm"
mkdir --parents "$ROOT/stage/lib"
make AS="$AS" CC="$CC" CPP="$CPP" LD="$LD" MANROFF="$MANROFF" NM="$NM" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
