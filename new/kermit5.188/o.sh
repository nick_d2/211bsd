#!/bin/sh
ROOT="`pwd |sed -e 's/\/new\/kermit5.188$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
MKSTR="$ROOT/cross/usr/ucb/mkstr"
MANROFF="nroff -man"
XSTR="$ROOT/cross/usr/ucb/xstr"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" MKSTR="$MKSTR" MANROFF="$MANROFF" XSTR="$XSTR" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
