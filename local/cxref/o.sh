#!/bin/sh
ROOT="`pwd |sed -e 's/\/local\/cxref$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
LEX="$ROOT/cross/usr/bin/lex"
MANROFF="nroff -man"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" LEX="$LEX" MANROFF="$MANROFF" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
