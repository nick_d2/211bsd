#!/bin/sh
ROOT="`pwd |sed -e 's/\/usr\.lib\/libvmf$//'`"
HOSTCC="cc -g -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib/ -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
INSTALL="$ROOT/scripts/install.sh --strip-program=/bin/true"
MANROFF="nroff -man"
mkdir --parents "$ROOT/cross/usr/lib"
mkdir --parents "$ROOT/cross/usr/man/cat3"
make CC="$HOSTCC" MANROFF="$MANROFF" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
