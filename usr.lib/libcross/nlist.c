/*
 * Copyright (c) 1989 The Regents of the University of California.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms are permitted
 * provided that: (1) source distributions retain this entire copyright
 * notice and comment, and (2) distributions including binaries display
 * the following acknowledgement:  ``This product includes software
 * developed by the University of California, Berkeley and its contributors''
 * in the documentation or other materials provided with the distribution
 * and in all advertising materials mentioning features or use of this
 * software. Neither the name of the University nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 * THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)nlist.c	5.7.1 (2.11BSD GTE) 12/31/93";
#endif

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_nlist nlist
#define cross_off_t off_t
#define cross_xexec xexec
#endif

#ifdef CROSS
#define N_NAME(sp) (strtab + (sp)->n_un.n_strx)
#define ISVALID(sp) ((sp)->n_un.n_strx && strtab[(sp)->n_un.n_strx])
#else
#define N_NAME(sp) ((sp)->n_un.n_name)
#define ISVALID(sp) ((sp)->n_un.n_name && (sp)->n_un.n_name[0])
#endif

#ifdef CROSS
int cross_nlist(name, list, strtab) char *name; struct cross_nlist *list; char *strtab;
#else
int cross_nlist(name, list) char *name; struct cross_nlist *list;
#endif
{
	register struct cross_nlist *p, *s;
	struct cross_xexec ebuf;
#ifdef CROSS
	int i;
#endif
	FILE *fstr, *fsym;
	struct cross_nlist nbuf;
	cross_off_t strings_offset, symbol_offset, symbol_size;
	int entries, len, maxlen;
	char sbuf[128];

	entries = -1;

	if (!(fsym = fopen(name, "r")))
		return(-1);
	if (fread((char *)&ebuf, sizeof(ebuf), 1, fsym) != 1)
		goto done1;
#ifdef CROSS
	ebuf.e.a_magic = cross_read_int((char *)&ebuf.e.a_magic);
	ebuf.e.a_text = cross_read_int((char *)&ebuf.e.a_text);
	ebuf.e.a_data = cross_read_int((char *)&ebuf.e.a_data);
	ebuf.e.a_bss = cross_read_int((char *)&ebuf.e.a_bss);
	ebuf.e.a_syms = cross_read_int((char *)&ebuf.e.a_syms);
	ebuf.e.a_entry = cross_read_int((char *)&ebuf.e.a_entry);
	ebuf.e.a_unused = cross_read_int((char *)&ebuf.e.a_unused);
	ebuf.e.a_flag = cross_read_int((char *)&ebuf.e.a_flag);
	ebuf.o.max_ovl = cross_read_int((char *)&ebuf.o.max_ovl);
	for (i = 0; i < NOVL; ++i)
		ebuf.o.ov_siz[i] = cross_read_int((char *)(ebuf.o.ov_siz + i));
#endif
	if (N_BADMAG(ebuf.e))
		goto done1;

	symbol_offset = N_SYMOFF(ebuf);
	symbol_size = ebuf.e.a_syms;
	strings_offset = N_STROFF(ebuf);
	if (fseek(fsym, (off_t)symbol_offset, L_SET))
		goto done1;

	if (!(fstr = fopen(name, "r")))
		goto done1;

	/*
	 * clean out any left-over information for all valid entries.
	 * Type and value defined to be 0 if not found; historical
	 * versions cleared other and desc as well.  Also figure out
	 * the largest string length so don't read any more of the
	 * string table than we have to.
	 */
	for (p = list, entries = maxlen = 0; ISVALID(p); ++p, ++entries) {
		p->n_type = 0;
		p->n_ovly = 0;
		p->n_value = 0;
		if ((len = strlen(N_NAME(p))) > maxlen)
			maxlen = len;
	}
	if (++maxlen > sizeof(sbuf)) {		/* for the NULL */
		(void)fprintf(stderr, "nlist: sym 2 big\n");
		entries = -1;
		goto done2;
	}

	for (s = &nbuf; symbol_size; symbol_size -= sizeof(struct cross_nlist)) {
		if (fread((char *)s, sizeof(struct cross_nlist), 1, fsym) != 1)
			goto done2;
#ifdef CROSS
		s->n_un.n_strx = cross_read_long((char *)&s->n_un.n_strx);
		s->n_value = cross_read_int((char *)&s->n_value);
#endif
		if (!s->n_un.n_strx)
			continue;
		if (fseek(fstr, (off_t)(strings_offset + s->n_un.n_strx), L_SET))
			goto done2;
		(void)fread(sbuf, sizeof(sbuf[0]), maxlen, fstr);
		for (p = list; ISVALID(p); p++)
			if (!strcmp(N_NAME(p), sbuf)) {
				p->n_value = s->n_value;
				p->n_type = s->n_type;
				p->n_ovly = s->n_ovly;
				if (!--entries)
					goto done2;
			}
	}
done2:	(void)fclose(fstr);
done1:	(void)fclose(fsym);
	return(entries);
}
