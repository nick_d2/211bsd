
/*
 *      Program Name:   nsym.c
 *      Author:  S.M. Schultz
 *
 *      -----------   Modification History   ------------
 *      Version Date            Reason For Modification
 *      1.0     31Oct93         1. Initial release into the public domain.
 *				   Calculating the offsets of the string
 *				   and symbol tables in an executable is
 *				   rather messy and verbose when dealing
 *				   with overlaid objects.  The macros (in
 *				   a.out.h) cross_n_stroff, cross_n_symoff, etc simply
 *				   call these routines.
 *      --------------------------------------------------              
*/

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_exec exec
#define cross_n_stroff n_stroff
#define cross_n_symoff n_symoff
#define cross_n_datoff n_datoff
#define cross_n_dreloc n_dreloc
#define cross_n_treloc n_treloc
#define cross_off_t off_t
#define cross_ovlhdr ovlhdr
#define cross_xexec xexec
#define cross_u_short u_short
#endif

cross_off_t cross_n_stroff(ep) register struct cross_xexec *ep; {
	cross_off_t	l;

	l = cross_n_symoff(ep);
	l += ep->e.a_syms;
	return(l);
}

cross_off_t cross_n_datoff(ep) register struct cross_xexec *ep; {
	cross_off_t	l;

	l = cross_n_treloc(ep);
	l -= ep->e.a_data;
	return(l);
}

/*
 * Obviously if bit 0 of the flags word (a_flag) is not off then there's
 * no relocation information present and this routine shouldn't have been
 * called.
*/

cross_off_t cross_n_dreloc(ep) register struct cross_xexec *ep; {
	cross_off_t	l;
	register cross_u_short *ov = ep->o.ov_siz;
	register int	i;

	l = (cross_off_t)sizeof (struct cross_exec) + ep->e.a_text + ep->e.a_data;
	if	(ep->e.a_magic == A_MAGIC5 || ep->e.a_magic == A_MAGIC6)
		{
		for	(i = 0; i < NOVL; i++)
			l += *ov++;
		l += sizeof (struct cross_ovlhdr);
		}
	l += ep->e.a_text;
	return(l);
}

cross_off_t cross_n_treloc(ep) register struct cross_xexec *ep; {
	cross_off_t	l;

	l = cross_n_dreloc(ep);
	l -= ep->e.a_text;
	return(l);
}

cross_off_t cross_n_symoff(ep) register struct cross_xexec *ep; {
	register int	i;
	register cross_u_short *ov;
	cross_off_t	sum, l;

	l = (cross_off_t) N_TXTOFF(ep->e);
	sum = (cross_off_t)ep->e.a_text + ep->e.a_data;
	if	(ep->e.a_magic == A_MAGIC5 || ep->e.a_magic == A_MAGIC6)
		{
		for	(ov = ep->o.ov_siz, i = 0; i < NOVL; i++)
			sum += *ov++;
		}
	l += sum;
	if	((ep->e.a_flag & 1) == 0)	/* relocation present? */
		l += sum;
	return(l);
}
