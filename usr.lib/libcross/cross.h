#ifndef _CROSS_H_
#define _CROSS_H_

#ifdef pdp11
typedef double cross_double_t;
typedef float cross_float_t;
typedef int cross_int_t;
typedef long cross_long_t;
typedef short cross_short_t;
typedef unsigned int cross_uint_t;
typedef unsigned long cross_ulong_t;
typedef unsigned short cross_ushort_t;
#else
#include <stdint.h>
typedef struct { uint32_t l; uint32_t h; } cross_double_t;
typedef struct { uint32_t h; } cross_float_t;
typedef int16_t cross_int_t;
typedef int32_t cross_long_t;
typedef int16_t cross_short_t;
typedef uint16_t cross_uint_t;
typedef uint32_t cross_ulong_t;
typedef uint16_t cross_ushort_t;
#endif

/* off_t and time_t stuff should really be in sys/types.h rather than here */
#define cross_read_int cross_read_short
#define cross_read_uint cross_read_ushort
#define cross_read_off_t cross_read_long
#define cross_read_time_t cross_read_long
#define cross_write_int cross_write_short
#define cross_write_uint cross_write_ushort
#define cross_write_off_t cross_write_long
#define cross_write_time_t cross_write_long

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

/* cross.c */
cross_float_t cross_read_float __P((char *buf));
cross_double_t cross_read_double __P((char *buf));
short cross_read_short __P((char *buf));
unsigned short cross_read_ushort __P((char *buf));
long cross_read_long __P((char *buf));
unsigned long cross_read_ulong __P((char *buf));
void cross_write_float __P((char *buf, cross_float_t val));
void cross_write_double __P((char *buf, cross_double_t val));
void cross_write_short __P((char *buf, int val));
void cross_write_ushort __P((char *buf, unsigned int val));
void cross_write_long __P((char *buf, long val));
void cross_write_ulong __P((char *buf, unsigned long val));

#endif
