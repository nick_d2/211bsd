/*
 * Copyright (c) 1983, 1987 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)time.h	1.2 (Berkeley) 3/4/87
 */

#ifndef _TIME_H_
#define _TIME_H_

/*
 * Structure returned by gmtime and localtime calls (see ctime(3)).
 */
struct tm {
	int	tm_sec;
	int	tm_min;
	int	tm_hour;
	int	tm_mday;
	int	tm_mon;
	int	tm_year;
	int	tm_wday;
	int	tm_yday;
	int	tm_isdst;
	long	tm_gmtoff;
	char	*tm_zone;
};

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

/* ctime.c */
char *ctime __P((time_t *t));
char *asctime __P((register struct tm *timeptr));
void tzset __P((void));
struct tm *localtime __P((time_t *timep));
struct tm *gmtime __P((time_t *clock));
struct tm *offtime __P((time_t *clock, long offset));
/* timezone.c */
char *timezone __P((int zone, int dst));
char *tztab __P((register int zone, int dst));

#endif
