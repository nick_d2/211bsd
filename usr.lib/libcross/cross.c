#include "cross/cross.h"

cross_float_t cross_read_float(res, buf) char *buf; {
	cross_float_t res;
	res.h = (((uint32_t)buf[0] & 0xff) << 16) | (((uint32_t)buf[1] & 0xff) << 24) |
		((uint32_t)buf[2] & 0xff) | (((uint32_t)buf[3] & 0xff) << 8);
	return res;
}

cross_double_t cross_read_double(res, buf) char *buf; {
	cross_double_t res;
	res.h = (((uint32_t)buf[0] & 0xff) << 16) | (((uint32_t)buf[1] & 0xff) << 24) |
		((uint32_t)buf[2] & 0xff) | (((uint32_t)buf[3] & 0xff) << 8);
	res.l = (((uint32_t)buf[4] & 0xff) << 16) | (((uint32_t)buf[5] & 0xff) << 24) |
		((uint32_t)buf[6] & 0xff) | (((uint32_t)buf[7] & 0xff) << 8);
	return res;
}

/* XXX we assume char is signed here */
short cross_read_short(buf) char *buf; {
	return (buf[0] & 0xff) | (buf[1] << 8);
}

unsigned short cross_read_ushort(buf) char *buf; {
	return (buf[0] & 0xff) | ((buf[1] & 0xff) << 8);
}

/* XXX we assume char is signed here */
long cross_read_long(buf) char *buf; {
	return (((long)buf[0] & 0xff) << 16) | ((long)buf[1] << 24) |
		((long)buf[2] & 0xff) | (((long)buf[3] & 0xff) << 8);
}

unsigned long cross_read_ulong(buf) char *buf; {
	return (((unsigned long)buf[0] & 0xff) << 16) | (((unsigned long)buf[1] & 0xff) << 24) |
		((unsigned long)buf[2] & 0xff) | (((unsigned long)buf[3] & 0xff) << 8);
}

void cross_write_float(buf, val) char *buf; cross_float_t val; {
	buf[0] = (val.h >> 16) & 0xff;
	buf[1] = (val.h >> 24) & 0xff;
	buf[2] = val.h & 0xff;
	buf[3] = (val.h >> 8) & 0xff;
}

void cross_write_double(buf, val) char *buf; cross_double_t val; {
	buf[0] = (val.h >> 16) & 0xff;
	buf[1] = (val.h >> 24) & 0xff;
	buf[2] = val.h & 0xff;
	buf[3] = (val.h >> 8) & 0xff;
	buf[4] = (val.l >> 16) & 0xff;
	buf[5] = (val.l >> 24) & 0xff;
	buf[6] = val.l & 0xff;
	buf[7] = (val.l >> 8) & 0xff;
}

void cross_write_short(buf, val) char *buf; int val; {
	buf[0] = val & 0xff;
	buf[1] = (val >> 8) & 0xff;
}

void cross_write_ushort(buf, val) char *buf; unsigned int val; {
	buf[0] = val & 0xff;
	buf[1] = (val >> 8) & 0xff;
}

void cross_write_long(buf, val) char *buf; long val; {
	buf[0] = (val >> 16) & 0xff;
	buf[1] = (val >> 24) & 0xff;
	buf[2] = val & 0xff;
	buf[3] = (val >> 8) & 0xff;
}

void cross_write_ulong(buf, val) char *buf; unsigned long val; {
	buf[0] = (val >> 16) & 0xff;
	buf[1] = (val >> 24) & 0xff;
	buf[2] = val & 0xff;
	buf[3] = (val >> 8) & 0xff;
}
