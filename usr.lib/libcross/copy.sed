s/^#\([\t ]*\)ifndef\([\t ]\+\)\(_[0-9A-Z_]\+_\)$/#\1ifndef\2_CROSS\3/
s/^#\([\t ]*\)define\([\t ]\+\)\(_[0-9A-Z_]\+_\)$/#\1define\2_CROSS\3\n\n#include "cross\/cross.h"/

s/^#\([\t ]*\)ifdef\([\t ]\+\)CROSS$/#\1if 1/
s/^#\([\t ]*\)ifndef\([\t ]\+\)CROSS$/#\1if 0/

s/#\([\t ]*\)include\([\t ]\)"\(a\.out\|ar\|machine\/machparam\|nlist\|sys\/dir\|sys\/exec\|sys\/param\|sys\/time\|sys\/types\|ranlib\|time\|tzfile\)\.h"/#\1include\2"cross\/\3.h"/
s/#\([\t ]*\)include\([\t ]\)<\(a\.out\|ar\|machine\/machparam\|nlist\|sys\/dir\|sys\/exec\|sys\/param\|sys\/time\|sys\/types\|ranlib\|time\|tzfile\)\.h>/#\1include\2"cross\/\3.h"/

s/^unsigned[\t ]\+\(int\|long\|short\)$/cross_u\1_t/g
s/^unsigned[\t ]\+\(int\|long\|short\)\([^0-9A-Za-z_]\)/cross_u\1_t\2/g
s/\([^0-9A-Za-z_]\)unsigned[\t ]\+\(int\|long\|short\)$/\1cross_u\2_t/g
s/\([^0-9A-Za-z_]\)unsigned[\t ]\+\(int\|long\|short\)\([^0-9A-Za-z_]\)/\1cross_u\2_t\3/g

s/^\(int\|long\|short\|unsigned\)$/cross_\1_t/g
s/^\(int\|long\|short\|unsigned\)\([^0-9A-Za-z_]\)/cross_\1_t\2/g
s/\([^0-9A-Za-z_]\)\(int\|long\|short\|unsigned\)$/\1cross_\2_t/g
s/\([^0-9A-Za-z_]\)\(int\|long\|short\|unsigned\)\([^0-9A-Za-z_]\)/\1cross_\2_t\3/g

s/^cross_unsigned_t\([\t ]\+\)char$/unsigned\1char/g
s/^cross_unsigned_t\([\t ]\+\)char\([^0-9A-Za-z_]\)/unsigned\1char\2/g
s/\([^0-9A-Za-z_]\)cross_unsigned_t\([\t ]\+\)char$/\1unsigned\2char/g
s/\([^0-9A-Za-z_]\)cross_unsigned_t\([\t ]\+\)char\([^0-9A-Za-z_]\)/\1unsigned\2char\3/g

s/^cross_unsigned_t$/cross_uint_t/g
s/^cross_unsigned_t\([^0-9A-Za-z_]\)/cross_uint_t\1/g
s/\([^0-9A-Za-z_]\)cross_unsigned_t$/\1cross_uint_t/g
s/\([^0-9A-Za-z_]\)cross_unsigned_t\([^0-9A-Za-z_]\)/\1cross_uint_t\2/g

s/^\(MAXNAMLEN\|MAXPATHLEN\)$/CROSS_\1/g
s/^\(MAXNAMLEN\|MAXPATHLEN\)\([^0-9A-Za-z_]\)/CROSS_\1\2/g
s/\([^0-9A-Za-z_]\)\(MAXNAMLEN\|MAXPATHLEN\)$/\1CROSS_\2/g
s/\([^0-9A-Za-z_]\)\(MAXNAMLEN\|MAXPATHLEN\)\([^0-9A-Za-z_]\)/\1CROSS_\2\3/g

s/^\(off_t\|time_t\|u_char\|u_int\|u_long\|u_short\)$/cross_\1/g
s/^\(off_t\|time_t\|u_char\|u_int\|u_long\|u_short\)\([^0-9A-Za-z_]\)/cross_\1\2/g
s/\([^0-9A-Za-z_]\)\(off_t\|time_t\|u_char\|u_int\|u_long\|u_short\)$/\1cross_\2/g
s/\([^0-9A-Za-z_]\)\(off_t\|time_t\|u_char\|u_int\|u_long\|u_short\)\([^0-9A-Za-z_]\)/\1cross_\2\3/g

s/^struct[\t ]\+\(ar_hdr\|exec\|nlist\|oldnlist\|ovlhdr\|ranlib\|tm\|tzhead\|xexec\)$/struct cross_\1/g
s/^struct[\t ]\+\(ar_hdr\|exec\|nlist\|oldnlist\|ovlhdr\|ranlib\|tm\|tzhead\|xexec\)\([^0-9A-Za-z_]\)/struct cross_\1\2/g
s/\([^0-9A-Za-z_]\)struct[\t ]\+\(ar\|exec\|nlist\|oldnlist\|ovlhdr\|ranlib\|tm\|tzhead\|xexec\)$/\1struct cross_\2/g
s/\([^0-9A-Za-z_]\)struct[\t ]\+\(ar\|exec\|nlist\|oldnlist\|ovlhdr\|ranlib\|tm\|tzhead\|xexec\)\([^0-9A-Za-z_]\)/\1struct cross_\2\3/g

s/^\(asctime\|ctime\|gmtime\|localtime\|n_stroff\|n_datoff\|n_dreloc\|n_treloc\|n_symoff\|nlist\|offtime\|time\|timezone\|tztab\|tzset\)\([\t ]*\(__P[\t ]*([\t ]*\)\?(\)/cross_\1\2/g
s/\([^0-9A-Za-z_]\)\(asctime\|ctime\|gmtime\|localtime\|n_stroff\|n_datoff\|n_dreloc\|n_treloc\|n_symoff\|nlist\|offtime\|time\|timezone\|tztab\|tzset\)\([\t ]*\(__P[\t ]*([\t ]*\)\?(\)/\1cross_\2\3/g
