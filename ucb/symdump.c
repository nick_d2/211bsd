/*
 *	Program Name:   strdump.c
 *	Date: January 21, 1994
 *	Author: S.M. Schultz
 *
 *	-----------------   Modification History   ---------------
 *      Version Date            Reason For Modification
 *      1.0     12Feb94         1. Initial release into the public domain.
*/

/*
 * Dump the symbol table of a program to stdout, one symbol per line in
 * the form:
 *
 *	symbol_string  type  overlay  value
 *
 * Typical use is to feed the output of this program into:
 *
 *    "sort +0 -1 +1n -2 +2n -3 +3n -4 -u"
 *
 * This program is used by 'strcompact' to compress the string (and
 * symbol) tables of an executable. 
*/

#include <sys/types.h>
#include <sys/dir.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/file.h>
#include <string.h>
#include <unistd.h>

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_nlist nlist
#define cross_off_t off_t
#define cross_xexec xexec
#endif

	char	**xargv;	/* global copy of argv */
	char	*strp;		/* pointer to in-memory string table */
	struct	cross_xexec xhdr;	/* the extended a.out header */

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

int main __P((int argc, char **argv));
void namelist __P((void));
void dumpsyms __P((register FILE *fi, int nsyms));
void error __P((char *s));

int main(argc, argv) int argc; char **argv; {
	if	(argc != 2)
		{
		fprintf(stderr, "%s:  need a file name\n", argv[0]);
		exit(1);
		}
	xargv = ++argv;
	namelist();
	exit(0);
}

void namelist() {
#ifndef pdp11
	int	i;
#endif
	char	ibuf[BUFSIZ];
	register FILE	*fi;
	cross_off_t	o, stroff;
	cross_off_t	strsiz;
	register int n;

	fi = fopen(*xargv, "r");
	if	(fi == NULL)
		error("cannot open");
	setbuf(fi, ibuf);

	if (fread((char *)&xhdr, sizeof(xhdr), 1, fi) != 1)
		error("error reading header");
#ifdef CROSS
	xhdr.e.a_magic = cross_read_int((char *)&xhdr.e.a_magic);
	xhdr.e.a_text = cross_read_uint((char *)&xhdr.e.a_text);
	xhdr.e.a_data = cross_read_uint((char *)&xhdr.e.a_data);
	xhdr.e.a_bss = cross_read_uint((char *)&xhdr.e.a_bss);
	xhdr.e.a_syms = cross_read_uint((char *)&xhdr.e.a_syms);
	xhdr.e.a_entry = cross_read_uint((char *)&xhdr.e.a_entry);
	xhdr.e.a_unused = cross_read_uint((char *)&xhdr.e.a_unused);
	xhdr.e.a_flag = cross_read_uint((char *)&xhdr.e.a_flag);
	xhdr.o.max_ovl = cross_read_int((char *)&xhdr.o.max_ovl);
	for (i = 0; i < NOVL; ++i)
		xhdr.o.ov_siz[i] = cross_read_uint((char *)(xhdr.o.ov_siz + i));
#endif
	if	(N_BADMAG(xhdr.e))
		error("bad format");
	rewind(fi);

	o = N_SYMOFF(xhdr);
	fseek(fi, (off_t)o, L_SET);
	n = xhdr.e.a_syms / sizeof(struct cross_nlist);
	if	(n == 0)
		error("no name list");

	stroff = N_STROFF(xhdr);
	fseek(fi, (off_t)stroff, L_SET);
	if	(fread(&strsiz, sizeof (cross_off_t), 1, fi) != 1)
		error("no string table");
#ifdef CROSS
	strsiz = cross_read_off_t((char *)&strsiz);
#endif
	strp = (char *)malloc((int)strsiz);
	if	(strp == NULL || strsiz > 48 * 1024L)
		error("no memory for strings");
	if	(fread(strp+sizeof(strsiz),(int)strsiz-sizeof(strsiz),1,fi)!=1)
		error("error reading strings");

	fseek(fi, o, L_SET);
	dumpsyms(fi, n);
	free((char *)strp);
	fclose(fi);
}

void dumpsyms(fi, nsyms) register FILE *fi; int nsyms; {
	register int n;
	struct	cross_nlist sym;
	register struct cross_nlist *sp;

	sp = &sym;
	for	(n = 0; n < nsyms; n++)
		{
		if (fread(&sym, sizeof sym, 1, fi) != 1)
			error("error reading sym");
#ifdef CROSS
		sym.n_un.n_strx = cross_read_off_t((char *)&sym.n_un.n_strx);
		sym.n_value = cross_read_uint((char *)&sym.n_value);
#endif
		printf("%s %u %u %u\n", strp + (int)sp->n_un.n_strx, sp->n_type,
			sp->n_ovly, sp->n_value);
		}	
}

void error(s) char *s; {
	fprintf(stderr, "syms: %s: %s\n", *xargv, s);
	exit(1);
}
