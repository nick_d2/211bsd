/*
 *	Program Name:   strcompact.c
 *	Date: February 12, 1994
 *	Author: S.M. Schultz
 *
 *	-----------------   Modification History   ---------------
 *      Version Date            Reason For Modification
 *      1.0     21Jan94         1. Initial release into the public domain.
 *	2.0	12Feb94		2. Rewrite.  Use new utility program 'symdump'
 *				   and a multi-key sort to not only create
 *				   shared symbol strings but remove identical
 *				   symbols (identical absolute local symbols 
 *				   are quite common).  Execution speed was
 *				   speed up by about a factor of 4.
*/

/*
 * This program compacts the string table of an executable image by
 * preserving only a single string definition of a symbol and updating
 * the symbol table string offsets.  Multiple symbols having the same
 * string are very common - local symbols in a function often have the
 * same name ('int error' inside a function for example).  This program
 * reduced the string table size of the kernel at least 25%!
 *
 * In addition, local symbols with the same value (frame offset within
 * a function) are very common.  By retaining only a single '~error=2'
 * for example the symbol table is reduced even further (about 500 symbols
 * are removed from a typical kernel).
*/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <sys/file.h>
#include <unistd.h>

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_nlist nlist
#define cross_off_t off_t
#define cross_xexec xexec
#endif

	char	*Pgm;
	char	*Sort = LOCALESTR "/usr/bin/sort";
	char	*Symdump = CROSSPREFIX "/usr/ucb/symdump";
static	char	strfn[32], symfn[32];

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

int main __P((int argc, char **argv));
void copyfile __P((register FILE *in, register FILE *out));
void fatal __P((char *Pgm, char *str));
void cleanup __P((void));

int main(argc, argv) int argc; char **argv; {
	struct	cross_nlist	sym;
#ifdef CROSS
/* CROSSPREFIX can be very long, really should check it, but fudge it for now */
	char	buf1[BUFSIZ];
#else
	char	buf1[128];
#endif
	char	symname[64], savedname[64];
	struct	cross_xexec	xhdr;
#ifdef CROSS
	int	i;
#endif
	int	nsyms, len;
	int	fd;
	FILE	*symfp, *strfp, *sortfp;
register FILE	*fpin;
	cross_off_t	stroff;
	unsigned /*short*/ type, value, ovly;
	void	cleanup();
#ifdef CROSS
	cross_off_t	temp_stroff;
#endif

	Pgm = argv[0];
	signal(SIGQUIT, (sig_t)cleanup);
	signal(SIGINT, (sig_t)cleanup);
	signal(SIGHUP, (sig_t)cleanup);

	if	(argc != 2)
		{
		fprintf(stderr, "%s: missing filename argument\n", Pgm);
		exit(EX_USAGE);
		}
	fpin = fopen(argv[1], "r+");
	if	(!fpin)
		{
		fprintf(stderr, "%s: can not open '%s' for update\n", 
			Pgm, argv[1]);
		exit(EX_NOINPUT);
		}
	if	(fread(&xhdr, 1, sizeof (xhdr), fpin) < sizeof (xhdr.e))
		{
		fprintf(stderr, "%s: premature EOF\n", Pgm);
		exit(EX_DATAERR);
		}
#ifdef CROSS
	xhdr.e.a_magic = cross_read_int((char *)&xhdr.e.a_magic);
	xhdr.e.a_text = cross_read_uint((char *)&xhdr.e.a_text);
	xhdr.e.a_data = cross_read_uint((char *)&xhdr.e.a_data);
	xhdr.e.a_bss = cross_read_uint((char *)&xhdr.e.a_bss);
	xhdr.e.a_syms = cross_read_uint((char *)&xhdr.e.a_syms);
	xhdr.e.a_entry = cross_read_uint((char *)&xhdr.e.a_entry);
	xhdr.e.a_unused = cross_read_uint((char *)&xhdr.e.a_unused);
	xhdr.e.a_flag = cross_read_uint((char *)&xhdr.e.a_flag);
	xhdr.o.max_ovl = cross_read_int((char *)&xhdr.o.max_ovl);
	for (i = 0; i < NOVL; ++i)
		xhdr.o.ov_siz[i] = cross_read_uint((char *)(xhdr.o.ov_siz + i));
#endif
	if	(N_BADMAG(xhdr.e))
		{
		fprintf(stderr, "%s: Bad magic number\n", Pgm);
		exit(EX_DATAERR);
		}
	nsyms = xhdr.e.a_syms / sizeof (struct cross_nlist);
	if	(!nsyms)
		{
		fprintf(stderr, "%s: '%s' stripped\n", Pgm, argv[1]);
		exit(EX_OK);
		}

	sprintf(buf1, "%s %s | %s +0 -1 +1n -2 +2n -3 +3n -4 -u", Symdump,
		argv[1], Sort);
 /*fprintf(stderr, "%s\n", buf1);*/
	sortfp = popen(buf1, "r");
	if	(!sortfp)
		{
		fprintf(stderr, "%s: symdump | sort failed\n", Pgm);
		exit(EX_SOFTWARE);
		}
	strcpy(symfn, "/tmp/symXXXXXX");
	if	((fd = 	mkstemp(symfn)) == -1 || !(symfp = fdopen(fd, "w+")))
		{
		fprintf(stderr, "%s: can't create %s\n", Pgm, symfn);
		exit(EX_CANTCREAT);
		}
	strcpy(strfn, "/tmp/strXXXXXX");
	if	((fd = mkstemp(strfn)) == -1 || !(strfp = fdopen(fd, "w+")))
		{
		fprintf(stderr, "%s: can't create %s\n", Pgm, strfn);
		exit(EX_CANTCREAT);
		}

	stroff = sizeof (cross_off_t);
	len = 0;
	nsyms = 0;
	while	(fscanf(sortfp, "%s %u %u %u\n", symname, &type, &ovly,
			&value) == 4)
		{
		if	(strcmp(symname, savedname))
			{
			stroff += len;
			len = strlen(symname) + 1;
			fwrite(symname, len, 1, strfp);
			strcpy(savedname, symname);
			}
		sym.n_un.n_strx = stroff;
		sym.n_type = type;
		sym.n_ovly = ovly;
		sym.n_value = value;
#ifdef CROSS
		cross_write_off_t((char *)&sym.n_un.n_strx, sym.n_un.n_strx);
		cross_write_uint((char *)&sym.n_value, sym.n_value);
#endif
		fwrite(&sym, sizeof (sym), 1, symfp);
		nsyms++;
		}
	stroff += len;

	pclose(sortfp);
	rewind(symfp);
	rewind(strfp);

	if	(nsyms == 0)
		{
		fprintf(stderr, "%s: No symbols - %s not modified\n", Pgm, argv[1]);
		cleanup();
		}

	fseek(fpin, N_SYMOFF(xhdr), L_SET);

/*
 * Now append the new symbol table.  Then write the string table length
 * followed by the string table.  Finally truncate the file to the new
 * length, reflecting the smaller string table.
*/
	copyfile(symfp, fpin);
#ifdef CROSS
	cross_write_off_t((char *)&temp_stroff, stroff);
	fwrite(&temp_stroff, sizeof (cross_off_t), 1, fpin);
#else
	fwrite(&stroff, sizeof (cross_off_t), 1, fpin);
#endif
	copyfile(strfp, fpin);
	ftruncate(fileno(fpin), ftell(fpin));

/*
 * Update the header with the correct symbol table size.
*/
	rewind(fpin);
	xhdr.e.a_syms = nsyms * sizeof (sym);
#ifdef CROSS
	cross_write_int((char *)&xhdr.e.a_magic, xhdr.e.a_magic);
	cross_write_uint((char *)&xhdr.e.a_text, xhdr.e.a_text);
	cross_write_uint((char *)&xhdr.e.a_data, xhdr.e.a_data);
	cross_write_uint((char *)&xhdr.e.a_bss, xhdr.e.a_bss);
	cross_write_uint((char *)&xhdr.e.a_syms, xhdr.e.a_syms);
	cross_write_uint((char *)&xhdr.e.a_entry, xhdr.e.a_entry);
	cross_write_uint((char *)&xhdr.e.a_unused, xhdr.e.a_unused);
	cross_write_uint((char *)&xhdr.e.a_flag, xhdr.e.a_flag);
#endif
	fwrite(&xhdr.e, sizeof (xhdr.e), 1, fpin);

	fclose(fpin);
	fclose(symfp);
	fclose(strfp);
	cleanup();
}

void copyfile(in, out) register FILE *in; register FILE *out; {
	register int c;

	while	((c = getc(in)) != EOF)
		putc(c, out);
}

void fatal(Pgm, str) char *Pgm; char *str; {
	if	(strfn[0])
		unlink(strfn);
	if	(symfn[0])
		unlink(symfn);
	if	(!str)
		exit(EX_OK);
	fprintf(stderr, "%s: %s\n", Pgm, str);
	exit(EX_SOFTWARE);
}

void cleanup() {
	fatal((char *)NULL, (char *)NULL);
}
