/* (c) 1979 Regents of the University of California */
/* fake portable I/O routines, for those
    sites so backward as to not have the
     port. library */
/* munged for portable i/o library: peter and louise 5 may 80 */

#include <stdlib.h>
#include "ey.h"

FILE *cin, *cout;

FILE *copen(s, c) char *s; char c; {
  FILE *f;

	  if( c == 'r' ){
	    f = fopen( s, "r" );
  } else  if( c == 'a' ){
	    f = fopen( s, "a" );
	    fseek( f, 0L, 2 );
  } else {  /* c == w */
	    f = fopen( s, "w" );
  }

  return( f );
}

void cflush(x) FILE *x; { /* fake! sets file to x */
  fflush( cout );
  cout = x;
}

void _system(char *cmd) {
  error( "The function \"_system\" is called" );
}

void cclose(i) FILE *i; {
  fclose(i);
}

int cexit(i) int i; {
  fflush( cout );
  if ( i != 0 ) {
    abort();
  }
  exit(i);
}
