#!/bin/sh
ROOT="`pwd |sed -e 's/\/ucb\/pascal\/eyacc$//'`"
HOSTCC="cc -g -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib/ -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
make HOSTCC="$HOSTCC" HOSTSEPFLAG=
