#!/bin/sh
ROOT="`pwd |sed -e 's/\/ucb\/pascal\/pxp$//'`"
CC="$ROOT/cross/bin/cc"
HOSTCC="cc -g -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib/ -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
MKSTR="$ROOT/cross/usr/ucb/mkstr"
mkdir --parents "$ROOT/stage/usr/share/pascal"
mkdir --parents "$ROOT/stage/usr/ucb"
make CC="$CC" HOSTCC="$HOSTCC" MKSTR="$MKSTR" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
