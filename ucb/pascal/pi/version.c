/* Copyright (c) 1979 Regents of the University of California */

#ifdef DOSCCS
static	char sccsid[] = "@(#)version.c	2.1";	/*	SCCS id keyword 	*/
#endif

    /*
     *	this writes the declaration of the character string version
     *	onto standard output.
     *	useful for makeing Version.c give the correct date for pi.
     */

#include <stdio.h>
#include <time.h>

long		_clock;
char		*cstring;

int main() {
	time( &_clock );
	cstring = ctime( &_clock );
	cstring[ 24 ] = '\0';
	printf( "char	version[] = \"%s\";\n" , cstring );
}
