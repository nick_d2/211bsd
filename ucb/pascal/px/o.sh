#!/bin/sh
ROOT="`pwd |sed -e 's/\/ucb\/pascal\/px$//'`"
AS="$ROOT/cross/bin/as"
CC="$ROOT/cross/bin/cc"
LD="$ROOT/cross/bin/ld"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
mkdir --parents "$ROOT/stage/usr/ucb/px"
mkdir --parents "$ROOT/stage/usr/libexec/gather"
make AS="$AS" CC="$CC" LD="$LD" STAGEDIR="$ROOT/stage" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
