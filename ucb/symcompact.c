/*
 *	Program Name:   symcompact.c
 *	Date: January 21, 1994
 *	Author: S.M. Schultz
 *
 *	-----------------   Modification History   ---------------
 *      Version Date            Reason For Modification
 *      1.0     21Jan94         1. Initial release into the public domain.
 *	1.1	11Feb94		2. Remove register symbols to save memory.
*/

/*
 * This program compacts the symbol table of an executable.  This is
 * done by removing '~symbol' references when _both_ the '~symbol' and
 * '_symbol' have an overlay number of 0.  The assembler always generates
 * both forms.  The only time both forms are needed is in an overlaid 
 * program and the routine has been relocated by the linker, in that event
 * the '_' form is the overlay "thunk" and the '~' form is the actual 
 * routine itself.  Only 'text' symbols have both forms.  Reducing the
 * number of symbols greatly speeds up 'cross_nlist' processing as well as 
 * cutting down memory requirements for programs such as 'adb' and 'nm'.
 *
 * NOTE: This program attempts to hold both the string and symbol tables
 * in memory.  For the kernel which has not been 'strcompact'd this
 * amounts to about 49kb.  IF this program runs out of memory you should
 * run 'strcompact' first - that program removes redundant strings, 
 * significantly reducing the amount of memory needed.  Alas, this program
 * will undo some of strcompact's work and you may/will need to run
 * strcompact once more after removing excess symbols.
 *
 * Register symbols are removed to save memory.  This program was initially
 * used with a smaller kernel, adding an additional driver caused the symbol
 * table to grow enough that memory couldn't be allocated for strings.  See
 * the comments in 'symorder.c' - they explain why register variables are
 * no big loss.
*/

#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <sys/file.h>
#include <unistd.h>

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_nlist nlist
#define cross_off_t off_t
#define cross_xexec xexec
#endif

	char	*Pgm;
static	char	strtmp[20];

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

int main __P((int argc, char **argv));
void cleanup __P((void));
int exclude __P((register struct cross_nlist *sp));

#ifdef CROSS
#define N_NAME(sp) (strtab + (int)(sp)->n_un.n_strx)
#else
#define N_NAME(sp) ((sp)->n_un.n_name)
#endif

int main(argc, argv) int argc; char **argv; {
	int	fd;
	FILE	*fp, *strfp;
	int	cnt, nsyms, len, c, symsremoved = 0, i;
	void	cleanup();
	char	*strtab;
	char	fbuf1[BUFSIZ], fbuf2[BUFSIZ];
	cross_off_t	symoff, stroff, ltmp;
	cross_off_t	strsiz;
	register struct	cross_nlist	*sp, *sp2;
	struct	cross_nlist	*symtab, *symtabend, syment;
	struct	cross_xexec	xhdr;

	Pgm = argv[0];
	signal(SIGQUIT, (sig_t)cleanup);
	signal(SIGINT, (sig_t)cleanup);
	signal(SIGHUP, (sig_t)cleanup);

	if	(argc != 2)
		{
		fprintf(stderr, "%s: filename argument missing\n", Pgm);
		exit(EX_USAGE);
		}
	fp = fopen(argv[1], "r+");
	if	(!fp)
		{
		fprintf(stderr, "%s: can't open '%s' for update\n", Pgm,
			argv[1]);
		exit(EX_NOINPUT);
		}
	setbuf(fp, fbuf1);
	cnt = fread(&xhdr, 1, sizeof (xhdr), fp);
	if	(cnt < sizeof (xhdr.e))
		{
		fprintf(stderr, "%s: Premature EOF reading header\n", Pgm);
		exit(EX_DATAERR);
		}
#ifdef CROSS
	xhdr.e.a_magic = cross_read_int((char *)&xhdr.e.a_magic);
	xhdr.e.a_text = cross_read_uint((char *)&xhdr.e.a_text);
	xhdr.e.a_data = cross_read_uint((char *)&xhdr.e.a_data);
	xhdr.e.a_bss = cross_read_uint((char *)&xhdr.e.a_bss);
	xhdr.e.a_syms = cross_read_uint((char *)&xhdr.e.a_syms);
	xhdr.e.a_entry = cross_read_uint((char *)&xhdr.e.a_entry);
	xhdr.e.a_unused = cross_read_uint((char *)&xhdr.e.a_unused);
	xhdr.e.a_flag = cross_read_uint((char *)&xhdr.e.a_flag);
	xhdr.o.max_ovl = cross_read_int((char *)&xhdr.o.max_ovl);
	for (i = 0; i < NOVL; ++i)
		xhdr.o.ov_siz[i] = cross_read_uint((char *)(xhdr.o.ov_siz + i));
#endif
	if	(N_BADMAG(xhdr.e))
		{
		fprintf(stderr, "%s: Bad magic number\n", Pgm);
		exit(EX_DATAERR);
		}
	nsyms = xhdr.e.a_syms / sizeof (struct cross_nlist);
	if	(!nsyms)
		{
		fprintf(stderr, "%s: '%s' stripped\n", Pgm);
		exit(EX_OK);
		}
	stroff = N_STROFF(xhdr);
	symoff = N_SYMOFF(xhdr);
/*
 * Seek to the string table size longword and read it.  Then attempt to
 * malloc memory to hold the string table.  First make a sanity check on
 * the size.
*/
	fseek(fp, stroff, L_SET);
	fread(&strsiz, sizeof (cross_off_t), 1, fp);
#ifdef CROSS
	strsiz = cross_read_off_t((char *)&strsiz);
#endif
	if	(strsiz > 48 * 1024L)
		{
		fprintf(stderr, "%s: string table > 48kb\n", Pgm);
		exit(EX_DATAERR);
		}
	strtab = (char *)malloc((int)strsiz);
	if	(!strtab)
		{
		fprintf(stderr, "%s: no memory for strings\n", Pgm);
		exit(EX_OSERR);
		}
/*
 * Now read the string table into memory.  Reduce the size read because
 * we've already retrieved the string table size longword.  Adjust the
 * address used so that we don't have to adjust each symbol table entry's
 * string offset.
*/
	cnt = fread(strtab + sizeof (cross_off_t), 1, (int)strsiz - sizeof (cross_off_t), fp);
	if	(cnt != (int)strsiz - sizeof (cross_off_t))
		{
		fprintf(stderr, "%s: Premature EOF reading strings\n", Pgm);
		exit(EX_DATAERR);
		}
/*
 * Seek to the symbol table.  Scan it and count how many symbols are 
 * significant.
*/
	fseek(fp, symoff, L_SET);
	cnt = 0;
	for	(i = 0; i < nsyms; i++)
		{
		fread(&syment, sizeof (syment), 1, fp);
#ifdef CROSS
		syment.n_un.n_strx = cross_read_off_t((char *)&syment.n_un.n_strx);
		syment.n_value = cross_read_uint((char *)&syment.n_value);
#endif
		if	(exclude(&syment))
			continue;
		cnt++;
		}

/*
 * Allocate memory for the symbol table.
*/
	symtab = (struct cross_nlist *)malloc(cnt * sizeof (struct cross_nlist));
	if	(!symtab)
		{
		fprintf(stderr, "%s: no memory for symbols\n", Pgm);
		exit(EX_OSERR);
		}

/*
 * Now read the symbols in, excluding the same ones as before, and
 * assign the in-memory string addresses at the same time
*/
	sp = symtab;
	fseek(fp, symoff, L_SET);

	for	(i = 0; i < nsyms; i++)
		{
		fread(&syment, sizeof (syment), 1, fp);
#ifdef CROSS
		syment.n_un.n_strx = cross_read_off_t((char *)&syment.n_un.n_strx);
		syment.n_value = cross_read_uint((char *)&syment.n_value);
#endif
		if	(exclude(&syment))
			continue;
		bcopy(&syment, sp, sizeof (syment));
#ifdef pdp11
		sp->n_un.n_name = strtab + (int)sp->n_un.n_strx;
#endif
		sp++;
		}
	symtabend = &symtab[cnt];

/*
 * Now look for symbols with overlay numbers of 0 (root/base segment) and
 * of type 'text'.  For each symbol found check if there exists both a '~'
 * and '_' prefixed form of the symbol.  Preserve the '_' form and clear
 * the '~' entry by zeroing the string address of the '~' symbol.
*/
	for	(sp = symtab; sp < symtabend; sp++)
		{
 /*fprintf(stderr, "check %s %02x %02x %04x\n", N_NAME(sp), sp->n_ovly, sp->n_type, sp->n_value);*/
		if	(sp->n_ovly)
			continue;
		if	((sp->n_type & N_TYPE) != N_TEXT)
			continue;
		if	(N_NAME(sp)[0] != '~')
			continue;
/*
 * At this point we have the '~' form of a non overlaid text symbol.  Look
 * thru the symbol table for the '_' form.  All of 1) symbol type, 2) Symbol
 * value and 3) symbol name (starting after the first character) must match.
*/
		for	(sp2 = symtab; sp2 < symtabend; sp2++)
			{
 /*fprintf(stderr, "against %s %02x %02x %04x\n", N_NAME(sp2), sp2->n_ovly, sp2->n_type, sp2->n_value);*/
			if	(sp2->n_ovly)
				continue;
			if	((sp2->n_type & N_TYPE) != N_TEXT)
				continue;
			if	(N_NAME(sp2)[0] != '_')
				continue;
			if	(sp2->n_value != sp->n_value)
				continue;
			if	(strcmp(N_NAME(sp)+1, N_NAME(sp2)+1))
				continue;
/*
 * Found a match.  Null out the '~' symbol's string address.
*/
			symsremoved++;
#ifdef CROSS
			sp->n_un.n_strx = 0;
#else
			sp->n_un.n_name = NULL;
#endif
			break;
			}
		}
/*
 * Done with the nested scanning of the symbol table.  Now create a new
 * string table (from the remaining symbols) in a temporary file.
*/
	strcpy(strtmp, "/tmp/strXXXXXX");
	if ((fd = mkstemp(strtmp)) == -1 || !(strfp = fopen(strtmp, "w+")))
		{
		fprintf(stderr, "%s: can't create '%s'\n", Pgm, strtmp);
		exit(EX_CANTCREAT);
		}
	setbuf(strfp, fbuf2);

/*
 * As each symbol is written to the tmp file the symbol's string offset
 * is updated with the new file string table offset.
*/
	ltmp = sizeof (cross_off_t);
	for	(sp = symtab; sp < symtabend; sp++)
		{
#ifdef pdp11
		/* note: we could have just nulled out all 4 bytes earlier */
		if	(!sp->n_un.n_name) {
			sp->n_un.n_strx = 0;
			continue;
		}
#else
		if	(!sp->n_un.n_strx)
#endif
			continue;
		len = strlen(N_NAME(sp)) + 1;
		fwrite(N_NAME(sp), len, 1, strfp);
		sp->n_un.n_strx = ltmp;
		ltmp += len;
		}
/*
 * We're done with the memory string table - give it back.  Then reposition
 * the new string table file to the beginning.
*/
	free(strtab);
	rewind(strfp);

/*
 * Position the executable file to where the symbol table begins.  Truncate
 * the file.  Write out the valid symbols, counting each one so that the 
 * a.out header can be updated when we're done.
*/
	nsyms = 0;
	fseek(fp, symoff, L_SET);
	ftruncate(fileno(fp), ftell(fp));
	for	(sp = symtab; sp < symtabend; sp++)
		{
		if	(!sp->n_un.n_strx)
			continue;
		nsyms++;
#ifdef CROSS
		cross_write_off_t((char *)&sp->n_un.n_strx, sp->n_un.n_strx);
		cross_write_uint((char *)&sp->n_value, sp->n_value);
#endif
		fwrite(sp, sizeof (struct cross_nlist), 1, fp);
		}
/*
 * Next write out the string table size longword.
*/
#ifdef CROSS
	cross_write_off_t((char *)&ltmp, ltmp);
#endif
	fwrite(&ltmp, sizeof (cross_off_t), 1, fp);
/*
 * We're done with the in memory symbol table, release it.  Then append
 * the string table to the executable file.
*/
	free(symtab);
	while	((c = getc(strfp)) != EOF)
		putc(c, fp);
	fclose(strfp);
	rewind(fp);
	xhdr.e.a_syms = nsyms * sizeof (struct cross_nlist);
#ifdef CROSS
	cross_write_int((char *)&xhdr.e.a_magic, xhdr.e.a_magic);
	cross_write_uint((char *)&xhdr.e.a_text, xhdr.e.a_text);
	cross_write_uint((char *)&xhdr.e.a_data, xhdr.e.a_data);
	cross_write_uint((char *)&xhdr.e.a_bss, xhdr.e.a_bss);
	cross_write_uint((char *)&xhdr.e.a_syms, xhdr.e.a_syms);
	cross_write_uint((char *)&xhdr.e.a_entry, xhdr.e.a_entry);
	cross_write_uint((char *)&xhdr.e.a_unused, xhdr.e.a_unused);
	cross_write_uint((char *)&xhdr.e.a_flag, xhdr.e.a_flag);
#endif
	fwrite(&xhdr.e, sizeof (xhdr.e), 1, fp);
	fclose(fp);
	printf("%s: %d symbols removed\n", Pgm, symsremoved);
	cleanup();
}

void cleanup() {
	if	(strtmp[0])
		unlink(strtmp);
	exit(EX_OK);
}

/*
 * Place any symbol exclusion rules in this routine, return 1 if the
 * symbol is to be excluded, 0 if the symbol is to be retained.
*/

int exclude(sp) register struct cross_nlist *sp; {
	if	(sp->n_type == N_REG)
		return(1);
	if	(sp->n_un.n_strx == 0)
		return(1);
	return(0);
}
