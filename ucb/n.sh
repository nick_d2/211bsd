#!/bin/sh
ROOT="`pwd |sed -e 's/\/ucb$//'`"
HOSTCC="cc -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib -DCROSS -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
HOSTLIBCROSS="-lcross"
INSTALL="$ROOT/scripts/install.sh --strip-program=/bin/true"
MANROFF="nroff -man"
mkdir --parents "$ROOT/cross/usr/ucb"
make CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" CROSSPREFIX="\\\"$ROOT/cross\\\"" LOCALESTR="\\\"LC_ALL=C \\\"" MANROFF="$MANROFF" SEPFLAG= mkstr strcompact symcompact symdump symorder unifdef xstr && \
$INSTALL mkstr strcompact symcompact symdump symorder unifdef xstr "$ROOT/cross/usr/ucb"
