#!/bin/sh -e

ROOT="`pwd`"

AR="$ROOT/cross/bin/ar"
AS="$ROOT/cross/bin/as"
C2="$ROOT/cross/lib/c2"
CC="$ROOT/cross/bin/cc"
CHGRP="$ROOT/scripts/chgrp.sh"
CHMOD="$ROOT/scripts/chmod.sh"
CHOWN="$ROOT/scripts/chown.sh"
CPP="$ROOT/cross/lib/cpp"
LD="$ROOT/cross/bin/ld"
LEX="$ROOT/cross/usr/bin/lex"
LORDER="$ROOT/cross/usr/bin/lorder"
MANROFF="nroff -man"
MKDEP="$ROOT/cross/usr/bin/mkdep"
MKSTR="$ROOT/cross/usr/ucb/mkstr"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
HOSTCC="cc -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib -DCROSS -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
HOSTLIBCROSS="-lcross"
RANLIB="$ROOT/cross/usr/bin/ranlib"
UNIFDEF="$ROOT/cross/usr/ucb/unifdef"
SIZE="$ROOT/cross/bin/size"
STRCOMPACT="$ROOT/cross/usr/ucb/strcompact"
SYMCOMPACT="$ROOT/cross/usr/ucb/symcompact"
SYMORDER="$ROOT/cross/usr/ucb/symorder"
XSTR="$ROOT/cross/usr/ucb/xstr"
YACC="$ROOT/cross/usr/bin/yacc"

scripts/stage.sh

echo
echo "making clean"
echo

make clean

echo
echo "making depend"
echo

# this is needed before make depend, since "cc -M" accesses it
make -C include CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" INSTALL="$INSTALL" SHARED=copies SYSDIR="$ROOT/sys" DESTDIR="$ROOT/stage" install

# STAGEDIR is just for a few funny mkdep rules that look in /usr/include
make CC="$CC" MKDEP="$MKDEP" STAGEDIR="$ROOT/stage" depend

echo
echo "making libc"
echo

# build libraries, except lib/libkern and usr.lib/libU77
make -C lib/libc AR="$AR" AS="$AS" C2="$C2" CC="$CC" CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" CPP="$CPP" HOSTCC="$HOSTCC" HOSTLIBCROSS="$HOSTLIBCROSS" HOSTSEPFLAG= LD="$LD" LEX="$LEX" LORDER="$LORDER" MANROFF="$MANROFF" MKSTR="$MKSTR" RANLIB="$RANLIB" UNIFDEF="$UNIFDEF" SIZE="$SIZE" XSTR="$XSTR" YACC="$YACC" LOCALE="LC_ALL=C" STAGEDIR="$ROOT/stage"

echo
echo "installing libc"
echo

make -C lib/libc CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" INSTALL="$INSTALL" RANLIB="$RANLIB" DESTDIR="$ROOT/stage" install

echo
echo "making libraries"
echo

make -C usr.lib AR="$AR" AS="$AS" C2="$C2" CC="$CC" CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" CPP="$CPP" HOSTCC="$HOSTCC" HOSTLIBCROSS="$HOSTLIBCROSS" HOSTSEPFLAG= LD="$LD" LEX="$LEX" LORDER="$LORDER" MANROFF="$MANROFF" MKSTR="$MKSTR" RANLIB="$RANLIB" UNIFDEF="$UNIFDEF" SIZE="$SIZE" XSTR="$XSTR" YACC="$YACC" LOCALE="LC_ALL=C" STAGEDIR="$ROOT/stage" SUBDIR="lib2648 libF77 libI77 libcurses libdbm libln libom libmp libplot libtermlib liby libutil libvmf liberrlst libident libstubs"

echo
echo "installing libraries"
echo

# install libraries, except lib/libkern and usr.lib/libU77
make -C usr.lib CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" INSTALL="$INSTALL" RANLIB="$RANLIB" DESTDIR="$ROOT/stage" SUBDIR="lib2648 libF77 libI77 libcurses libdbm libln libom libmp libplot libtermlib liby libutil libvmf liberrlst libident libstubs" install

echo
echo "making"
echo

# build everything, except libraries and new
make -C lib AR="$AR" AS="$AS" C2="$C2" CC="$CC" CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" CPP="$CPP" HOSTCC="$HOSTCC" HOSTLIBCROSS="$HOSTLIBCROSS" HOSTSEPFLAG= LD="$LD" LEX="$LEX" LORDER="$LORDER" MANROFF="$MANROFF" MKSTR="$MKSTR" RANLIB="$RANLIB" UNIFDEF="$UNIFDEF" SIZE="$SIZE" XSTR="$XSTR" YACC="$YACC" LOCALE="LC_ALL=C" STAGEDIR="$ROOT/stage" SUBDIR="ccom c2 cpp"
make AR="$AR" AS="$AS" C2="$C2" CC="$CC" CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" CPP="$CPP" HOSTCC="$HOSTCC" HOSTLIBCROSS="$HOSTLIBCROSS" HOSTSEPFLAG= LD="$LD" LEX="$LEX" LORDER="$LORDER" MANROFF="$MANROFF" MKSTR="$MKSTR" RANLIB="$RANLIB" UNIFDEF="$UNIFDEF" SIZE="$SIZE" XSTR="$XSTR" YACC="$YACC" LOCALE="LC_ALL=C" STAGEDIR="$ROOT/stage" XXXLIBDIR= XXXSRCDIR="share bin sbin etc games libexec local ucb usr.bin usr.sbin man" XXXSUBDIR="backgammon battlestar boggle btlgammon cribbage fortune hack hangman hunt mille monop pdp phantasia quiz robots sail snake trek warp words" YYYSUBDIR="chess"

echo
echo "installing"
echo

# install everything, except libraries and new
make -C lib CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" INSTALL="$INSTALL" RANLIB="$RANLIB" DESTDIR="$ROOT/stage" SUBDIR="ccom c2 cpp" install
make CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" INSTALL="$INSTALL" RANLIB="$RANLIB" DESTDIR="$ROOT/stage" XXXLIBDIR= XXXSRCDIR="share bin sbin etc games libexec local ucb usr.bin usr.sbin man" XXXSUBDIR="backgammon battlestar boggle btlgammon cribbage fortune hack hangman hunt mille monop pdp phantasia quiz robots sail snake trek warp words" YYYSUBDIR="chess" install
