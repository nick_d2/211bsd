#!/bin/sh
ROOT="`pwd |sed -e 's/\/sys\/GENERIC$//'`"
AS="$ROOT/cross/bin/as"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=/bin/true"
MANROFF="nroff -man"
CPP="$ROOT/cross/lib/cpp"
LD="$ROOT/cross/bin/ld"
MKDEP="$ROOT/cross/usr/bin/mkdep"
SIZE="$ROOT/cross/bin/size"
STRCOMPACT="$ROOT/cross/usr/ucb/strcompact"
SYMCOMPACT="$ROOT/cross/usr/ucb/symcompact"
SYMORDER="$ROOT/cross/usr/ucb/symorder"
make I="$ROOT/stage/usr/include" CC="$HOSTCC" MANROFF="$MANROFF" MKDEP="$MKDEP" depend
make I="$ROOT/stage/usr/include" AS="$AS" CC="$HOSTCC" MANROFF="$MANROFF" CPP="$CPP" LD="$LD" SIZE="$SIZE" STRCOMPACT="$STRCOMPACT" SYMCOMPACT="$SYMCOMPACT" SYMORDER="$SYMORDER"
make DESTDIR="$ROOT/stage" install
