/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)checksys.c	1.6 (2.11BSD) 1998/12/5
 */

/*
 * checksys
 *	checks the system size and reports any limits exceeded.
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "csym.h"

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/machine/machparam.h"
#include "cross/nlist.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <machine/machparam.h>
#include <nlist.h>
#include <sys/types.h>
#define cross_exec exec
#define cross_nlist nlist
#define cross_off_t off_t
#define cross_ovlhdr ovlhdr
#define cross_u_int u_int
#endif

/* Round up to a click boundary. */
#define	cround(bytes)	((bytes + ctob(1) - 1) / ctob(1) * ctob(1));
#define	round(x)	(ctob(stoc(ctos(btoc(x)))))

#define	KB(val)		((u_int)(val * 1024))

#define	N_END		0
#define	N_NBUF		1
#define	N_PROC		4
#define	N_NINODE	9
#define	N_CLIST		14
#define	N_RAM		15
#define	N_XITDESC	16
#define	N_QUOTDESC	17
#define	N_NAMECACHE	18
#define	N_IOSIZE	19
#define	N_NLOG		20
#define	N_NUMSYMS	21

	struct	cross_nlist	nl[N_NUMSYMS+1];

char	strtab[] = "XXXX"		/* for notional string table length */
	"_end\0"			/*  0 */
	"_nbuf\0"			/*  1 */
	"_buf\0"			/*  2 */
	"_nproc\0"			/*  3 */
	"_proc\0"			/*  4 */
	"_ntext\0"			/*  5 */
	"_text\0"			/*  6 */
	"_nfile\0"			/*  7 */
	"_file\0"			/*  8 */
	"_ninode\0"			/*  9 */
	"_inode\0"			/* 10 */
	"_ncallout\0"			/* 11 */
	"_callout\0"			/* 12 */
	"_ucb_clist\0"			/* 13 */
	"_nclist\0"			/* 14 */
	"_ram_size\0"			/* 15 */
	"_xitdesc\0"			/* 16 */
	"_quotdesc\0"			/* 17 */
	"_namecache\0"			/* 18 */
	"__iosize\0"			/* 19 */
	"_nlog";			/* 20 */

static struct cross_exec obj;
static struct cross_ovlhdr ovlhdr;
static int fi;

#ifdef CROSS
#define N_NAME(sp) (strtab + (int)(sp)->n_un.n_strx)
#else
#define N_NAME(sp) ((sp)->n_un.n_name)
#endif

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

int main __P((int argc, char **argv));
static long getval __P((int indx));
	
int main(argc, argv) int argc; char **argv; {
	register int i, j;
	cross_off_t size, totsize;
	int errs = 0, texterrs = 0, ninode;

	if (argc != 2) {
		fputs("usage: checksys unix-binary\n", stderr);
		exit(-1);
	}
/*
 * Can't (portably) initialize unions, so we do it at run time
*/
	for (i = 0, j = 4; i < N_NUMSYMS; i++, j += strlen(strtab + j) + 1)
#ifdef CROSS
		nl[i].n_un.n_strx = j;
#else
		nl[i].n_un.n_name = strtab + j;
#endif
	if ((fi = open(argv[1], O_RDONLY)) < 0) {
		perror(argv[1]);
		exit(-1);
	}
	if (read(fi, &obj, sizeof(obj)) != sizeof(obj)) {
		fputs("checksys: can't read object header.\n", stderr);
		exit(-1);
	}
#ifdef CROSS
	obj.a_magic = cross_read_int((char *)&obj.a_magic);
	obj.a_text = cross_read_uint((char *)&obj.a_text);
	obj.a_data = cross_read_uint((char *)&obj.a_data);
	obj.a_bss = cross_read_uint((char *)&obj.a_bss);
	obj.a_syms = cross_read_uint((char *)&obj.a_syms);
	obj.a_entry = cross_read_uint((char *)&obj.a_entry);
	obj.a_unused = cross_read_uint((char *)&obj.a_unused);
	obj.a_flag = cross_read_uint((char *)&obj.a_flag);
#endif
	if (obj.a_magic == A_MAGIC5 || obj.a_magic == A_MAGIC6) {
		if (read(fi, &ovlhdr, sizeof(ovlhdr)) != sizeof(ovlhdr)) {
			fputs("checksys: can't read overlay header.\n", stderr);
			exit(-1);
		}
#ifdef CROSS
		ovlhdr.max_ovl = cross_read_int((char *)&ovlhdr.max_ovl);
		for (i = 0; i < NOVL; ++i)
			ovlhdr.ov_siz[i] = cross_read_uint((char *)(ovlhdr.ov_siz + i));
#endif
	}
	switch(obj.a_magic) {

	/*
	 *	0407-- nonseparate I/D "vanilla"
	 */
	case A_MAGIC1:
	case A_MAGIC2:
		size = obj.a_text + obj.a_data + obj.a_bss;
		if (size > KB(48)) {
			printf("total size is %ld, max is %u, too large by %ld bytes.\n", size, KB(48), size - KB(48));
			errs++;
		}
		totsize = cround(size);
		break;

	/*
	 *	0411-- separate I/D
	 */
	case A_MAGIC3:
		size = obj.a_data + obj.a_bss;
		if (size > KB(48)) {
			printf("data is %ld, max is %u, too large by %ld bytes.\n", size, KB(48), size - KB(48));
			errs++;
		}
		totsize = obj.a_text + cround(size);
		break;

	/*
	 *	0430-- overlaid nonseparate I/D
	 */
	case A_MAGIC5:
		if (obj.a_text > KB(16)) {
			printf("base segment is %u, max is %u, too large by %u bytes.\n", obj.a_text, KB(16), obj.a_text - KB(16));
			errs++;
			texterrs++;
		}
		if (obj.a_text <= KB(8)) {
			printf("base segment is %u, minimum is %u, too small by %u bytes.\n", obj.a_text, KB(8), KB(8) - obj.a_text);
			errs++;
		}
		size = obj.a_data + obj.a_bss;
		if (size > KB(24)) {
			printf("data is %ld, max is %u, too large by %ld bytes.\n", size, KB(24), size - KB(24));
			errs++;
		}
		/*
		 *  Base and overlay 1 occupy 16K and 8K of physical
		 *  memory, respectively, regardless of actual size.
		 */
		totsize = KB(24) + cround(size);
		/*
		 *  Subtract the first overlay, it will be added below
		 *  and it has already been included.
		 */
		totsize -= ovlhdr.ov_siz[0];
		goto checkov;
		break;

	/*
	 *	0431-- overlaid separate I/D
	 */
	case A_MAGIC6:
		if (obj.a_text > KB(56)) {
			printf("base segment is %u, max is %u, too large by %u bytes.\n", obj.a_text, KB(56), obj.a_text - KB(56));
			errs++;
		}
		if (obj.a_text <= KB(48)) {
			printf("base segment is %u, min is %u, too small by %u bytes.\n", obj.a_text, KB(48), KB(48) - obj.a_text);
			errs++;
		}
		size = obj.a_data + obj.a_bss;
		if (size > KB(48)) {
			printf("data is %ld, max is %u, too large by %ld bytes.\n", size, KB(48), size - KB(48));
			errs++;
		}
		totsize = obj.a_text + cround(size);

checkov:
		for (i = 0;i < NOVL;i++) {
			totsize += ovlhdr.ov_siz[i];
			if (ovlhdr.ov_siz[i] > KB(8)) {
				printf("overlay %d is %u, max is %u, too large by %u bytes.\n", i + 1, ovlhdr.ov_siz[i], KB(8), ovlhdr.ov_siz[i] - KB(8));
				errs++;
				texterrs++;
			}
			/*
			 * check for zero length overlay in the middle of
			 * non-zero length overlays (boot croaks when faced
			 * with this) ...
			 */
			if (i < NOVL-1
			    && ovlhdr.ov_siz[i] == 0
			    && ovlhdr.ov_siz[i+1] > 0) {
				printf("overlay %d is empty and there are non-empty overlays following it.\n", i + 1);
				errs++;
				texterrs++;
			}
		}
		break;

	default:
		printf("magic number 0%o not recognized.\n", obj.a_magic);
		exit(-1);
	}

#ifdef CROSS
	(void)cross_nlist(argv[1], nl, strtab);
#else
	(void)cross_nlist(argv[1], nl);
#endif

	if (!nl[N_NINODE].n_type) {
		puts("\"ninode\" not found in namelist.");
		exit(-1);
	}
	ninode = getval(N_NINODE);
	if (!texterrs)
		{
		if (nl[N_PROC].n_value >= 0120000) {
			printf("The remapping area (0120000-0140000, KDSD5)\n\tcontains data other than the proc, text and file tables.\n\tReduce other data by %u bytes.\n", nl[N_PROC].n_value - 0120000);
			errs++;
			}
		}
	totsize += (getval(N_NBUF) * MAXBSIZE);
	if (nl[N_CLIST].n_value)
		totsize += cround(getval(N_CLIST) * (long)CBLOCK_SIZE);
	if (nl[N_RAM].n_type)
		totsize += getval(N_RAM)*512;
	if (nl[N_QUOTDESC].n_type)
		totsize += 8192;
	if (nl[N_XITDESC].n_type)
		totsize += (ninode * 3 * sizeof (long));
	if (nl[N_NAMECACHE].n_type)
		totsize += (ninode * NAMECACHE_SIZE);
	if (nl[N_IOSIZE].n_type)
		totsize += getval(N_IOSIZE);
	if (nl[N_NLOG].n_type)
		totsize += (getval(N_NLOG) * MSG_BSIZE);
	totsize += ctob(USIZE);
	printf("System will occupy %ld bytes of memory (including buffers and clists).\n", totsize);
	for (i = 0; i < N_NUMSYMS; i++) {
		if (!(i % 3))
			putchar('\n');
		printf("\t%10.10s {0%06o}", N_NAME(nl+i)+1, nl[i].n_value);
	}
	putchar('\n');
	if (errs)
		puts("**** SYSTEM IS NOT BOOTABLE. ****");
	exit(errs);
}

/*
 *  Get the value of an initialized variable from the object file.
 */
static long getval(indx) int indx; {
	register int novl;
	cross_u_int ret;
	cross_off_t offst;

	if ((nl[indx].n_type&N_TYPE) == N_BSS)
		return((long)0);
	offst = nl[indx].n_value;
	offst += obj.a_text;
	offst += sizeof(obj);
	if (obj.a_magic == A_MAGIC2 || obj.a_magic == A_MAGIC5)
		offst -= (cross_off_t)round(obj.a_text);
	if (obj.a_magic == A_MAGIC5 || obj.a_magic == A_MAGIC6) {
		offst += sizeof ovlhdr;
		if (obj.a_magic == A_MAGIC5)
			offst -= (cross_off_t)round(ovlhdr.max_ovl);
		for (novl = 0;novl < NOVL;novl++)
			offst += (cross_off_t)ovlhdr.ov_siz[novl];
	}
	(void)lseek(fi, (off_t)offst, L_SET);
	read(fi, &ret, sizeof(ret));
#ifdef CROSS
	return((long)cross_read_uint((char *)&ret));
#else
	return((long)ret);
#endif
}
