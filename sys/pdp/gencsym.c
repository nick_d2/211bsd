/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)genassym.c	1.1 (2.10BSD Berkeley) 6/12/88
 */

#include <stdio.h>
#include <stdlib.h>
#include "param.h"
#include "user.h"
#include "file.h"
#include "ioctl.h"
#include "clist.h"
#include "namei.h"
#include "msgbuf.h"

/* for offsetof() macro: */
/*#include "stddef.h"*/

struct item {
	char *format;
	int value;
} items[] = {
	{"#define MAXBSIZE %d\n", MAXBSIZE},
	{"#define CBLOCK_SIZE %d\n", sizeof(struct cblock)},
	{"#define NAMECACHE_SIZE %d\n", sizeof(struct namecache)},
	{"#define MSG_BSIZE %d\n", MSG_BSIZE},
	{"#define USIZE %d\n", USIZE}
};

/* for compatibility with old genassym.c, not used at the moment: */
int main() {
	int i;

	for (i = 0; i < sizeof(items) / sizeof(struct item); ++i)
		printf(items[i].format, items[i].value);
	exit(0);
}
