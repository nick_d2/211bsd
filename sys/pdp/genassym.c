/*
 * Copyright (c) 1986 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 *
 *	@(#)genassym.c	1.1 (2.10BSD Berkeley) 6/12/88
 */

#include <stdio.h>
#include <stdlib.h>

#include "param.h"
#include "../machine/seg.h"

#include "user.h"
#include "inode.h"
#include "mbuf.h"
#include "buf.h"
#include "proc.h"
#include "errno.h"
#include "reboot.h"
#include "syscall.h"
#include "vm.h"
#include "dz.h"
#include "../net/netisr.h"

/* for offsetof() macro: */
#include "stddef.h"

struct item {
	char *format;
	int value;
} items[] = {
	{"#define B_ADDR %o\n", offsetof(struct buf, b_un.b_addr)},
	{"#define B_XMEM %o\n", offsetof(struct buf, b_xmem)},

	{"#define SE_ADDR %o\n", offsetof(segm, se_addr)},
	{"#define SE_DESC %o\n", offsetof(segm, se_desc)},

	{"#define U_AR0 %o\n", offsetof(struct user, u_ar0)},
	{"#define U_CUROV %o\n", offsetof(struct user, u_ovdata.uo_curov)},
	{"#define U_FPERR %o\n", offsetof(struct user, u_fperr)},
	{"#define U_FPREGS %o\n", offsetof(struct user, u_fps.u_fpregs[0])},
	{"#define U_FPSR %o\n", offsetof(struct user, u_fps.u_fpsr)},
	{"#define U_OVBASE %o\n", offsetof(struct user, u_ovdata.uo_ovbase)},
	{"#define U_RU %o\n", offsetof(struct user, u_ru)},
	{"#define U_PROCP %o\n", offsetof(struct user, u_procp)},
	{"#define U_SSIZE %o\n", offsetof(struct user, u_ssize)},
	{"#define U_STACK %o\n", offsetof(struct user, u_stack)},

	{"#define F_FEC %o\n", offsetof(struct fperr, f_fec)},
	{"#define F_FEA %o\n", offsetof(struct fperr, f_fea)},

	{"#define RU_OVLY %o\n", offsetof(struct k_rusage, ru_ovly)},

	{"#define V_INTR %o\n", offsetof(struct vmrate, v_intr)},
	{"#define V_SOFT %o\n", offsetof(struct vmrate, v_soft)},
	{"#define V_PDMA %o\n", offsetof(struct vmrate, v_pdma)},
	{"#define V_OVLY %o\n", offsetof(struct vmrate, v_ovly)},

	{"#define NET_SBASE [_u+%d.]\n", NET_SBASE},
	{"#define NET_STOP [_u+%d.]\n", NET_STOP},
	{"#define KERN_SBASE [_u+%d.]\n", KERN_SBASE},
	{"#define KERN_STOP [_u+%d.]\n", KERN_STOP},

	{"#define DEV_BSIZE %d.\n", DEV_BSIZE},
	{"#define EFAULT %d.\n", EFAULT},
	{"#define ENOENT %d.\n", ENOENT},
	{"#define MAXBSIZE %d.\n", MAXBSIZE},
	{"#define NETISR_IMP %d.\n", NETISR_IMP},
	{"#define NETISR_IP %d.\n", NETISR_IP},
	{"#define NETISR_NS %d.\n", NETISR_NS},
	{"#define NETISR_RAW %d.\n", NETISR_RAW},
	{"#define	NETISR_CLOCK %d.\n", NETISR_CLOCK},
	{"#define NOVL %d.\n", NOVL},
	{"#define RB_POWRFAIL %d.\n", RB_POWRFAIL},
	{"#define RB_SINGLE %d.\n", RB_SINGLE},
	{"#define SIGFPE %d.\n", SIGFPE},
	{"#define SIGILL %d.\n", SIGILL},
	{"#define SIGSEGV %d.\n", SIGSEGV},
	{"#define SIGTRAP %d.\n", SIGTRAP},
	{"#define SYS_execv %d.\n", SYS_execv},
	{"#define SYS_exit %d.\n", SYS_exit},
	{"#define USIZE %d.\n", USIZE}
};

/* for compatibility with old genassym.c, not used at the moment: */
int main() {
	int i;

	for (i = 0; i < sizeof(items) / sizeof(struct item); ++i)
		printf(items[i].format, items[i].value);
	exit(0);
}
