#!/bin/sh
ROOT="`pwd |sed -e 's/\/bin\/awk$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
LEX="$ROOT/cross/usr/bin/lex"
YACC="$ROOT/cross/usr/bin/yacc"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" LEX="$LEX" YACC="$YACC" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
