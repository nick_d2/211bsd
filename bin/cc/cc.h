#ifndef _CC_H
#define _CC_H 1

#ifndef PARAMS
#ifdef PDP11
#define PARAMS(params) ()
#else
#define PARAMS(params) params
#endif
#endif

int main PARAMS((int argc, char **argv));
int idexit PARAMS((void));
void dexit PARAMS((void));
void error PARAMS((char *s, char *x));
int getsuf PARAMS((char as[]));
char *setsuf PARAMS((char *as, int ch));
int callsys PARAMS((char *f, char **v));
int nodup PARAMS((char **l, char *os));
char *savestr PARAMS((register char *cp));
char *strspl PARAMS((char *left, char *right));

#endif
