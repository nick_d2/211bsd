#if	defined(DOSCCS) && !defined(lint)
static	char *sccsid = "@(#)size.c	4.4.1 (2.11BSD GTE) 1/1/94";
#endif

/*
 * size
 */

#include <stdio.h>
#include <stdlib.h>

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_exec exec
#define cross_off_t off_t
#define cross_ovlhdr ovlhdr
#endif

int	header;

int main(argc, argv) int argc; char **argv; {
	struct cross_exec buf;
	cross_off_t sum;
	int gorp,i;
	int err = 0;
	FILE *f;
#if 1 /*def pdp11 for cross compilation always assume target is pdp11 */
	struct cross_ovlhdr	ovlbuf;		/* overlay structure */
	cross_off_t	coresize;		/* total text size */
	short	skip;			/* skip over overlay sizes of 0 */
#endif

	if (argc==1) {
		*argv = "a.out";
		argc++;
		--argv;
	}
	gorp = argc;
	while(--argc) {
		++argv;
		if ((f = fopen(*argv, "r"))==NULL) {
			printf("size: %s not found\n", *argv);
			err++;
			continue;
		}
		if (fread((char *)&buf, sizeof(buf), 1, f) != 1)
			goto bad_read;
#ifdef CROSS
		buf.a_magic = cross_read_int((char *)&buf.a_magic);
		buf.a_text = cross_read_uint((char *)&buf.a_text);
		buf.a_data = cross_read_uint((char *)&buf.a_data);
		buf.a_bss = cross_read_uint((char *)&buf.a_bss);
		buf.a_syms = cross_read_uint((char *)&buf.a_syms);
		buf.a_entry = cross_read_uint((char *)&buf.a_entry);
		buf.a_unused = cross_read_uint((char *)&buf.a_unused);
		buf.a_flag = cross_read_uint((char *)&buf.a_flag);
#endif
		if (N_BADMAG(buf)) {
		bad_read:
			printf("size: %s not an object file\n", *argv);
			fclose(f);
			err++;
			continue;
		}
		if (header == 0) {
			printf("text\tdata\tbss\tdec\thex\n");
			header = 1;
		}
		printf("%u\t%u\t%u\t", buf.a_text,buf.a_data,buf.a_bss);
		sum = (cross_off_t) buf.a_text + (cross_off_t) buf.a_data + (cross_off_t) buf.a_bss;
		printf("%ld\t%lx", sum, sum);
		if (gorp>2)
			printf("\t%s", *argv);
#if 1 /*def pdp11 for cross compilation always assume target is pdp11 */
		if (buf.a_magic == A_MAGIC5 || buf.a_magic == A_MAGIC6) {
			if (fread(&ovlbuf,sizeof(ovlbuf),1,f) != 1)
			{
				printf("size: %s not an overlay file\n", *argv);
				err++;
				continue;
			}
#ifdef CROSS
			ovlbuf.max_ovl = cross_read_int((char *)&ovlbuf.max_ovl);
			for (i = 0; i < NOVL; ++i)
				ovlbuf.ov_siz[i] = cross_read_uint((char *)(ovlbuf.ov_siz + i));
#endif
			coresize = buf.a_text;
			for (i = 0; i < NOVL; i++)
				coresize += ovlbuf.ov_siz[i];
			printf("\ttotal text: %ld\n\toverlays: ", coresize);
			for (i = 0,skip = 0; i < NOVL; i++) {
				if (!ovlbuf.ov_siz[i]) {
					++skip;
					continue;
				}
				for (;skip;--skip)
					fputs(",0",stdout);
				if (i > 0)
					putchar(',');
				printf("%u", ovlbuf.ov_siz[i]);
			}
		}
#endif
		printf("\n");
		fclose(f);
	}
	exit(err);
}
