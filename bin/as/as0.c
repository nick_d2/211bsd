/* as0.c */

#include <stdio.h> /* temp */
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#ifdef pdp11
typedef int intptr_t;
#endif
#include "as0.h"
#include "as2.h"

/*	.globl	_main, _write, _close, _execl, __exit, _brk */
/*	.globl	_read, _signal, _stat, _open, _mkstemp, _calloc, _realloc */

/*	.globl	error, errore, errora, checkeos, pass1, aexit, argb */
/*	.globl	overlaid, defund, a.outp, errflg, passno, filerr, outmod */
/*	.globl	wrterr, argb, hshsiz, dot, dotdot, savdot, ch, outbuf */
/*	.globl	line, savop, inbuf, fbptr, fbtbl, symnum, hshtab, symblk */
/*	.globl	symleft, dotrel, symtab, fin, fout, curfb, nxtfb, ibufp */
/*	.globl	ibufc, a.tmp1, usymtab, SYMENTSZ, SYMBLKSZ, PSTENTSZ */
/*	.globl 	obufp, Newsym, symbol,csv */

/* This assembler supports _both_ the old style object files (with */
/* fixed 8 character symbols) and the new style (with a strings table). */
/* The variable 'Newsym' defined below specifies which format will be */
/* emitted, if the value is '0' the old style is output, if non-zero */
/* then the new 'string table' format is output. */

/* The old style on disc symbol table entries looked like this: */
/*   struct symbol */
/*	{ */
/*	char name[8]; */
/*	short type; */
/*	short value; */
/*	}; */

/* The new style of on disc symbol table entry looks like: */
/*   struct symbol */
/*	{ */
/*	off_t offset_in_string_table; */
/*	short type; */
/*	short value; */
/*	}; */

/*	.data */
/* Newsym:	1 */
/*	.text */

char Newsym = 1;

struct symbol *pdot = symtab;
struct symbol *pdotdot = symtab + 1;
intptr_t numbertoken; /* r0 */
int numbervalue; /* r0 */
int endflag; /* r1 */
int indirect; /* r0 */
int optoken; /* (sp) */
int rightflags; /* r0 */
int rightvalue; /* r1 */
int leftvalue; /* r2 */
int leftflags; /* r3 */
intptr_t token; /* r4 */

/* PDP-11 assembler */
/* _main: */
/*	jsr	r5,csv */

int main(argc, argv) int argc; char **argv; {
	sig_t oldsig;
	char *p;
	struct symbol *psymbol;
	int hash, c;
	struct symbol **ppsymbol;

/*	mov	$1,-(sp)		/ signal(SIGINT, SIG_IGN) */
/*	mov	$2,-(sp) */
/*	jsr	pc,_signal */
/*	cmp	(sp)+,(sp)+ */
/*	ror	r0 */
/*	bcs	1f */

/*	mov	$aexit,-(sp)		/ signal(SIGINT, aexit) */
/*	mov	$2,-(sp) */
/*	jsr	pc,_signal */
/*	cmp	(sp)+,(sp)+ */

	oldsig = signal(SIGINT, SIG_IGN);
	if (oldsig != SIG_IGN) /*((int)oldsig & 1) == 0)*/
		signal(SIGINT, (sig_t)aexit);

/* 1: */
/*	mov	4(r5),r0		/ argc */
/*	mov	6(r5),curarg		/ argv */

	curarg = argv;
	nargs = argc;
	while (nargs) {

/* 9: */
/*	dec	r0			/ argc-- */
/*	add	$2,curarg		/ argv++ */

		curarg++;
		nargs--;
		if (nargs == 0)
			break;

/* 1: */
/*	mov	*curarg,r1 */

		p = *curarg;

/*	cmpb	(r1)+,$'- */
/*	bne	1f */

		if (*p++ != '-')
			break;

/*	cmpb	(r1),$'-		/ is this "--"? */
/*	bne	8f			/ no - br */
/*	tstb	1(r1)			/ check for null terminator */
/*	beq	1f			/ got it, the "--" means read 'stdin' */

		if (*p == '-' && p[1] == 0)
			break;

/* 8: */
/*	add	$2,curarg		/ argv++ */
/*	dec	r0			/ argc-- */
/*	cmpb	(r1),$'u */
/*	beq	3f */
/*	cmpb	(r1), $'V */
/*	bne	2f */
/*	inc	overlaid */
/*	br	1b */
/* 2: */

		if (*p == 'V') {
			overlaid = 1;
			continue;
		}

/*	tstb	(r1) */
/*	bne	2f */
/* 3: */
/*	mov	$40,defund */
/*	br	1b */
/* 2: */

		if (*p == 'u' || *p == 0) {
			defund = FGLOBAL;
			continue;
		}

/*	cmpb	(r1),$'o */
/*	bne	1f */
/*	mov	*curarg,a.outp */
/*	br	9b */
/* 1: */

		if (*p == 'o') {
			curarg++;
			a_outp = *curarg;
			nargs--;
			continue;
		}
		break;
	}

/* The new object file format puts a ceiling of 32 characters on symbols. */
/* If it is desired to raise this limit all that need be done is increase */
/* the same ceiling in the C compiler and linker (ld). */

/*	tst	Newsym */
/*	beq	1f */
/*	movb	$32.,Ncps */
/* 1: */

	if (Newsym)
		Ncps = 32;

/*	mov	r0,nargs		/ # of remaining args */
/*	bne	8f			/ br if any left */
/*	inc	nargs			/ fake at least one arg */
/*	mov	$dash, curarg		/ of '-' so we read stdin */
/* 8: */

	if (nargs == 0) {
		nargs = 1;
		curarg = &dash;
	}

/*	mov	$a.tmp1,-(sp) */
/*	jsr	pc,_mkstemp		/ fout = mkstemp(a.tmp1); */
/*	tst	(sp)+ */
/*	mov	r0,fout */
/*	bmi	oops */

	fout = mkstemp(a_tmp1);
	if (fout < 0)
		oops();

/* the symbol table is a single linked list of dynamically allocated */
/* 'SYMBLKSZ' byte blocks.  Allocate the first one now. */
/*	mov	$SYMBLKSZ+2,-(sp)	/ symblk = calloc(1, SYMBLKSZ+2) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_calloc */
/*	cmp	(sp)+,(sp)+ */
/*	mov	r0,symblk */
/*	mov	r0,usymtab		/ pointer to first block */
/*	tst	(r0)+			/ skip link word */
/*	mov	r0,symend		/ current end in block */
/*	mov	$SYMBLKSZ,symleft	/ number of bytes left in block */

	symblk = (struct symblk *)calloc(1, sizeof(struct symblk));
	if (symblk == NULL)
		nomem(); /* don't use oops(), want to delete temp file */
	usymtab = symblk;
	symend = symblk->data;
	symleft = SYMBLKSZ;

/* The string portion of symbol table entries is now allocated dynamically. */
/* We allocate the strings in 1kb chunks to cut down the number of times */
/* the string table needs to be extended (besides, long variable names are */
/* coming real soon now). */

/* NOTE: the string blocks are linked together for debugging purposes only, */
/* nothing depends on the link. */

/*	mov	$STRBLKSZ+2,-(sp) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_calloc		/ strblk = calloc(1, STRBLKSZ+2) */
/*	/ check for failure??? */
/*	cmp	(sp)+,(sp)+ */
/*	mov	r0,strblk		/ save pointer to string block */
/*	tst	(r0)+			/ skip link word */
/*	mov	r0,strend		/ set available string pointer */
/*	mov	$STRBLKSZ,strleft	/ set amount left in block */

	strblk = (struct strblk *)calloc(1, sizeof(struct strblk));
	if (strblk == NULL)
		nomem(); /* don't use oops(), want to delete temp file */
	strend = strblk->data;
	strleft = STRBLKSZ;

/* the hash table is now dynamically allocated so that it can be */
/* reused in pass 2 and re-alloced if necessary. */
/*	mov	$2,-(sp)		/ hshtab = calloc($hshsiz, sizeof(int)) */
/*	mov	$hshsiz,-(sp) */
/*	jsr	pc,_calloc */
/*	cmp	(sp)+,(sp)+ */
/*	mov	r0, hshtab */

	hshtab = (struct symbol **)calloc(hshsiz, sizeof(struct symbol *));
	if (hshtab == NULL)
		nomem(); /* don't use oops(), want to delete temp file */

/*	mov	$symtab,r1 */

	psymbol = symtab;
	do {

/* 1: */
/*	clr	r3 */
/*	mov	(r1),r2			/ pointer to PST symbol's string */

		hash = 0;
		p = psymbol->name;

/* 2: */
/*	movb	(r2)+,r4 */
/*	beq	2f */
/*	add	r4,r3 */
/*	swab	r3 */
/*	br	2b */
/* 2: */

		while (1) {
			c = *p++;
			if (c == 0)
				break;
			hash += c;
			hash = ((hash >> 8) & 0377) | ((hash & 0377) << 8);
		}

/*	clr	r2 */
/*	div	$hshsiz,r2 */
/*	ashc	$1,r2 */
/*	add	hshtab,r3 */

		ppsymbol = hshtab + (hash % hshsiz);
		hash /= hshsiz;

/* 4: */
/*	sub	r2,r3 */
/*	cmp	r3,hshtab */
/*	bhi	3f */
/*	add	$2*hshsiz,r3 */
/* 3: */
/*	tst	-(r3) */
/*	bne	4b */
/*	mov	r1,(r3) */
/*	add	$PSTENTSZ,r1 */
/*	cmp	r1,$ebsymtab */
/*	blo	1b */

		do {
			ppsymbol -= hash;
			if (ppsymbol <= hshtab)
				ppsymbol += hshsiz;
		} while (*--ppsymbol);
		*ppsymbol = psymbol++;
	} while (psymbol < symtab + ebsymtab);

/* perform pass 0 processing */
/*	jsr	pc,pass0 */

#ifdef LISTING
	printf("pass 0\n");
#endif
	pass0();

/* flush the intermediate object file */
/*	mov	$1024.,-(sp)		/ write(fout, outbuf, 1024) */
/*	mov	$outbuf,-(sp) */
/*	mov	fout,-(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */

	write(fout, outbuf, 512 * sizeof(short));

/*	tst	errflg			/ any errors? */
/*	beq	1f			/ yes - br */
/*	jsr	pc,aexit */
/* not reached */
/* 1: */

	if (errflg)
		aexit();

/*	inc	passno			/ go from -1 to 0 */
/*	clr	line			/ reset line number */
/*	jmp	pass1			/ pass1 does both passes 1, 2, exit */

#ifdef LISTING
	printf("pass 1\n");
#endif
	passno++;
	line = 0;
	pass1();
}

/* oops: */
/*	mov	$9f-8f,-(sp)		/ write(fileno(stderr),8f,strlen()) */
/*	mov	$8f,-(sp) */
/*	mov	$2,-(sp) */
/*	jsr	pc,_write */
/*	mov	$2,(sp) */
/*	jsr	pc,__exit */
/*	.data */
/* 8: */
/*	<as: can't create tmpfile\n> */
/* 9: */
/*	.even */
/*	.text */

int oops() {
	write(2, "as: can't create tmpfile\n", 25);
	exit(2);
}

void nomem() {
	write(2, "as: out of memory\n", 18);
	aexit();
}

/* error: */

void error(code) int code; {
	int i, j;
	static char report[] = "f xxxx\n";

/*	tst	passno			/ on pass1,2 ? */
/*	bpl	errorp2 */

/* putchar('\n'); */
 fflush(stdout);
	if (passno < 0) {

/*	inc	errflg */

		errflg = 1;

/*	mov	r0,-(sp) */
/*	mov	r1,-(sp) */
/*	mov	r5,r0 */
/*	tst	*curarg */
/*	beq	1f */
/*	mov	r0,-(sp) */
/*	mov	*curarg,-(sp) */
/*	clr	*curarg */
/*	jsr	pc,filerr */
/*	tst	(sp)+ */
/*	mov	(sp)+,r0 */
/* 1: */

		if (*curarg) {
			filerr(*curarg);
			*curarg = NULL;
		}

/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */
/*	jsr	pc,errcmn */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */
/*	mov	(sp)+,r1 */
/*	mov	(sp)+,r0 */
/*	rts	pc */

	}
	else {

/* errorp2: */
/*	mov	pc,errflg */
/*	mov	$666,outmod		/ make nonexecutable */

		errflg = 2;
		outmod = 0666;

/*	mov	r3,-(sp) */
/*	mov	r2,-(sp) */
/*	mov	r1,-(sp) */
/*	mov	r0,-(sp) */
/*	tst	-(sp)			/ write(1, argb, strlen(argb)) */
/*	mov	$argb,-(sp) */
/*	mov	$1,-(sp) */
/*	mov	$argb,r1 */
/*	clr	r0 */
/* 1: */
/*	tstb	(r1)+ */
/*	beq	2f */
/*	inc	r0 */
/*	br	1b */
/* 2: */
/*	mov	r0,4(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */

		if (*argb) {
			write(1, argb, strlen(argb));
			*argb = 0; /* a nick innovation */
		}

/*	movb	12(sp),r0 */
/*	jsr	pc,errcmn */
/*	mov	(sp)+,r0 */
/*	mov	(sp)+,r1 */
/*	mov	(sp)+,r2 */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,(sp) */
/*	rts	pc */

	}

/* errcmn: */
/*	mov	line,r3 */
/*	movb	r0,9f */

	j = line;
	*report = code;

/*	mov	$9f+6,r0 */
/*	mov	$4,r1 */
/* 2: */
/*	clr	r2 */
/*	div	$10.,r2 */
/*	add	$'0,r3 */
/*	movb	r3,-(r0) */
/*	mov	r2,r3 */
/*	sob	r1,2b */

	for (i = 0; i < 4; i++) {
		report[5 - i] = (j % 10) + '0';
		j /= 10;
	}

/*	mov	$7,-(sp)		/ write(1, 9f, 7) */
/*	mov	$9f,-(sp) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */

	write (1, report, 7);

/*	rts	pc */

}

/*	.data */
/* 9:	<f xxxx\n> */
/*	.even */
/*	.text */

/* p0putw: */

void p0putw(word) int word; {

/*	tst	ifflg */
/*	beq	1f */
/*	cmp	r4,$'\n */
/*	bne	2f */
/* 1: */

/* printf("p0putw(0%o)\n", word); */
	if (ifflg == 0 || word == TNEWLINE)
		{

/*	mov	r4,*obufp */
/*	add	$2,obufp */
/*	cmp	obufp,$outbuf+1024. */
/*	blo	2f */
/*	mov	$outbuf,obufp */

		*obufp++ = word;
		if (obufp >= outbuf + 512) {
			obufp = outbuf;

/*	mov	r1,-(sp)		/ protect r1 from library */
/*	mov	$1024.,-(sp)		/ write(fout, outbuf, 1024) */
/*	mov	$outbuf,-(sp) */
/*	mov	fout,-(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */
/*	mov	(sp)+,r1 */
/*	tst	r0 */
/*	bpl	2f */
/*	jmp	wrterr */

			if (write(fout, outbuf, 512 * sizeof(short)) < 0)
				wrterr();
		}
	}

/* 2: */
/*	rts	pc */

}

/* Pass 0. */

/* pass0: */
/*	jsr	pc,p0readop */
/*	jsr	pc,checkeos */
/*		br 7f */

void pass0() {
	intptr_t tokensave;
	struct symbol *psymbol;
	int label;

	while (1) {
		p0readop();
		if (checkeos())
			goto ealoop;

/*	tst	ifflg */
/*	beq	3f */
/*	cmp	r4,$200 */
/*	blos	pass0 */
/*	cmpb	(r4),$21		/if */
/*	bne	2f */
/*	inc	ifflg */
/* 2: */
/*	cmpb	(r4),$22   		/endif */
/*	bne	pass0 */
/*	dec	ifflg */
/*	br	pass0 */
/* 3: */

		if (ifflg) {
			if (token < 0 || token >= TASCII) {
				psymbol = (struct symbol *)token;
				switch (psymbol->flags) {
				case FDOTIF:
					ifflg++;
					break;
				case FDOTENDIF:
					ifflg--;
					break;
				}
			}
			continue;
		}

/*	mov	r4,-(sp) */
/*	jsr	pc,p0readop */
/*	cmp	r4,$'= */
/*	beq	4f */
/*	cmp	r4,$': */
/*	beq	1f */
/*	mov	r4,savop */
/*	mov	(sp)+,r4 */
/*	jsr	pc,opline */
/*	br	ealoop */

		tokensave = token;
		p0readop();
		if (token == TEQUAL)
			goto equal;
		if (token == TCOLON)
			goto colon;
		savop = token;
		token = tokensave;
		p0opline();
		goto ealoop;

/* 1: */
/*	mov	(sp)+,r4 */
/*	cmp	r4,$200 */
/*	bhis	1f */
/*	cmp	r4,$1			/ digit */
/*	beq	3f */
/*	mov	$'x,r5 */
/*	jsr	pc,error */
/*	br	pass0 */
/* 1: */

	colon:
		if (tokensave >= 0 && tokensave < TASCII) {
			if (tokensave != TABS) {
				error('x');
				continue;
			}
			goto digit;
		}

/*	bitb	$37,(r4) */
/*	beq	1f */
/*	mov	$'m,r5 */
/*	jsr	pc,error */
/* 1: */

		psymbol = (struct symbol *)tokensave;
		if (psymbol->flags & 037)
			error('m');

/*	bisb	dot-2,(r4) */
/*	mov	dot,2(r4) */
/*	br	pass0 */

		psymbol->flags |= pdot->flags;
		psymbol->value = pdot->value;
		continue;

/* 3: */
/*	mov	numval,r0 */
/*	jsr	pc,fbcheck */
/*	movb	dotrel,curfbr(r0) */
/*	asl	r0 */
/*	movb	dotrel,nxtfb */
/*	mov	dot,nxtfb+2 */
/*	movb	r0,nxtfb+1 */
/*	mov	dot,curfb(r0) */

	digit:
		label = fbcheck(numval);
		curfbr[label] = pdot->flags;
		curfb[label] = pdot->value;

/*	cmp	fbfree,$4		/ room for another fb entry? */
/*	bge	5f			/ yes - br */
/*	jsr	pc,growfb		/ no - grow the table */
/* 5: */
		if (fbfree == 0)
			growfb();

/*	sub	$4,fbfree		/ four bytes less available */
/*	mov	nxtfb,*fbptr		/ store first word */
/*	add	$2,fbptr		/ advance to next */
/*	mov	nxtfb+2,*fbptr		/ store second word */
/*	add	$2,fbptr		/ point to next entry */
/*	br	pass0 */

		fbfree--;
		fbptr->name = NULL;
		fbptr->flags = pdot->flags;
		fbptr->value = pdot->value;
		fbptr->number = label;
		fbptr++;
		continue;

/* 4: */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	mov	(sp)+,r1 */
/*	cmp	r1,$200 */
/*	bhis	1f */
/*	mov	$'x,r5 */
/*	jsr	pc,error */
/* 7: */
/*	br	ealoop */

	equal:
		p0readop();
		p0expres();
		if (tokensave >= 0 && tokensave < TASCII) {
			error('x');
			goto ealoop;
		}

/* 1: */
/*	cmp	r1,$dotrel */
/*	bne	2f */
/*	bic	$40,r3 */
/*	cmp	r3,dotrel */
/*	bne	1f */
/* 2: */

		psymbol = (struct symbol *)tokensave;
		if (psymbol == pdot) {
			leftflags &= ~FGLOBAL;
/* printf("leftflags 0%o pdot->flags 0%o\n", leftflags, pdot->flags); */
			if (leftflags != pdot->flags)
				goto doterr;
		}

/*	bicb	$37,(r1) */
/*	bic	$!37,r3 */
/*	bne	2f */
/*	clr	r2 */
/* 2: */

		psymbol->flags &= ~037;
		leftflags &= 037;
		if (leftflags == 0)
			leftvalue = 0;

/*	bisb	r3,(r1) */
/*	mov	r2,2(r1) */
/*	br	ealoop */

		psymbol->flags |= leftflags;
		psymbol->value = leftvalue;
		goto ealoop;

/* 1: */
/*	mov	$'.,r5 */
/*	jsr	pc,error */
/*	movb	$2,dotrel */

	doterr:
		error('.');
		pdot->flags = FTEXT;

/* ealoop: */
/*	cmp	r4,$'; */
/*	beq	9f */
/*	cmp	r4,$'\n */
/*	bne	1f */
/*	inc	line */
/*	br	9f */
/* 1: */

	ealoop:
/* printf("ealoop token = 0x%08x\n", token); */
		if (token == TSEMICOLON)
			continue;
		if (token == TNEWLINE) {
			line++;
			continue;
		}

/*	cmp	r4,$'\e */
/*	bne	2f */
/*	tst	ifflg */
/*	beq	1f */
/*	mov	$'x,r5 */
/*	jsr	pc,error */
/* 1: */
/*	rts	pc */
/* 2: */
/*	mov	$'x,r5 */
/*	jsr	pc,error */

		if (token == TENDFILE) {
			if (ifflg)
				error('x');
			break;
		}
		error('x');

/* 2: */
/*	jsr	pc,checkeos */
/*		br 9f */
/*	jsr	pc,p0readop */
/*	br	2b */
/* 9: */
/*	jmp	pass0 */

		while (checkeos() == 0)
			p0readop();
	}
}

/* fbcheck: */
/*	cmp	r0,$9. */
/*	bhi	1f */
/*	rts	pc */
/* 1: */
/*	mov	$'f,r5 */
/*	jsr	pc,error */
/*	clr	r0 */
/*	rts	pc */

int fbcheck(label) int label; {
	if (label < 10)
		return label;
	error('f');
	return 0;
}

/* the 'fb' table never grows to be large.  In fact all of the assemblies */
/* of C compiler generated code which were processed by 'c2' never */
/* produced a table larger than 0 bytes.  So we 'realloc' because */
/* this is not done enough to matter. */

/* growfb: */
/*	mov	r1,-(sp)		/ save r1 from library */
/*	add	$256.,fbtblsz		/ new size of fb table */
/*	mov	$256.,fbfree		/ number available now */

void growfb() {
	fbtblsz += 64;
	fbfree = 64;

/*	mov	fbtblsz,-(sp)		/ fbtbl = realloc(fbtbl, fbtblsz); */
/*	mov	fbtbl,-(sp) */
/*	bne	1f			/ extending table - br */
/*	mov	$1,(sp)			/ r0 = calloc(1, fbtblsz); */
/*	jsr	pc,_calloc */
/*	br	2f */
/* 1: */
/*	jsr	pc,_realloc */
/* 2: */
/*	cmp	(sp)+,(sp)+ */
/*	mov	r0,fbtbl */

	if (fbtbl == NULL)
		fbtbl = (struct symbol *)calloc(fbtblsz, sizeof(struct symbol));
	else
		fbtbl = (struct symbol *)realloc(fbtbl, fbtblsz * sizeof(struct symbol));

/*	bne	1f */
/*	iot				/ Can never happen (I hope) */
/* 1: */

	if (fbtbl == NULL)
		nomem();

/*	add	fbtblsz,r0		/ fbptr starts 256 bytes from */
/*	sub	$256.,r0		/ end of new region */
/*	mov	r0,fbptr */
/*	mov	(sp)+,r1		/ restore register */
/*	rts	pc */

	fbptr = fbtbl + fbtblsz - 64;
}

/* Symbol table lookup and hashtable maintenance. */

/* rname: */
/*	mov	r1,-(sp) */
/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */

void rname() {
	int count;
	char *p;
	int local;
	int hash, c;
	int timesaround;
	struct symbol *psymbol;
	struct symbol **ppsymbol;
	int len;

/*	movb	Ncps,r5			/ Max num of chars to accept */
/*	mov	$symbol,r2 */
/*	clr	(r2) */

/* printf("rname()\n"); */
	count = Ncps;
	p = symbol;
	*p = 0;

/*	clr	-(sp) */
/*	clr	-(sp) */

	local = 0;
	hash = 0;

/*	cmp	r0,$'~			/ symbol not for hash table? */
/*	bne	1f			/ no - br */
/*	inc	2(sp) */
/*	clr	ch */

	if (ch == '~') {
		local = 1;
		ch = 0;
	}

/* 1: */
/*	jsr	pc,rch */
/*	movb	chartab(r0),r3 */
/*	ble	1f */
/*	add	r3,(sp) */
/*	swab	(sp) */
/*	dec	r5 */
/*	blt	1b */
/*	movb	r3,(r2)+ */
/*	br	1b */
/* 1: */

	while (1) {
		c = rch();
		if (chartab[c] <= 0)
			break;
		hash += c;
		hash = ((hash >> 8) & 0377) | ((hash & 0377) << 8);
		if (count) {
			count--;
			*p++ = c;
		}
 /* bug!! should stop accumulating hash when count == 0 */
	}

/*	clrb	(r2)+			/ null terminate string */
/*	movb	r0,ch */

	*p++ = 0;
/* printf("symbol = \"%s\"\n", symbol); */
	ch = c;
/* if (ch) printf("a push 0x%02x\n", ch); */

/*	mov	(sp)+,r1 */
/*	clr	r0 */
/*	tst	(sp)+ */
/*	beq	1f */
/*	mov	symend,r4 */
/*	br	4f			/ go insert into symtable (!hashtbl) */
/* 1: */

	if (local) {
		psymbol = isroom(symend); /* symend; */
		goto insert;
	}

/*	div	$hshsiz,r0 */
/*	ashc	$1,r0 */
/*	add	hshtab,r1 */
/*	clr	timesaround */

	ppsymbol = hshtab + (hash % hshsiz);
	hash /= hshsiz;
	timesaround = 0;

/* 1: */
/*	sub	r0,r1 */
/*	cmp	r1,hshtab */
/*	bhi	2f */
/*	add	$2*hshsiz,r1 */

	while (1) {
		ppsymbol -= hash;
		if (ppsymbol <= hshtab) {
			ppsymbol += hshsiz;

/*	tst	timesaround */
/*	beq	3f */
/*	mov	$8f-9f,-(sp)		/ write(fileno(stdout),9f,8f-9f); */
/*	mov	$9f,-(sp) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */
/*	jsr	pc,aexit */
/* not reached */

			if (timesaround) {
				write(2, "as: symbol table overflow\n", 26);
				aexit();
			}

/*	.data */
/* timesaround: 0 */
/* 9: */
/*	<as: symbol table overflow\n> */
/* 8: */
/*	.even */
/*	.text */
/* 3: */
/*	inc	timesaround */

			timesaround = 1;
		}

/* 2: */
/*	mov	$symbol,r2 */
/*	mov	-(r1),r4 */
/*	beq	3f */
/*	mov	(r4)+,r3		/ ptr to symbol's name */
/* 9: */
/*	cmpb	(r2),(r3)+ */
/*	bne	1b			/ not the right symbol - br */
/*	tstb	(r2)+			/ at end of symbol? */
/*	bne	9b			/ nope - keep looking */
/*	br	1f			/ yep - br */

		psymbol = *--ppsymbol;
		if (psymbol == NULL)
			break;
		if (!strcmp(symbol, psymbol->name))
			goto found;
	}

/* 3: */
/*	mov	symend,r4 */
/*	jsr	pc,isroom		/ make sure there's room in block */
/*	mov	r4,(r1) */
/* 4: */
/*	jsr	pc,isroom		/ check for room in current block */

	psymbol = isroom(symend);
	*ppsymbol = psymbol;
insert:
	/* isroom(psymbol); */

/*	mov	$symbol,r2		/ length of string (including null) */
/* 8 : */
/*	tstb	(r2)+ */
/*	bne	8b */
/*	sub	$symbol,r2 */
/*	jsr	pc,astring		/ allocate string space */
/*	mov	r0,(r4)+		/ save string pointer in symtab entry */
/*	mov	$symbol,r1 */
/* 9: */
/*	movb	(r1)+,(r0)+		/ copy symbol name to string block */
/*	bne	9b */
/*	sub	$SYMENTSZ,symleft */

	len = strlen(symbol) + 1;
	p = astring(len);
	psymbol->name = p;
	memcpy(p, symbol, len);
	symleft--;

/* each new symbol is assigned a unique one up number.  This is done because */
/* the user symbol table is no longer contiguous - the symbol number can */
/* not be calculated by subtracting a base address and dividing by the */
/* size of a symbol. */

/*	clr	(r4)+			/ flags word */
/*	clr	(r4)+			/ value word */
/*	mov	symnum,(r4)+ */
/*	inc	symnum */
/*	mov	r4,symend */
/*	sub	$6,r4			/ point to flags word */

	psymbol->flags = 0; /* redundant */
	psymbol->value = 0; /* redundant */
	psymbol->number = symnum++;
	symend = psymbol + 1;

/* 1: */
/*	mov	r4,-(sp) */
/*	mov	r4,r3 */
/*	tst	-(r3)			/ back to beginning of entry */
/*	cmp	r3,$ebsymtab		/ Permanent Symbol Table(opcode, etc)? */
/*	blo	1f			/ yes - br */
/*	mov	6(r3),r4		/ get symbol number */
/*	add	$4000,r4		/ user symbol flag */
/*	br	2f */
/* 1: */

/* PST entries are PSTENTSZ bytes each because they do not have a 'symnum' */
/* entry associated with them. */

/*	sub	$symtab,r3 */
/*	clr	r2 */
/*	div	$PSTENTSZ,r2 */
/*	mov	r2,r4 */
/*	add	$1000,r4		/ builtin symbol */

/* 2: */
/*	jsr	pc,p0putw */

found:
	if (psymbol >= symtab + ebsymtab)
		p0putw(04000 + psymbol->number);
	else
		p0putw(01000 + (psymbol - symtab));

/*	mov	(sp)+,r4 */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */
/*	mov	(sp)+,r1 */
/*	tst	(sp)+ */
/*	rts	pc */

	token = (intptr_t)psymbol;
}

/* isroom: */
/*	cmp	symleft,$SYMENTSZ	/ room for another symbol? */
/*	bge	1f			/ yes - br */

struct symbol *isroom(psymbol) struct symbol *psymbol; {
	struct symblk *psymblk;

/* printf("isroom(0x%08x) symleft = %d\n", psymbol, symleft); */
	if (symleft == 0) {

/*	mov	r1,-(sp)		/ save from library */
/*	mov	$SYMBLKSZ+2,-(sp)	/ size of sym block plus link word */
/*	mov	$1,-(sp)		/ number of blocks to allocate */
/*	jsr	pc,_calloc		/ calloc(1, SYMBLKSZ+2); */
/*	cmp	(sp)+,(sp)+ */
/*	/ check for failure? */

		psymblk = (struct symblk *)calloc(1, sizeof(struct symblk));
		if (psymblk == NULL)
			nomem();

/*	mov	r0,*symblk		/ link new block to old */
/*	mov	r0,symblk		/ this is now the current block */
/*	tst	(r0)+			/ skip link word */
/*	mov	$SYMBLKSZ,symleft	/ number of bytes available */
/*	mov	r0,r4			/ put where it's expected */

		symblk->next = psymblk;
		symblk = psymblk;
		symleft = SYMBLKSZ;
		return psymblk->data;

/*	mov	(sp)+,r1		/ restore saved register */
/* 1: */
/*	rts	pc			/ return */

	}
	return psymbol;
}

/* allocate room for a string, the length is in R2 and includes room for */
/* a terminating null byte. */

/* astring: */
/*	cmp	r2,strleft		/ room in current block? */
/*	ble	1f			/ yes - go carve out a chunk */

char *astring(len) int len; {
	struct strblk *pstrblk;
	char *p;

/* printf("astring(%d) strleft = %d\n", len, strleft); */
	if (len > strleft) {

/*	mov	$STRBLKSZ+2,-(sp) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_calloc		/ symblk = calloc(1,STRBLKSZ+2) */
/*	/ check for failure? */
/*	cmp	(sp)+,(sp)+ */

		pstrblk = (struct strblk *)calloc(1, sizeof(struct strblk));
		if (pstrblk == NULL)
			nomem();

/*	mov	r0,*strblk		/ update forward link between blocks */
/*	mov	r0,strblk		/ update current string block pointer */
/*	tst	(r0)+			/ skip link word */
/*	mov	r0,strend		/ current data pointer */
/*	mov	$STRBLKSZ,strleft	/ amount of space left */

		strblk->next = pstrblk;
		strblk = pstrblk;
		strleft = STRBLKSZ;
		strend = strblk->data;

/* 1: */
/*	mov	strend,r0		/ string address */
/*	add	r2,strend		/ update current end point */
/*	sub	r2,strleft		/ update amount of space left */
/*	rts	pc */

	}
	p = strend;
	strend += len;
	strleft -= len;
	return p;
}

/* number: */
/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */
/*	mov	r5,-(sp) */
/*	clr	r1 */
/*	clr	r5 */

int number() {
	int octal;
	int decimal;
	int c;

	octal = 0;
	decimal = 0;

/* 1: */
/*	jsr	pc,rch */
/*	cmp	r0,$'0 */
/*	blt	1f */
/*	cmp	r0,$'9 */
/*	bgt	1f */
/*	sub	$'0,r0 */
/*	mul	$10.,r5 */
/*	add	r0,r5 */
/*	ash	$3,r1 */
/*	add	r0,r1 */
/*	br	1b */

	while (1) {
		c = rch();
		if (c < '0' || c > '9')
			break;
		c -= '0';
		decimal = decimal * 10 + c;
		octal = octal * 8 + c;
	}

/* 1: */
/*	cmp	r0,$'b */
/*	beq	1f */
/*	cmp	r0,$'f */
/*	beq	1f */
/*	cmp	r0,$'. */
/*	bne	2f */
/*	mov	r5,r1 */
/*	clr	r0 */
/* 2: */

	if (c == 'b' || c == 'f')
		goto label;
	if (c == '.') {
		octal = decimal;
		c = 0;
	}

/*	movb	r0,ch */
/*	mov	r1,r0 */

	ch = c;
/* if (ch) printf("b push 0x%02x\n", ch); */
	numbervalue = octal;

/*	mov	(sp)+,r5 */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */
/*	rts	pc */

	return 1;

/* 1: */
/*	mov	r0,r3 */
/*	mov	r5,r0 */
/*	jsr	pc,fbcheck */
/*	add	$141,r0 */
/*	cmp	r3,$'b */
/*	beq	1f */
/*	add	$10.,r0 */
/* 1: */

label:
	numbertoken = fbcheck(decimal) + TLABEL;
	if (c != 'b')
		numbertoken += 10;

/*	mov	r0,r4 */
/*	mov	(sp)+,r5 */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */
/*	add	$2,(sp) */
/*	rts	pc */

	return 0;
}

/* rch: */
/*	movb	ch,r0 */
/*	beq	1f */
/*	clrb	ch */
/*	rts	pc */

int rch() {
	int c;
	char *p;

/* printf("rch() ch = 0x%02x\n", ch); */
	if (ch) {
		c = ch;
		ch = 0;
		return c;
	}

/* 1: */
/*	dec	ibufc */
/*	blt	2f */
/*	movb	*ibufp,r0 */
/*	inc	ibufp */
/*	bic	$!177,r0 */
/*	beq	1b */
/*	rts	pc */

	while (1) {
		ibufc--;
		if (ibufc < 0)
			goto refill;
		c = *p0ibufp++ & 0177;
/* putchar(c); */
		if (c == 0)
			continue;
		return c;

/* 2: */
/*	mov	fin,r0 */
/*	bmi	3f */

	refill:
		if (fin >= 0) {

/*	mov	r1,-(sp)		/ protect r1 from library */
/*	mov	$1024.,-(sp)		/ read(fin, inbuf, 1024) */
/*	mov	$inbuf,-(sp) */
/*	mov	r0,-(sp) */
/*	jsr	pc,_read */
/*	add	$6,sp */
/*	mov	(sp)+,r1 */

			ibufc = read(fin, inbuf, 1024);

/*	tst	r0 */
/*	ble	2f */
/*	mov	r0,ibufc */
/*	mov	$inbuf,ibufp */
/*	br	1b */
/* 2: */

			if (ibufc > 0) {
				p0ibufp = inbuf;
				continue;
			}

/*	mov	r1,-(sp)		/ protect r1 from library */
/*	mov	fin,-(sp)		/ close(r0) */
/*	jsr	pc,_close */
/*	tst	(sp)+ */
/*	mov	$-1,fin */
/*	mov	(sp)+,r1 */

			close(fin);
			fin = -1;
		}

/* 3: */
/*	dec	nargs */
/*	bge	2f */
/*	mov	$'\e,r0 */
/*	rts	pc */
/* 2: */

		if (nargs == 0)
			return 004;
		nargs--;

/*	tst	ifflg */
/*	beq	2f */
/*	mov	$'i,r5 */
/*	jsr	pc,error */
/*	jsr	pc,aexit */
/* not reached */
/* 2: */

		if (ifflg) {
			error('i');
			aexit();
		}

/* check for the filename arguments of "-" or "--", these mean to read 'stdin'. */
/* Additional filenames are permitted and will be processed when EOF */
/* is detected on stdin. */
/*	mov	*curarg,r0 */
/*	cmpb	(r0)+,$'- */
/*	bne	5f			/ not the special case - br */
/*	tstb	(r0)			/ must be '-' by itself */
/*	beq	4f */
/*	cmpb	(r0)+,$'-		/ check for "--" */
/*	bne	5f			/ not a double -, must be a filename */
/*	tstb	(r0)			/ null terminated? */
/*	bne	5f			/ no - must be a filename */
/* 4: */
/*	clr	fin			/ file descriptor is 0 for stdin */
/*	br	2f */
/* 5: */

		p = *curarg;
		if (*p++ == '-' && (*p == 0 || (*p++ == '-' && *p == 0))) {
			fin = 0;
			goto reset;
		}

/*	mov	r1,-(sp)		/ protect r1 from library */
/*	clr	-(sp)			/ open((r0), O_RDONLY, 0) */
/*	clr	-(sp) */
/*	mov	*curarg,-(sp) */
/*	jsr	pc,_open */
/*	add	$6,sp */
/*	mov	(sp)+,r1 */

		fin = open(*curarg, O_RDONLY);

/*	mov	r0,fin */
/*	bpl	2f */
/*	mov	*curarg,-(sp) */
/*	jsr	pc,filerr */
/*	tst	(sp)+ */
/*	jsr	pc,aexit */
/*not reached */

		if (fin < 0) {
			filerr(*curarg);
			aexit();
		}

/* 2: */
/*	mov	$1,line */
/*	mov	r4,-(sp) */
/*	mov	r1,-(sp) */
/*	mov	$5,r4 */
/*	jsr	pc,p0putw */

	reset:
		line = 1;
		token = TNEWFILE;
		p0putw(token);

/*	mov	*curarg,r1 */
/* 2: */
/*	movb	(r1)+,r4 */
/*	beq	2f */
/*	jsr	pc,p0putw */
/*	br	2b */
/* 2: */
/*	add	$2,curarg */
/*	mov	$-1,r4 */
/*	jsr	pc,p0putw */

		p = *curarg++;
		while (1) {
			c = *p++;
			if (c == 0)
				break;
			p0putw(c);
		}
		p0putw(-1);

/*	mov	(sp)+,r1 */
/*	mov	(sp)+,r4 */
/*	br	1b */

	}
}

/* p0readop: */
/*	mov	savop,r4 */
/*	beq	1f */
/*	clr	savop */
/*	rts	pc */
/* 1: */
/*	jsr	pc,8f */
/*	jsr	pc,p0putw */
/*	rts	pc */

void p0readop() {
	int c;

/* printf("p0readop() savop = 0x%08x\n", savop); */
	if (savop) {
		token = savop;
		savop = 0;
		return;
	}

/* 8: */
/*	jsr	pc,rch */
/*	mov	r0,r4 */
/*	movb	chartab(r0),r1 */
/*	bgt	rdname */
/*	jmp	*1f-2(r1) */

	while (1) {
		c = rch();
		switch (chartab[c]) {
		default:
			rdname(c);
			return;


/*	.data */
/*	fixor */

		case -026:
#if 1 /* modifications for dec syntax */
			if (c == '#')
				retread(TDOLL);
			else if (c == '@')
				retread(TSTAR);
			else
#endif
			fixor();
			return;

/*	escp */

		case -024:
			escp();
			return;

/*	8b */

		case -022:
			break;

/*	retread */

		case -020:
			retread(c);
			return;

/*	dquote */

		case -016:
			dquote();
			return;

/*	garb */

		case -014:
			garb();
			break;

/*	squote */

		case -012:
			squote();
			return;

/*	rdname */

		case -010:
			rdname(c);
			return;

/*	skip */

		case -006:
			skip();
			return;

/*	rdnum */

		case -004: /* can never get here */
			rdnum();
			return;

/*	retread */

		case -002:
			retread(c);
			return;

/*	string */

		case -000:
			string();
			return;

/* 1: */
/*	.text */

		}
	}
}

/* escp: */
/*	jsr	pc,rch */
/*	mov	$esctab,r1 */
/* 1: */
/*	cmpb	r0,(r1)+ */
/*	beq	1f */
/*	tstb	(r1)+ */
/*	bne	1b */
/*	rts	pc */
/* 1: */
/*	movb	(r1),r4 */
/*	rts	pc */

void escp() {
	int c;

	c = rch();
	switch (c) {

/*	.data */
/* esctab: */
/*	.byte '/, '/ */
/*	.byte '\<, 035 */

	case '<':
		retread(TLSH);
		return;

/*	.byte '>, 036 */

	case '>':
		retread(TRSH);
		return;

/*	.byte '%, 037 */

	case '%':
		retread(TOR);
		return;

/*	.byte 0, 0 */
/*	.text */

	}
	retread(c);
}

/* fixor: */
/*	mov	$037,r4 */
/* retread: */
/*	rts	pc */

void fixor() {
	retread(TOR);
}

void retread(c) int c; {
	token = c;
	p0putw(token);
}

/* rdname: */
/*	movb	r0,ch */
/*	cmp	r1,$'0 */
/*	blo	1f */
/*	cmp	r1,$'9 */
/*	blos	rdnum */
/* 1: */
/*	jmp	rname */

void rdname(c) int c; {
	ch = c;
/* if (ch) printf("c push 0x%02x\n", ch); */
	if (c >= '0' && c <= '9')
		rdnum();
	else
		rname();
}

/* rdnum: */
/*	jsr	pc,number */
/*		br 1f */
/*	rts	pc */

void rdnum() {
	if (number())
		retnum(numbervalue);
	else
		retread(numbertoken);
}

/* squote: */
/*	jsr	pc,rsch */
/*	br	1f */

void squote() {
	retnum(rsch());
}

/* dquote: */
/*	jsr	pc,rsch */
/*	mov	r0,-(sp) */
/*	jsr	pc,rsch */
/*	swab	r0 */
/*	bis	(sp)+,r0 */

void dquote() {
	int c;

	c = rsch();
	retnum(rsch() | (c << 8));
}

/* 1: */
/*	mov	r0,numval */
/*	mov	$1,r4 */
/*	jsr	pc,p0putw */
/*	mov	numval,r4 */
/*	jsr	pc,p0putw */
/*	mov	$1,r4 */
/*	tst	(sp)+ */
/*	rts	pc */

void retnum(value) int value; {
	numval = value;
	token = TABS;
	p0putw(token);
	p0putw(numval);
}

/* skip: */
/*	jsr	pc,rch */
/*	mov	r0,r4 */
/*	cmp	r0,$'\e */
/*	beq	1f */
/*	cmp	r0,$'\n */
/*	bne	skip */
/* 1: */
/*	rts	pc */

void skip() {
	int c;

	do {
		c = rch();
		if (c == '\e') {
			retread(TENDFILE);
			return;
		}
	} while (c != '\n');
	retread(TNEWLINE);
}

/* garb: */
/*	mov	$'g,r5 */
/*	jsr	pc,error */
/*	br	8b */

void garb() {
	error('g');
}

/* string: */
/*	mov	$'<,r4 */
/*	jsr	pc,p0putw */

void string() {
	int c;

	token = TSTRING;
	p0putw(token);

/*	clr	numval */
/* 1: */
/*	jsr	pc,rsch */
/*	tst	r1 */
/*	bne	1f */
/*	mov	r0,r4 */
/*	bis	$400,r4 */
/*	jsr	pc,p0putw */
/*	inc	numval */
/*	br	1b */
/* 1: */

	numval = 0;
	while (1) {
		c = rsch();
		if (endflag)
			break;
		p0putw(0400 | c);
		numval++;
	}

/*	mov	$-1,r4 */
/*	jsr	pc,p0putw */
/*	mov	$'<,r4 */
/*	tst	(sp)+ */
/*	rts	pc */

	p0putw(-1);
}

/* rsch: */
/*	jsr	pc,rch */

int rsch() {
	int c;

	c = rch();

/*	cmp	r0,$'\e */
/*	beq	4f */
/*	cmp	r0,$'\n */
/*	beq	4f */

	endflag = 0;
	if (c != 004 && c != '\n') {

/*	clr	r1 */
/*	cmp	r0,$'\\ */
/*	bne	3f */
/*	jsr	pc,rch */

		if (c == '\\') {
			c = rch();

/*	mov	$schar,r2 */
/* 1: */
/*	cmpb	(r2)+,r0 */
/*	beq	2f */
/*	tstb	(r2)+ */
/*	bpl	1b */
/*	rts	pc */
/* 2: */
/*	movb	(r2)+,r0 */
/*	clr	r1 */
/*	rts	pc */

			switch (c) {
			case 'n':
				return 012;
			case 's':
				return 040;
			case 't':
				return 011;
			case 'e':
				return 004;
			case '0':
				return 000;
			case 'r':
				return 015;
			case 'a':
				return 006;
			case 'p':
				return 033;
			}
			return c;

/* 3: */
/*	cmp	r0,$'> */
/*	bne	1f */
/*	inc	r1 */
/* 1: */
/*	rts	pc */

		}
		if (c == '>')
			endflag = 1;
		return c;

/* 4: */
/*	mov	$'<,r5 */
/*	jsr	pc,error */
/*	jsr	pc,aexit */
/* not reached */

	}
	error('<');
	aexit();
	return 0; /* keep compiler happy */
}

/*	.data */
/* schar: */
/*	.byte 'n, 012 */
/*	.byte 's, 040 */
/*	.byte 't, 011 */
/*	.byte 'e, 004 */
/*	.byte '0, 000 */
/*	.byte 'r, 015 */
/*	.byte 'a, 006 */
/*	.byte 'p, 033 */
/*	.byte 0,  -1 */
/*	.text */

/* opline: */
/*	mov	r4,r0 */
/*	bmi	1f */
/*	cmp	r0,$200 */
/*	bgt	1f */
/*	cmp	r0,$'< */
/*	bne	xpr */
/*	jmp	opl17 */
/* xpr: */
/*	jsr	pc,expres */
/*	add	$2,dot */
/*	rts	pc */
/* 1: */

void p0xpr() {
	p0expres();
	pdot->value += 2;
}

void p0opline() {
	struct symbol *psymbol;
	int flags;

/* printf("p0opline() token 0x%08x\n", token); */
	if (token >= 0 && token < TASCII) {
		if (token == TSTRING)
			p0opl17();
		else
			p0xpr();
		return;
	}

/*	movb	(r4),r0 */
/*	cmp	r0,$24 */
/*	beq	xpr */
/*	cmp	r0,$5 */
/*	blt	xpr */
/*	cmp	r0,$36 */
/*	bgt	xpr */

	psymbol = (struct symbol *)token;
	flags = psymbol->flags;
	if (flags <= FBSS || flags == FREGISTER || flags > FJCOND) {
		p0xpr();
		return;
	}

/*	mov	r0,-(sp) */
/*	jsr	pc,p0readop */
/*	mov	(sp)+,r0 */
/*	asl	r0 */
/*	jmp	*1f-12(r0) */

	p0readop();
	switch (flags) {

/*	.data */
/* 1: */
/*	opl13			/ map fop freg,fdst to double */

	case FMOVFO:
		p0opl13();
		return;

/*	opl6 */

	case FBRANCH:
		p0opl6();
		return;

/*	opl7 */

	case FJSR:
		p0opl7();
		return;

/*	opl10 */

	case FRTS:
		p0opl10();
		return;

/*	opl11 */

	case FSYSTRAP:
		p0opl11();
		return;

/*	opl13			/ map fld/fst to double */
/*	opl13 */
/*	opl13			/ map fop fsrc,freg to double */

	case FMOVF:
	case FDOUBLE:
	case FFLOP:
		p0opl13();
		return;

/*	opl15 */

	case FSINGLE:
		p0opl15();
		return;

/*	opl16 */

	case FDOTBYTE:
		p0opl16();
		return;

#if 1 /* modifications for dec syntax */
	case FDOTWORD:
		p0opldotword();
		return;
#endif

/*	opl17 */

	case FSTRING:
		p0opl17();
		return;

/*	opl20 */

	case FDOTEVEN:
		p0opl20();
		return;

/*	opl21 */

	case FDOTIF:
		p0opl21();
		return;

/*	opl22 */

	case FDOTENDIF:
		p0opl22();
		return;

/*	opl23 */

	case FDOTGLOBL:
		p0opl23();
		return;

/*	xpr */

	case FREGISTER: /* can never get here */
		p0xpr();
		return;

/*	opl25 */

	case FDOTTEXT:
		p0opl25();
		return;

/*	opl26 */

	case FDOTDATA:
		p0opl26();
		return;

/*	opl27 */

	case FDOTBSS:
		p0opl27();
		return;

/*	opl13  			/ map mul s,r to double */

	case FMULDIV:
		p0opl13();
		return;

/*	opl31 */

	case FSOB:
		p0opl31();
		return;

/*	opl32 */

	case FDOTCOMM:
		p0opl32();
		return;

/*	xpr */
/*	xpr */

	case FESTTEXT:
	case FESTDATA:
		p0xpr();
		return;

/*	opl35 */

	case FJBR:
		p0opl35();
		return;

/*	opl36 */

	case FJCOND:
		p0opl36();
		return;

/*	.text */

	}
}

/* opl35:					/ jbr */
/*	mov	$4,-(sp) */
/*	br	1f */

void p0opl35() {
	relative(4);
}

/* opl36:					/ jeq, etc */
/*	mov	$6,-(sp) */

void p0opl36() {
	relative(6);
}

/* 1: */
/*	jsr	pc,expres */
/*	cmp	r3,dotrel */
/*	bne	1f */
/*	sub	dot,r2 */
/*	bge	1f */
/*	cmp	r2,$-376 */
/*	blt	1f */
/*	mov	$2,(sp) */
/* 1: */
/*	add	(sp)+,dot */
/*	rts	pc */

void relative(size) int size; {
	p0expres();
	if (leftflags == pdot->flags) {
		leftvalue -= pdot->value;
		if (leftvalue < 0 && leftvalue >= -0376)
			size = 2;
	}
	pdot->value += size;
}

/* opl13: */

void p0opl13() {
	p0opl7();
}

/* opl7:					/double */
/*	jsr	pc,addres */

void p0opl7() {
	p0addres();

/* op2: */
/*	cmp	r4,$', */
/*	beq	1f */
/*	jsr	pc,errora */
/*	rts	pc */
/* 1: */
/*	jsr	pc,p0readop */

	if (token != TCOMMA) {
		error('a');
		return;
	}
	p0readop();
	p0opl15();
}

/* opl15:   				/ single operand */
/*	jsr	pc,addres */
/*	add	$2,dot */
/*	rts	pc */

void p0opl15() {
	p0addres();
	pdot->value += 2;
}

/* opl31:					/ sob */
/*	jsr	pc,expres */
/*	cmp	r4,$', */
/*	beq	1f */
/*	jsr	pc,errora */
/* 1: */
/*	jsr	pc,p0readop */

void p0opl31() {
	p0expres();
	if (token != TCOMMA) {
		error('a');
		return;
	}
	p0readop();
	p0opl11();
}

/* opl6: */

void p0opl6() {
	p0opl11();
}

/* opl10: */

void p0opl10() {
	p0opl11();
}

/* opl11:					/branch */
/*	jsr	pc,expres */
/*	add	$2,dot */
/*	rts	pc */

void p0opl11() {
	p0expres();
	pdot->value += 2;
}

/* opl16:					/ .byte */
/*	jsr	pc,expres */
/*	inc	dot */
/*	cmp	r4,$', */
/*	bne	1f */
/*	jsr	pc,p0readop */
/*	br	opl16 */
/* 1: */
/*	rts	pc */

void p0opl16() {
	while (1) {
		p0expres();
		pdot->value++;
		if (token != TCOMMA)
			break;
		p0readop();
	}
}

#if 1 /* modifications for dec syntax */
void p0opldotword() {
	while (1) {
		p0expres();
		pdot->value += 2;
		if (token != TCOMMA)
			break;
		p0readop();
	}
}
#endif

/* opl17:					/ < (.ascii) */
/*	add	numval,dot */
/*	jsr	pc,p0readop */
/*	rts	pc */

void p0opl17() {
	pdot->value += numval;
	p0readop();
}

/* opl20:					/.even */
/*	inc	dot */
/*	bic	$1,dot */
/*	rts	pc */

void p0opl20() {
	pdot->value = (pdot->value + 1) & ~1;
}

/* opl21:					/.if */
/*	jsr	pc,expres */
/*	tst	r3 */
/*	bne	1f */
/*	mov	$'U,r5 */
/*	jsr	pc,error */
/* 1: */
/*	tst	r2 */
/*	bne	opl22 */
/*	inc	ifflg */
/* opl22:					/endif */
/*	rts	pc */

void p0opl21() {
	p0expres();
	if (leftflags == 0)
		error('U');
	if (leftvalue == 0)
		ifflg++;
}

void p0opl22() {
}

/* opl23:					/.globl */
/*	cmp	r4,$200 */
/*	blo	1f */
/*	bisb	$40,(r4) */
/*	jsr	pc,p0readop */
/*	cmp	r4,$', */
/*	bne	1f */
/*	jsr	pc,p0readop */
/*	br	opl23 */
/* 1: */
/*	rts	pc */

void p0opl23() {
	struct symbol *psymbol;

	while (1) {
		if (token >= 0 && token < TASCII)
			break;
		psymbol = (struct symbol *)token;
		psymbol->flags |= FGLOBAL;
		p0readop();
		if (token != TCOMMA)
			break;
		p0readop();
	}
}

/* opl25: */
/* opl26: */
/* opl27: */
/*	mov	dotrel,r1 */
/*	asl	r1 */
/*	mov	dot,savdot-4(r1) */
/*	mov	savdot-[2*25](r0),dot */
/*	asr	r0 */
/*	sub	$25-2,r0 */
/*	mov	r0,dotrel */
/*	rts	pc */

void p0opl25() {
	savdot[pdot->flags - FTEXT] = pdot->value;
	pdot->value = savdot[0];
	pdot->flags = FTEXT;
}

void p0opl26() {
	savdot[pdot->flags - FTEXT] = pdot->value;
	pdot->value = savdot[1];
	pdot->flags = FDATA;
}

void p0opl27() {
	savdot[pdot->flags - FTEXT] = pdot->value;
	pdot->value = savdot[2];
	pdot->flags = FBSS;
}

/* opl32:					/ .common */
/*	cmp	r4,$200 */
/*	blo	1f */
/*	bis	$40,(r4) */
/*	jsr	pc,p0readop */
/*	cmp	r4,$', */
/*	bne	1f */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	rts	pc */
/* 1: */
/*	mov	$'x,r5 */
/*	jsr	pc,error */
/*	rts	pc */

void p0opl32() {
	struct symbol *psymbol;

	if (token >= 0 && token < TASCII) {
		error('x');
		return;
	}
	psymbol = (struct symbol *)token;
	psymbol->flags |= FGLOBAL;
	p0readop();
	if (token != TCOMMA) {
		error('x');
		return;
	}
	p0readop();
	p0expres();
}

/* addres: */
/*	cmp	r4,$'( */
/*	beq	alp */
/*	cmp	r4,$'- */
/*	beq	amin */
/*	cmp	r4,$'$ */
/*	beq	adoll */
/*	cmp	r4,$'* */
/*	beq	astar */

void p0addres() {
/* printf("p0addres() token = 0x%08x\n", token); */
	switch (token) {
	case TLPAREN:
		p0alp();
		return;
	case TMIN:
		p0amin();
		return;
	case TDOLL:
		p0adoll();
		return;
	case TSTAR:
		p0astar();
		return;
	}
	p0getx();
}

/* getx: */
/*	jsr	pc,expres */
/*	cmp	r4,$'( */
/*	bne	2f */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	jsr	pc,checkreg */
/*	jsr	pc,checkrp */
/* 1: */
/*	add	$2,dot */
/*	clr	r0 */
/*	rts	pc */
/* 2: */
/*	cmp	r3,$24			/ register type */
/*	bne	1b */
/*	jsr	pc,checkreg */
/*	clr	r0 */
/*	rts	pc */

void p0getx() {
	p0expres();
	if (token == TLPAREN) {
		p0readop();
		p0expres();
		p0checkreg();
		p0checkrp();
		pdot->value += 2;
	}
	else if (leftflags == FREGISTER)
		p0checkreg();
	else
		pdot->value += 2;
	indirect = 0;
}

/* alp: */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	jsr	pc,checkrp */
/*	jsr	pc,checkreg */
/*	cmp	r4,$'+ */
/*	bne	1f */
/*	jsr	pc,p0readop */
/*	clr	r0 */
/*	rts	pc */
/* 1: */
/*	mov	$2,r0 */
/*	rts	pc */

void p0alp() {
	p0readop();
	p0expres();
	p0checkrp();
	p0checkreg();
	if (token == TPLUS) {
		p0readop();
		indirect = 0;
	}
	else
		indirect = 2;
}

/* amin: */
/*	jsr	pc,p0readop */
/*	cmp	r4,$'( */
/*	beq	1f */
/*	mov	r4,savop */
/*	mov	$'-,r4 */
/*	br	getx */
/* 1: */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	jsr	pc,checkrp */
/*	jsr	pc,checkreg */
/*	clr	r0 */
/*	rts	pc */

void p0amin() {
	p0readop();
	if (token != TLPAREN) {
		savop = token;
		token = TMIN;
		p0getx();
	}
	else {
		p0readop();
		p0expres();
		p0checkrp();
		p0checkreg();
		indirect = 0;
	}
}

/* adoll: */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	add	$2,dot */
/*	clr	r0 */
/*	rts	pc */

void p0adoll() {
	p0readop();
	p0expres();
	pdot->value += 2;
	indirect = 0;
}

/* astar: */
/*	jsr	pc,p0readop */
/*	cmp	r4,$'* */
/*	bne	1f */
/*	mov	$'*,r5 */
/*	jsr	pc,error */
/* 1: */
/*	jsr	pc,addres */
/*	add	r0,dot */
/*	rts	pc */

void p0astar() {
	p0readop();
	if (token == TSTAR)
		error('*');
	p0addres();
	pdot->value += indirect;
}

/* errora: */
/*	mov	$'a,r5 */
/*	jsr	pc,error */
/*	rts	pc */

void errora() {
	error('a');
}

/* checkreg: */
/*	cmp	r2,$7 */
/*	bhi	1f */
/*	cmp	r3,$1 */
/*	beq	2f */
/*	cmp	r3,$4 */
/*	bhi	2f */
/* 1: */
/*	jsr	pc,errora */
/* 2: */
/*	rts	pc */

void p0checkreg() {
	if (leftvalue > 7 || (leftflags != FABS && leftflags <= FBSS))
		errora();
}

/* errore: */
/*	mov	$'e,r5 */
/*	jsr	pc,error */
/*	rts	pc */

void errore() {
	error('e');
}

/* checkrp: */
/*	cmp	r4,$') */
/*	beq	1f */
/*	mov	$'),r5 */
/*	jsr	pc,error */
/*	rts	pc */
/* 1: */
/*	jsr	pc,p0readop */
/*	rts	pc */

void p0checkrp() {
	if (token != TRPAREN)
		error(')');
	else
		p0readop();
}

/* expres: */
/*	mov	r5,-(sp) */
/*	mov	$'+,-(sp) */
/*	clr	opfound */
/*	clr	r2 */
/*	mov	$1,r3 */
/*	br	1f */

void p0expres() {
	struct symbol *psymbol;

/* printf("p0expres()\n"); */
	optoken = TPLUS;
	opfound = 0;
	leftvalue = 0;
	leftflags = FABS;
	goto entry;

/* advanc: */
/*	jsr	pc,p0readop */
/* 1: */
/*	mov	r4,r0 */
/*	tst	r0 */
/*	blt	6f */
/*	cmp	r0,$177 */
/*	ble	7f */
/* 6: */
/*	movb	(r4),r0 */
/*	mov	2(r4),r1 */
/*	br	oprand */
/* 7: */

	while (1) {
		p0readop();
	entry:
		rightflags = token;
		if (token < 0 || token >= TASCII) {
			psymbol = (struct symbol *)token;
			rightflags = psymbol->flags;
			rightvalue = psymbol->value;
			p0oprand();
			continue;
		}

/*	cmp	r4,$141 */
/*	blo	1f */
/*	cmp	r4,$141+10. */
/*	bhis	2f */
/*	movb	curfbr-141(r4),r0 */
/*	asl	r4 */
/*	mov	curfb-[2*141](r4),r2 */
/*	cmp	r2,$-1 */
/*	bne	oprand */
/*	mov	$'f,r5 */
/*	jsr	pc,error */
/*	br	oprand */
/* 2: */
/*	clr	r3 */
/*	clr	r2 */
/*	br	oprand */
/* 1: */

		if (token >= TLABEL) {
			if (token < TLABEL + 10) {
				rightflags = curfbr[token - TLABEL];
 /* bug!! should be rightvalue / rightflags below: */
				leftvalue = curfb[token - TLABEL];
				if (leftvalue == -1)
					error('f');
			}
			else {
				leftflags = 0;
				leftvalue = 0;
			}
			p0oprand();
			continue;
		}

/*	mov	$esw1,r1 */
/* 1: */
/*	cmp	(r1)+,r4 */
/*	beq	1f */
/*	tst	(r1)+ */
/*	bne	1b */
/*	tst	opfound */
/*	bne	2f */
/*	jsr	pc,errore */
/* 2: */
/*	tst	(sp)+ */
/*	mov	(sp)+,r5 */
/*	rts	pc */
/* 1: */
/*	jmp	*(r1) */

		switch (token) {
		default:
			if (opfound == 0)
				error('e');
			return;

/*	.data */
/* esw1: */
/*	'+;	binop */
/*	'-;	binop */
/*	'*;	binop */
/*	'/;	binop */
/*	'&;	binop */
/*	037;	binop */
/*	035;	binop */
/*	036;	binop */
/*	'%;	binop */

		case TPLUS:
		case TMIN:
		case TSTAR:
		case TSLASH:
		case TAND:
		case TOR:
		case TLSH:
		case TRSH:
		case TMOD:
			p0binop();
			break;

/*	'[;	brack */

		case TLBRACK:
			p0brack();
			break;

/*	'^;	binop */

		case TCARET:
			p0binop();
			break;

/*	1;	exnum */

		case TABS:
			p0exnum();
			break;

/*	'!;	binop */

		case TNOT:
			p0binop();
			break;

/*	0;	0 */
/*	.text */

		}
	}
}

/* binop: */
/*	cmpb	(sp),$'+ */
/*	beq	1f */
/*	jsr	pc,errore */
/* 1: */
/*	movb	r4,(sp) */
/*	br	advanc */

void p0binop() {
	if (optoken != TPLUS)
		error('e');
	optoken = token;
}

/* exnum: */
/*	mov	numval,r1 */
/*	mov	$1,r0 */
/*	br	oprand */

void p0exnum() {
/* printf("p0exnum() numval = 0%o\n", numval); */
	rightvalue = numval;
	rightflags = FABS;
	p0oprand();
}

/* brack: */
/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */
/*	jsr	pc,p0readop */
/*	jsr	pc,expres */
/*	cmp	r4,$'] */
/*	beq	1f */
/*	mov	$'],r5 */
/*	jsr	pc,error */
/* 1: */
/*	mov	r3,r0 */
/*	mov	r2,r1 */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */

void p0brack() {
	int tempvalue;
	int tempflags;
	int temptoken;

	tempvalue = leftvalue;
	tempflags = leftflags;
	temptoken = optoken;
	p0readop();
	p0expres();
	if (token != TRBRACK)
		error(']');
	rightflags = leftflags;
	rightvalue = leftvalue;
	leftflags = tempflags;
	leftvalue = tempvalue;
	optoken = temptoken;
	p0oprand();
}

/* oprand: */
/*	inc	opfound */
/*	mov	$exsw2,r5 */
/* 1: */
/*	cmp	(sp),(r5)+ */
/*	beq	1f */
/*	tst	(r5)+ */
/*	bne	1b */
/*	br	eoprnd */
/* 1: */
/*	jmp	*(r5) */

void p0oprand() {
	opfound = 1;
	switch (optoken) {
	default:
		p0eoprnd();
		return;

/*	.data */
/* exsw2: */
/*	'+; exadd */

	case TPLUS:
		p0exadd();
		return;

/*	'-; exsub */

	case TMIN:
		p0exsub();
		return;

/*	'*; exmul */

	case TSTAR:
		p0exmul();
		return;

/*	'/; exdiv */

	case TSLASH:
		p0exdiv();
		return;

/*	037; exor */

	case TOR:
		p0exor();
		return;

/*	'&; exand */

	case TAND:
		p0exand();
		return;

/*	035;exlsh */

	case TLSH:
		p0exlsh();
		return;

/*	036;exrsh */

	case TRSH:
		p0exrsh();
		return;

/*	'%; exmod */

	case TMOD:
		p0exmod();
		return;

/*	'!; exnot */

	case TNOT:
		p0exnot();
		return;

/*	'^; excmbin */

	case TCARET:
		p0excmbin();
		return;

/*	0;  0 */
/*	.text */

	}
}

/* excmbin: */
/*	mov	r0,r3			/ give left flag of right */
/*	br	eoprnd */

void p0excmbin() {
	leftflags = rightflags;
	p0eoprnd();
}

/* exrsh: */
/*	neg	r1 */
/*	beq	exlsh */
/*	inc	r1 */
/*	clc */
/*	ror	r2 */
/* exlsh: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	ash	r1,r2 */
/*	br	eoprnd */

void p0exrsh() {
	p0combin(0);
	leftvalue = ((unsigned int)leftvalue & 0177777) >> rightvalue;
	p0eoprnd();
}

void p0exlsh() {
	p0combin(0);
	leftvalue <<= rightvalue;
	p0eoprnd();
}

/* exmod: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	mov	r1,-(sp) */
/*	mov	r2,r1 */
/*	clr	r0 */
/*	div	(sp)+,r0 */
/*	mov	r1,r2 */
/*	br	eoprnd */

void p0exmod() {
	int temp;

	p0combin(0);
	temp = rightvalue & 0177777;
	if (temp)
		leftvalue = (leftvalue & 0177777) % temp;
	else {
		error('0'); /* a nick innovation */
		leftvalue = 0;
	}
	p0eoprnd();
}

/* exadd: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	add	r1,r2 */
/*	br	eoprnd */

void p0exadd() {
	p0combin(0);
	leftvalue += rightvalue;
	p0eoprnd();
}

/* exsub: */
/*	mov	$1,r5 */
/*	jsr	pc,combin */
/*	sub	r1,r2 */
/*	br	eoprnd */

void p0exsub() {
	p0combin(1);
	leftvalue -= rightvalue;
	p0eoprnd();
}

/* exand: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	com	r1 */
/*	bic	r1,r2 */
/*	br	eoprnd */

void p0exand() {
	p0combin(0);
	leftvalue &= rightvalue;
	p0eoprnd();
}

/* exor: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	bis	r1,r2 */
/*	br	eoprnd */

void p0exor() {
	p0combin(0);
	leftvalue |= rightvalue;
	p0eoprnd();
}

/* exmul: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	mul	r2,r1 */
/*	mov	r1,r2 */
/*	br	eoprnd */

void p0exmul() {
	p0combin(0);
	leftvalue *= rightvalue;
	p0eoprnd();
}

/* exdiv: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	mov	r1,-(sp) */
/*	mov	r2,r1 */
/*	clr	r0 */
/*	div	(sp)+,r0 */
/*	mov	r0,r2 */
/*	br	eoprnd */

void p0exdiv() {
	int temp;

	p0combin(0);
	temp = rightvalue & 0177777;
	if (temp)
		leftvalue = (leftvalue & 0177777) / temp;
	else {
		error('0'); /* a nick innovation */
		leftvalue = 0;
	}
	p0eoprnd();
}

/* exnot: */
/*	clr	r5 */
/*	jsr	pc,combin */
/*	com	r1 */
/*	add	r1,r2 */
/*	br	eoprnd */

void p0exnot() {
	p0combin(0);
	leftvalue += ~rightvalue;
	p0eoprnd();
}

/* eoprnd: */
/*	mov	$'+,(sp) */
/*	jmp	advanc */

void p0eoprnd() {
	optoken = TPLUS;
}

/* combin: */
/*	mov	r0,-(sp) */
/*	bis	r3,(sp) */
/*	bic	$!40,(sp) */
/*	bic	$!37,r0 */
/*	bic	$!37,r3 */
/*	cmp	r0,r3 */
/*	ble	1f */
/*	mov	r0,-(sp) */
/*	mov	r3,r0 */
/*	mov	(sp)+,r3 */
/* 1: */

void p0combin(minflag) int minflag; {
	int global;
	int flags;

	global = (rightflags | leftflags) & FGLOBAL;
	rightflags &= 037;
	leftflags &= 037;
	if (rightflags > leftflags) {
		flags = rightflags;
		rightflags = leftflags;
		leftflags = flags;
	}

/*	tst	r0 */
/*	beq	1f */
/*	tst	r5 */
/*	beq	2f */
/*	cmp	r0,r3 */
/*	bne	2f */
/*	mov	$1,r3 */
/*	br	2f */
/* 1: */
/*	clr	r3 */
/* 2: */
/*	bis	(sp)+,r3 */
/*	rts	pc */

	if (rightflags) {
		if (minflag && rightflags == leftflags)
			leftflags = TABS;
	}
	else {
		leftflags = 0;
	}
	leftflags |= global;
}

/*	.data */
/* chartab: */
/*	.byte -14,-14,-14,-14,-02,-14,-14,-14 */
/*	.byte -14,-22, -2,-14,-14,-22,-14,-14 */
/*	.byte -14,-14,-14,-14,-14,-14,-14,-14 */
/*	.byte -14,-14,-14,-14,-14,-14,-14,-14 */
/*	.byte -22,-20,-16,-14,-20,-20,-20,-12 */
/*	.byte -20,-20,-20,-20,-20,-20,056,-06 */
/*	.byte 060,061,062,063,064,065,066,067 */
/*	.byte 070,071,-20,-02,-00,-20,-14,-14 */
/*	.byte -14,101,102,103,104,105,106,107 */
/*	.byte 110,111,112,113,114,115,116,117 */
/*	.byte 120,121,122,123,124,125,126,127 */
/*	.byte 130,131,132,-20,-24,-20,-20,137 */
/*	.byte -14,141,142,143,144,145,146,147 */
/*	.byte 150,151,152,153,154,155,156,157 */
/*	.byte 160,161,162,163,164,165,166,167 */
/*	.byte 170,171,172,-14,-26,-14,176,-14 */

char chartab[TASCII] = {
	-014,-014,-014,-014,-002,-014,-014,-014,
	-014,-022,0 -2,-014,-014,-022,-014,-014,
	-014,-014,-014,-014,-014,-014,-014,-014,
	-014,-014,-014,-014,-014,-014,-014,-014,
#if 1 /* modifications for dec syntax */
	-022,-020,-016,-026,-020,-020,-020,-012,
#else
	-022,-020,-016,-014,-020,-020,-020,-012,
#endif
	-020,-020,-020,-020,-020,-020,0056,-006,
	0060,0061,0062,0063,0064,0065,0066,0067,
	0070,0071,-020,-002,-000,-020,-014,-014,
#if 1 /* modifications for dec syntax */
	-026,0101,0102,0103,0104,0105,0106,0107,
#else
	-014,0101,0102,0103,0104,0105,0106,0107,
#endif
	0110,0111,0112,0113,0114,0115,0116,0117,
	0120,0121,0122,0123,0124,0125,0126,0127,
	0130,0131,0132,-020,-024,-020,-020,0137,
	-014,0141,0142,0143,0144,0145,0146,0147,
	0150,0151,0152,0153,0154,0155,0156,0157,
	0160,0161,0162,0163,0164,0165,0166,0167,
	0170,0171,0172,-014,-026,-014,0176,-014
};

/* a.tmp1:	</tmp/atm1XX\0> */

#ifdef pdp11
char a_tmp1[] = "/tmp/atm1XX";
#else
char a_tmp1[] = "/tmp/atm1XXXXXX"; /* linux requires 6 X's */
#endif

/* Ncps:	.byte 8. */

char Ncps = 8;

/* 1:	<-\0> */
/*	.even */
/* dash:	1b */

char *dash = "-";

/* fin:	-1 */

int fin = -1;

/* fout:	-1 */

int fout = -1;

/* The next two _must_ be adjacent!  Not sure why, but then this whole */
/* assembler is weird beyond belief. */
/* curfb:	-1;-1;-1;-1;-1;-1;-1;-1;-1;-1 */

/* THEY HAD TO BE ADJACENT BECAUSE PASS 2 p0expres() READS curfb[token - 0141] */
/* THIS HAS BEEN SPLIT INTO curfb[token - 0141], nxtfb[token - (0141 + 10)] */
/* BY THE WAY, THE ASSEMBLER IS NOT REALLY WEIRD, IT'S KIND OF ELEGANT !!! */
intptr_t curfb[10] = {
	-1, -1, -1, -1, -1, -1, -1, -1, -1, -1
};

/* nxtfb:	.=.+20.			/ first 4 used by pass0, all 20. by pass1+2 */

/* FIRST 4 AREN'T USED BY PASS 0 ANYMORE, IT WAS ONLY TEMPORARY SCRATCH !!!! */
intptr_t nxtfb[10];

/*	.bss */
/* curfbr:	.=.+10. */

int curfbr[10];

/* savdot:	.=.+6 */

int savdot[3];

/* hshtab:	.=.+2			/ dynamically allocated */

struct symbol **hshtab;

/* ch:	.=.+2 */

int ch;

/* symnum:	.=.+2			/ symbol number */

int symnum;

/* symbol:	.=.+32.			/ XXX */
/*	.=.+2			/ paranoia to make sure a null is present */

char symbol[33];

/* inbuf:  .=.+1024. */

char inbuf[1024];

/* line:	.=.+2 */

int line;

/* ifflg:	.=.+2 */

int ifflg;

/* nargs:	.=.+2 */

int nargs;

/* curarg:	.=.+2 */

char **curarg;

/* opfound:.=.+2 */

int opfound;

/* savop:	.=.+2 */

int savop;

/* numval:	.=.+2 */

int numval;

/* fbtblsz:.=.+2 */

int fbtblsz;

/* fbfree: .=.+2 */

int fbfree;

/* fbptr:  .=.+2 */

struct symbol *fbptr;

/* fbtbl:  .=.+2 */

struct symbol *fbtbl;

/* usymtab:.=.+2			/ ptr to first block of symbols */

struct symblk *usymtab;

/* symleft:.=.+2			/ bytes left in current symbol block */

int symleft;

/* symend: .=.+2			/ ptr to next symbol space in block */

struct symbol *symend;

/* symblk: .=.+2			/ ptr to beginning of current sym block */

struct symblk *symblk;

/* strleft:.=.+2			/ amount left in string block */

int strleft;

/* strend:	.=.+2			/ ptr to next available string byte */

char *strend;

/* strblk:	.=.+2			/ ptr to current string block link word */

struct strblk *strblk;

/* key to types */

/*	0	undefined */
/*	1	absolute (nop, reset, bpt, ...) */
/*	2	text */
/*	3	data */
/*	4	bss */
/*	5	flop freg,dst (movfo, = stcfd) */
/*	6	branch */
/*	7	jsr */
/*	10	rts */
/*	11	sys, trap */
/*	12	movf (=ldf,stf) */
/*	13	double operand (mov) */
/*	14	flop fsrc,freg (addf) */
/*	15	single operand (clr) */
/*	16	.byte */
/*	17	string (.ascii, "<") */
/*	20	.even */
/*	21	.if */
/*	22	.endif */
/*	23	.globl */
/*	24	register */
/*	25	.text */
/*	26	.data */
/*	27	.bss */
/*	30	mul,div, etc */
/*	31	sob */
/*	32	.comm */
/*	33	estimated text */
/*	34	estimated data */
/*	35	jbr */
/*	36	jeq, jne, etc */

/*	.data */
/* the format of PST entries was changed.  rather than fixed 8 byte strings */
/* (often with many trailing nulls) a pointer to a null terminated string */
/* is now used.  This saves quite a bit of space since most PST entries are */
/* only 3 or 4 characters long.  we had to do this the hard way since there */
/* is no macro capability in the assembler and i'd chuck out the SDI [Span */
/* Dependent Instruction] stuff and use my own assembler before trying to */
/* add macros to this one.  Symbols beginning with 'L' are used since the */
/* linker can be told to discard those. */

/* symtab: */
/* special symbols */

/* L1; dotrel: 02;    dot: 0000000 */
/* L2;	    01; dotdot: 0000000 */

/* register */

/* L3;	24;	000000 */
/* L4;	24;	000001 */
/* L5;	24;	000002 */
/* L6;	24;	000003 */
/* L7;	24;	000004 */
/* L8;	24;	000005 */
/* L9;	24;	000006 */
/* L10;	24;	000007 */

/* double operand */

/* L11;	13;	0010000 */
/* L12;	13;	0110000 */
/* L13;	13;	0020000 */
/* L14;	13;	0120000 */
/* L15;	13;	0030000 */
/* L16;	13;	0130000 */
/* L17;	13;	0040000 */
/* L18;	13;	0140000 */
/* L19;	13;	0050000 */
/* L20;	13;	0150000 */
/* L21;	13;	0060000 */
/* L22;	13;	0160000 */

/* branch */

/* L23;	06;	0000400 */
/* L24;	06;	0001000 */
/* L25;	06;	0001400 */
/* L26;	06;	0002000 */
/* L27;	06;	0002400 */
/* L28;	06;	0003000 */
/* L29;	06;	0003400 */
/* L30;	06;	0100000 */
/* L31;	06;	0100400 */
/* L32;	06;	0101000 */
/* L33;	06;	0101400 */
/* L34;	06;	0102000 */
/* L35;	06;	0102400 */
/* L36;	06;	0103000 */
/* L37;	06;	0103000 */
/* L38;	06;	0103000 */
/* L39;	06;	0103400 */
/* L40;	06;	0103400 */
/* L41;	06;	0103400 */

/* jump/branch type */

/* L42;	35;	0000400 */
/* L43;	36;	0001000 */
/* L44;	36;	0001400 */
/* L45;	36;	0002000 */
/* L46;	36;	0002400 */
/* L47;	36;	0003000 */
/* L48;	36;	0003400 */
/* L49;	36;	0100000 */
/* L50;	36;	0100400 */
/* L51;	36;	0101000 */
/* L52;	36;	0101400 */
/* L53;	36;	0102000 */
/* L54;	36;	0102400 */
/* L55;	36;	0103000 */
/* L56;	36;	0103000 */
/* L57;	36;	0103000 */
/* L58;	36;	0103400 */
/* L59;	36;	0103400 */
/* L60;	36;	0103400 */

/* single operand */

/* L61;	15;	0005000 */
/* L62;	15;	0105000 */
/* L63;	15;	0005100 */
/* L64;	15;	0105100 */
/* L65;	15;	0005200 */
/* L66;	15;	0105200 */
/* L67;	15;	0005300 */
/* L68;	15;	0105300 */
/* L69;	15;	0005400 */
/* L70;	15;	0105400 */
/* L71;	15;	0005500 */
/* L72;	15;	0105500 */
/* L73;	15;	0005600 */
/* L74;	15;	0105600 */
/* L75;	15;	0005700 */
/* L76;	15;	0105700 */
/* L77;	15;	0006000 */
/* L78;	15;	0106000 */
/* L79;	15;	0006100 */
/* L80;	15;	0106100 */
/* L81;	15;	0006200 */
/* L82;	15;	0106200 */
/* L83;	15;	0006300 */
/* L84;	15;	0106300 */
/* L85;	15;	0000100 */
/* L86;	15;	0000300 */
/* L87;	15;	0006500 */
/* L88;	15;	0006600 */
/* L89;	15;	0106500 */
/* L90;	15;	0106600 */
/* L91;	15;	0170300 */
/* L92;	15;	0106700 */
/* L93;	15;	0106400 */
/* L94;	15;	0007000 */
/* L95;	15;	0007200 */
/* L96;	15;	0007300 */

/* jsr */

/* L97;	07;	0004000 */

/* rts */

/* L98;	010;	000200 */

/* simple operand */

/* L99;	011;	104400 */
/* L102;	011;	000230 */

/* flag-setting */

/* L103;	01;	0000240 */
/* L104;	01;	0000241 */
/* L105;	01;	0000242 */
/* L106;	01;	0000244 */
/* L107;	01;	0000250 */
/* L108;	01;	0000257 */
/* L109;	01;	0000261 */
/* L110;	01;	0000262 */
/* L111;	01;	0000264 */
/* L112;	01;	0000270 */
/* L113;	01;	0000277 */
/* L114;	01;	0000000 */
/* L115;	01;	0000001 */
/* L116;	01;	0000002 */
/* L117;	01;	0000003 */
/* L118;	01;	0000004 */
/* L119;	01;	0000005 */
/* L120;	01;	0000006 */
/* L121;	01;	0000007 */

/* floating point ops */

/* L122;	01;	170000 */
/* L123;	01;	170001 */
/* L124;	01;	170011 */
/* L125;	01;	170002 */
/* L126;	01;	170012 */
/* L127;	15;	170400 */
/* L128;	15;	170700 */
/* L129;	15;	170600 */
/* L130;	15;	170500 */
/* L131;	12;	172400 */
/* L132;	14;	177000 */
/* L133;	05;	175400 */
/* L134;	14;	177400 */
/* L135;	05;	176000 */
/* L136;	14;	172000 */
/* L137;	14;	173000 */
/* L138;	14;	171000 */
/* L139;	14;	174400 */
/* L140;	14;	173400 */
/* L141;	14;	171400 */
/* L142;	14;	176400 */
/* L143;	05;	175000 */
/* L144;	15;	170100 */
/* L145;	15;	170200 */
/* L146;	24;	000000 */
/* L147;	24;	000001 */
/* L148;	24;	000002 */
/* L149;	24;	000003 */
/* L150;	24;	000004 */
/* L151;	24;	000005 */

/* L152;	30;	070000 */
/* L153;	30;	071000 */
/* L154;	30;	072000 */
/* L155;	30;	073000 */
/* L156;	07;	074000 */
/* L157;	15;	006700 */
/* L158;	11;	006400 */
/* L159;	31;	077000 */

/* pseudo ops */

/* L160;	16;	000000 */
/* L161;	20;	000000 */
/* L162;	21;	000000 */
/* L163;	22;	000000 */
/* L164;	23;	000000 */
/* L165;	25;	000000 */
/* L166;	26;	000000 */
/* L167;	27;	000000 */
/* L168;	32;	000000 */

/* ebsymtab: */

/* L1:	<.\0> */
/* L2:	<..\0> */
/* L3:	<r0\0> */
/* L4:	<r1\0> */
/* L5:	<r2\0> */
/* L6:	<r3\0> */
/* L7:	<r4\0> */
/* L8:	<r5\0> */
/* L9:	<sp\0> */
/* L10:	<pc\0> */
/* L11:	<mov\0> */
/* L12:	<movb\0> */
/* L13:	<cmp\0> */
/* L14:	<cmpb\0> */
/* L15:	<bit\0> */
/* L16:	<bitb\0> */
/* L17:	<bic\0> */
/* L18:	<bicb\0> */
/* L19:	<bis\0> */
/* L20:	<bisb\0> */
/* L21:	<add\0> */
/* L22:	<sub\0> */
/* L23:	<br\0> */
/* L24:	<bne\0> */
/* L25:	<beq\0> */
/* L26:	<bge\0> */
/* L27:	<blt\0> */
/* L28:	<bgt\0> */
/* L29:	<ble\0> */
/* L30:	<bpl\0> */
/* L31:	<bmi\0> */
/* L32:	<bhi\0> */
/* L33:	<blos\0> */
/* L34:	<bvc\0> */
/* L35:	<bvs\0> */
/* L36:	<bhis\0> */
/* L37:	<bec\0> */
/* L38:	<bcc\0> */
/* L39:	<blo\0> */
/* L40:	<bcs\0> */
/* L41:	<bes\0> */
/* L42:	<jbr\0> */
/* L43:	<jne\0> */
/* L44:	<jeq\0> */
/* L45:	<jge\0> */
/* L46:	<jlt\0> */
/* L47:	<jgt\0> */
/* L48:	<jle\0> */
/* L49:	<jpl\0> */
/* L50:	<jmi\0> */
/* L51:	<jhi\0> */
/* L52:	<jlos\0> */
/* L53:	<jvc\0> */
/* L54:	<jvs\0> */
/* L55:	<jhis\0> */
/* L56:	<jec\0> */
/* L57:	<jcc\0> */
/* L58:	<jlo\0> */
/* L59:	<jcs\0> */
/* L60:	<jes\0> */
/* L61:	<clr\0> */
/* L62:	<clrb\0> */
/* L63:	<com\0> */
/* L64:	<comb\0> */
/* L65:	<inc\0> */
/* L66:	<incb\0> */
/* L67:	<dec\0> */
/* L68:	<decb\0> */
/* L69:	<neg\0> */
/* L70:	<negb\0> */
/* L71:	<adc\0> */
/* L72:	<adcb\0> */
/* L73:	<sbc\0> */
/* L74:	<sbcb\0> */
/* L75:	<tst\0> */
/* L76:	<tstb\0> */
/* L77:	<ror\0> */
/* L78:	<rorb\0> */
/* L79:	<rol\0> */
/* L80:	<rolb\0> */
/* L81:	<asr\0> */
/* L82:	<asrb\0> */
/* L83:	<asl\0> */
/* L84:	<aslb\0> */
/* L85:	<jmp\0> */
/* L86:	<swab\0> */
/* L87:	<mfpi\0> */
/* L88:	<mtpi\0> */
/* L89:	<mfpd\0> */
/* L90:	<mtpd\0> */
/* L91:	<stst\0> */
/* L92:	<mfps\0> */
/* L93:	<mtps\0> */
/* L94:	<csm\0> */
/* L95:	<tstset\0> */
/* L96:	<wrtlck\0> */
/* L97:	<jsr\0> */
/* L98:	<rts\0> */
/* L99:	<sys\0> */
/* L102:	<spl\0> */
/* L103:	<nop\0> */
/* L104:	<clc\0> */
/* L105:	<clv\0> */
/* L106:	<clz\0> */
/* L107:	<cln\0> */
/* L108:	<ccc\0> */
/* L109:	<sec\0> */
/* L110:	<sev\0> */
/* L111:	<sez\0> */
/* L112:	<sen\0> */
/* L113:	<scc\0> */
/* L114:	<halt\0> */
/* L115:	<wait\0> */
/* L116:	<rti\0> */
/* L117:	<bpt\0> */
/* L118:	<iot\0> */
/* L119:	<reset\0> */
/* L120:	<rtt\0> */
/* L121:	<mfpt\0> */
/* L122:	<cfcc\0> */
/* L123:	<setf\0> */
/* L124:	<setd\0> */
/* L125:	<seti\0> */
/* L126:	<setl\0> */
/* L127:	<clrf\0> */
/* L128:	<negf\0> */
/* L129:	<absf\0> */
/* L130:	<tstf\0> */
/* L131:	<movf\0> */
/* L132:	<movif\0> */
/* L133:	<movfi\0> */
/* L134:	<movof\0> */
/* L135:	<movfo\0> */
/* L136:	<addf\0> */
/* L137:	<subf\0> */
/* L138:	<mulf\0> */
/* L139:	<divf\0> */
/* L140:	<cmpf\0> */
/* L141:	<modf\0> */
/* L142:	<movie\0> */
/* L143:	<movei\0> */
/* L144:	<ldfps\0> */
/* L145:	<stfps\0> */
/* L146:	<fr0\0> */
/* L147:	<fr1\0> */
/* L148:	<fr2\0> */
/* L149:	<fr3\0> */
/* L150:	<fr4\0> */
/* L151:	<fr5\0> */
/* L152:	<mul\0> */
/* L153:	<div\0> */
/* L154:	<ash\0> */
/* L155:	<ashc\0> */
/* L156:	<xor\0> */
/* L157:	<sxt\0> */
/* L158:	<mark\0> */
/* L159:	<sob\0> */
/* L160:	<.byte\0> */
/* L161:	<.even\0> */
/* L162:	<.if\0> */
/* L163:	<.endif\0> */
/* L164:	<.globl\0> */
/* L165:	<.text\0> */
/* L166:	<.data\0> */
/* L167:	<.bss\0> */
/* L168:	<.comm\0> */
/*	.text */

struct symbol symtab[] = {
	{ ".",		FTEXT,		0000000,	0 },
	{ "..",		FABS,		0000000,	0 },

/* register */

	{ "r0",		FREGISTER,	0000000,	0 },
	{ "r1",		FREGISTER,	0000001,	0 },
	{ "r2",		FREGISTER,	0000002,	0 },
	{ "r3",		FREGISTER,	0000003,	0 },
	{ "r4",		FREGISTER,	0000004,	0 },
	{ "r5",		FREGISTER,	0000005,	0 },
	{ "sp",		FREGISTER,	0000006,	0 },
	{ "pc",		FREGISTER,	0000007,	0 },

/* double operand */

	{ "mov",	FDOUBLE,	0010000,	0 },
	{ "movb",	FDOUBLE,	0110000,	0 },
	{ "cmp",	FDOUBLE,	0020000,	0 },
	{ "cmpb",	FDOUBLE,	0120000,	0 },
	{ "bit",	FDOUBLE,	0030000,	0 },
	{ "bitb",	FDOUBLE,	0130000,	0 },
	{ "bic",	FDOUBLE,	0040000,	0 },
	{ "bicb",	FDOUBLE,	0140000,	0 },
	{ "bis",	FDOUBLE,	0050000,	0 },
	{ "bisb",	FDOUBLE,	0150000,	0 },
	{ "add",	FDOUBLE,	0060000,	0 },
	{ "sub",	FDOUBLE,	0160000,	0 },

/* branch */

	{ "br",		FBRANCH,	0000400,	0 },
	{ "bne",	FBRANCH,	0001000,	0 },
	{ "beq",	FBRANCH,	0001400,	0 },
	{ "bge",	FBRANCH,	0002000,	0 },
	{ "blt",	FBRANCH,	0002400,	0 },
	{ "bgt",	FBRANCH,	0003000,	0 },
	{ "ble",	FBRANCH,	0003400,	0 },
	{ "bpl",	FBRANCH,	0100000,	0 },
	{ "bmi",	FBRANCH,	0100400,	0 },
	{ "bhi",	FBRANCH,	0101000,	0 },
	{ "blos",	FBRANCH,	0101400,	0 },
	{ "bvc",	FBRANCH,	0102000,	0 },
	{ "bvs",	FBRANCH,	0102400,	0 },
	{ "bhis",	FBRANCH,	0103000,	0 },
	{ "bec",	FBRANCH,	0103000,	0 },
	{ "bcc",	FBRANCH,	0103000,	0 },
	{ "blo",	FBRANCH,	0103400,	0 },
	{ "bcs",	FBRANCH,	0103400,	0 },
	{ "bes",	FBRANCH,	0103400,	0 },

/* jump/branch type */

	{ "jbr",	FJBR,		0000400,	0 },
	{ "jne",	FJCOND,		0001000,	0 },
	{ "jeq",	FJCOND,		0001400,	0 },
	{ "jge",	FJCOND,		0002000,	0 },
	{ "jlt",	FJCOND,		0002400,	0 },
	{ "jgt",	FJCOND,		0003000,	0 },
	{ "jle",	FJCOND,		0003400,	0 },
	{ "jpl",	FJCOND,		0100000,	0 },
	{ "jmi",	FJCOND,		0100400,	0 },
	{ "jhi",	FJCOND,		0101000,	0 },
	{ "jlos",	FJCOND,		0101400,	0 },
	{ "jvc",	FJCOND,		0102000,	0 },
	{ "jvs",	FJCOND,		0102400,	0 },
	{ "jhis",	FJCOND,		0103000,	0 },
	{ "jec",	FJCOND,		0103000,	0 },
	{ "jcc",	FJCOND,		0103000,	0 },
	{ "jlo",	FJCOND,		0103400,	0 },
	{ "jcs",	FJCOND,		0103400,	0 },
	{ "jes",	FJCOND,		0103400,	0 },

/* single operand */

	{ "clr",	FSINGLE,	0005000,	0 },
	{ "clrb",	FSINGLE,	0105000,	0 },
	{ "com",	FSINGLE,	0005100,	0 },
	{ "comb",	FSINGLE,	0105100,	0 },
	{ "inc",	FSINGLE,	0005200,	0 },
	{ "incb",	FSINGLE,	0105200,	0 },
	{ "dec",	FSINGLE,	0005300,	0 },
	{ "decb",	FSINGLE,	0105300,	0 },
	{ "neg",	FSINGLE,	0005400,	0 },
	{ "negb",	FSINGLE,	0105400,	0 },
	{ "adc",	FSINGLE,	0005500,	0 },
	{ "adcb",	FSINGLE,	0105500,	0 },
	{ "sbc",	FSINGLE,	0005600,	0 },
	{ "sbcb",	FSINGLE,	0105600,	0 },
	{ "tst",	FSINGLE,	0005700,	0 },
	{ "tstb",	FSINGLE,	0105700,	0 },
	{ "ror",	FSINGLE,	0006000,	0 },
	{ "rorb",	FSINGLE,	0106000,	0 },
	{ "rol",	FSINGLE,	0006100,	0 },
	{ "rolb",	FSINGLE,	0106100,	0 },
	{ "asr",	FSINGLE,	0006200,	0 },
	{ "asrb",	FSINGLE,	0106200,	0 },
	{ "asl",	FSINGLE,	0006300,	0 },
	{ "aslb",	FSINGLE,	0106300,	0 },
	{ "jmp",	FSINGLE,	0000100,	0 },
	{ "swab",	FSINGLE,	0000300,	0 },
	{ "mfpi",	FSINGLE,	0006500,	0 },
	{ "mtpi",	FSINGLE,	0006600,	0 },
	{ "mfpd",	FSINGLE,	0106500,	0 },
	{ "mtpd",	FSINGLE,	0106600,	0 },
	{ "stst",	FSINGLE,	0170300,	0 },
	{ "mfps",	FSINGLE,	0106700,	0 },
	{ "mtps",	FSINGLE,	0106400,	0 },
	{ "csm",	FSINGLE,	0007000,	0 },
	{ "tstset",	FSINGLE,	0007200,	0 },
	{ "wrtlck",	FSINGLE,	0007300,	0 },

/* jsr */

	{ "jsr",	FJSR,		0004000,	0 },

/* rts */

	{ "rts",	FRTS,		0000200,	0 },

/* simple operand */

	{ "sys",	FSYSTRAP,	0104400,	0 },
	{ "spl",	FSYSTRAP,	0000230,	0 },

/* flag-setting */

	{ "nop",	FABS,		0000240,	0 },
	{ "clc",	FABS,		0000241,	0 },
	{ "clv",	FABS,		0000242,	0 },
	{ "clz",	FABS,		0000244,	0 },
	{ "cln",	FABS,		0000250,	0 },
	{ "ccc",	FABS,		0000257,	0 },
	{ "sec",	FABS,		0000261,	0 },
	{ "sev",	FABS,		0000262,	0 },
	{ "sez",	FABS,		0000264,	0 },
	{ "sen",	FABS,		0000270,	0 },
	{ "scc",	FABS,		0000277,	0 },
	{ "halt",	FABS,		0000000,	0 },
	{ "wait",	FABS,		0000001,	0 },
	{ "rti",	FABS,		0000002,	0 },
	{ "bpt",	FABS,		0000003,	0 },
	{ "iot",	FABS,		0000004,	0 },
	{ "reset",	FABS,		0000005,	0 },
	{ "rtt",	FABS,		0000006,	0 },
	{ "mfpt",	FABS,		0000007,	0 },

/* floating point ops */

	{ "cfcc",	FABS,		0170000,	0 },
	{ "setf",	FABS,		0170001,	0 },
	{ "setd",	FABS,		0170011,	0 },
	{ "seti",	FABS,		0170002,	0 },
	{ "setl",	FABS,		0170012,	0 },
	{ "clrf",	FSINGLE,	0170400,	0 },
	{ "negf",	FSINGLE,	0170700,	0 },
	{ "absf",	FSINGLE,	0170600,	0 },
	{ "tstf",	FSINGLE,	0170500,	0 },
	{ "movf",	FMOVF,		0172400,	0 },
	{ "movif",	FFLOP,		0177000,	0 },
	{ "movfi",	FMOVFO,		0175400,	0 },
	{ "movof",	FFLOP,		0177400,	0 },
	{ "movfo",	FMOVFO,		0176000,	0 },
	{ "addf",	FFLOP,		0172000,	0 },
	{ "subf",	FFLOP,		0173000,	0 },
	{ "mulf",	FFLOP,		0171000,	0 },
	{ "divf",	FFLOP,		0174400,	0 },
	{ "cmpf",	FFLOP,		0173400,	0 },
	{ "modf",	FFLOP,		0171400,	0 },
	{ "movie",	FFLOP,		0176400,	0 },
	{ "movei",	FMOVFO,		0175000,	0 },
	{ "ldfps",	FSINGLE,	0170100,	0 },
	{ "stfps",	FSINGLE,	0170200,	0 },
	{ "fr0",	FREGISTER,	0000000,	0 },
	{ "fr1",	FREGISTER,	0000001,	0 },
	{ "fr2",	FREGISTER,	0000002,	0 },
	{ "fr3",	FREGISTER,	0000003,	0 },
	{ "fr4",	FREGISTER,	0000004,	0 },
	{ "fr5",	FREGISTER,	0000005,	0 },

	{ "mul",	FMULDIV,	0070000,	0 },
	{ "div",	FMULDIV,	0071000,	0 },
	{ "ash",	FMULDIV,	0072000,	0 },
	{ "ashc",	FMULDIV,	0073000,	0 },
	{ "xor",	FJSR,		0074000,	0 },
	{ "sxt",	FSINGLE,	0006700,	0 },
	{ "mark",	FSYSTRAP,	0006400,	0 },
	{ "sob",	FSOB,		0077000,	0 },

/* pseudo ops */

	{ ".byte",	FDOTBYTE,	0000000,	0 },
#if 1 /* modifications for dec syntax */
	{ ".word",	FDOTWORD,	0000000,	0 },
#endif
	{ ".even",	FDOTEVEN,	0000000,	0 },
	{ ".if",	FDOTIF,		0000000,	0 },
	{ ".endif",	FDOTENDIF,	0000000,	0 },
	{ ".globl",	FDOTGLOBL,	0000000,	0 },
	{ ".text",	FDOTTEXT,	0000000,	0 },
	{ ".data",	FDOTDATA,	0000000,	0 },
	{ ".bss",	FDOTBSS,	0000000,	0 },
	{ ".comm",	FDOTCOMM,	0000000,	0 }
};

int ebsymtab = sizeof(symtab) / sizeof(struct symbol);

