#ifndef _KRCOMPAT_H
#define _KRCOMPAT_H 1

#undef PARAMS
#ifdef __STDC__
#include <stdarg.h>
#define VA_START(ap, arg) va_start(ap, arg)
#define PARAMS(args) args
#else
#include <varargs.h>
#define VA_START(ap, arg) va_start(ap)
#define PARAMS(args) ()
#endif

#ifdef __GNUC__
#define NORETURN __attribute__ ((noreturn))
#else
#define NORETURN
#endif

#endif
