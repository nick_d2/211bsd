#ifndef _AS0_H
#define _AS0_H 1

#include "krcompat.h"

/* PSTENTSZ = 6. */
/* SYMENTSZ = 8. */

struct symbol {
	char	*name;
	int	flags;
	int	value;
	int	number;
};

/* User symbols and Permanent Symbol Table entries used to have the */
/* same 12 byte format.  Merging the two phases of the assembler, moving */
/* the symbol name characters to an externally allocated heap and */
/* using a non-contiguous user symbol table meant that the symbol number */
/* could no longer be calculated by subtracting the base of the symbol */
/* table and dividing by the size of an entry.  What was done was to */
/* expand the symbol table entry by another word and keep the symbol number */
/* in that.  The new internal symbol structure is: */

/*	char	*name; */
/*	u_short	flags; */
/*	u_short value; */
/*	u_short number; */

/* SYMBLKSZ = 512. */

#define SYMBLKSZ (512 / sizeof(struct symbol))

struct symblk {
	struct symblk *next;
	struct symbol data[SYMBLKSZ];
};

/* STRBLKSZ = 1024. */

#define STRBLKSZ 1024

struct strblk {
	struct strblk *next;
	char data[STRBLKSZ];
};

/* hshsiz = 3001. */

#define hshsiz 3001

#define TABS 1
#define TABS2 2
#define TENDFILE 4
#define TNEWFILE 5
#define TNEWLINE '\n'
#define TEQUAL '='
#define TCOLON ':'
#define FGLOBAL 040
#define TSEMICOLON ';'
#define TLABEL 0141
#define TLSH 035
#define TRSH 036
#define TOR 037
#define TSTRING '<'
#define TCOMMA ','
#define TLPAREN '('
#define TMIN '-'
#define TDOLL '$'
#define TSTAR '*'
#define TPLUS '+'
#define TRPAREN ')'
#define TSLASH '/'
#define TAND '&'
#define TMOD '%'
#define TLBRACK '['
#define TCARET '^'
#define TNOT '!'
#define TRBRACK ']'
#define TASCII 0200

#define FABS 1
#define FTEXT 2
#define FDATA 3
#define FBSS 4
#define FMOVFO 5
#define FBRANCH 6
#define FJSR 7
#define FRTS 010
#define FSYSTRAP 011
#define FMOVF 012
#define FDOUBLE 013
#define FFLOP 014
#define FSINGLE 015
#define FDOTBYTE 016
#define FSTRING 017
#define FDOTEVEN 020
#define FDOTIF 021
#define FDOTENDIF 022
#define FDOTGLOBL 023
#define FREGISTER 024
#define FDOTTEXT 025
#define FDOTDATA 026
#define FDOTBSS 027
#define FMULDIV 030
#define FSOB 031
#define FDOTCOMM 032
#define FESTTEXT 033
#define FESTDATA 034
#define FJBR 035
#define FJCOND 036
#if 1 /* modifications for dec syntax */
#define FDOTWORD 037
#endif

extern char Newsym;
extern struct symbol *pdot;
extern struct symbol *pdotdot;
extern intptr_t numbertoken;
extern int numbervalue;
extern int indirect;
extern int optoken; /* (sp) */
extern int rightflags; /* r0 */
extern int rightvalue; /* r1 */
extern int leftvalue; /* r2 */
extern int leftflags; /* r3 */
extern intptr_t token; /* r4 */
extern char chartab[TASCII];
extern char a_tmp1[];
extern char Ncps;
extern char *dash;
extern int fin;
extern int fout;
extern intptr_t curfb[10];
extern intptr_t nxtfb[10];
extern int curfbr[10];
extern int savdot[3];
extern struct symbol **hshtab;
extern int ch;
extern int symnum;
extern char symbol[33];
extern char inbuf[1024];
extern int line;
extern int ifflg;
extern int nargs;
extern char **curarg;
extern int opfound;
extern int savop;
extern int numval;
extern int fbtblsz;
extern int fbfree;
extern struct symbol *fbptr;
extern struct symbol *fbtbl;
extern struct symblk *usymtab;
extern int symleft;
extern struct symbol *symend;
extern struct symblk *symblk;
extern int strleft;
extern char *strend;
extern struct strblk *strblk;
extern struct symbol symtab[];
extern int ebsymtab;

int main PARAMS((int argc, char **argv));
int oops PARAMS((void));
void nomem PARAMS((void));
void error PARAMS((int code));
void p0putw PARAMS((int word));
void pass0 PARAMS((void));
int fbcheck PARAMS((int label));
void growfb PARAMS((void));
void rname PARAMS((void));
struct symbol *isroom PARAMS((struct symbol *psymbol));
char *astring PARAMS((int len));
int number PARAMS((void));
int rch PARAMS((void));
void p0readop PARAMS((void));
void escp PARAMS((void));
void fixor PARAMS((void));
void retread PARAMS((int c));
void rdname PARAMS((int c));
void rdnum PARAMS((void));
void squote PARAMS((void));
void dquote PARAMS((void));
void retnum PARAMS((int value));
void skip PARAMS((void));
void garb PARAMS((void));
void string PARAMS((void));
int rsch PARAMS((void));
void p0xpr PARAMS((void));
void p0opline PARAMS((void));
void p0opl35 PARAMS((void));
void p0opl36 PARAMS((void));
void relative PARAMS((int size));
void p0opl13 PARAMS((void));
void p0opl7 PARAMS((void));
void p0opl15 PARAMS((void));
void p0opl31 PARAMS((void));
void p0opl6 PARAMS((void));
void p0opl10 PARAMS((void));
void p0opl11 PARAMS((void));
void p0opl16 PARAMS((void));
#if 1 /* modifications for dec syntax */
void p0opldotword PARAMS((void));
#endif
void p0opl17 PARAMS((void));
void p0opl20 PARAMS((void));
void p0opl21 PARAMS((void));
void p0opl22 PARAMS((void));
void p0opl23 PARAMS((void));
void p0opl25 PARAMS((void));
void p0opl26 PARAMS((void));
void p0opl27 PARAMS((void));
void p0opl32 PARAMS((void));
void p0addres PARAMS((void));
void p0getx PARAMS((void));
void p0alp PARAMS((void));
void p0amin PARAMS((void));
void p0adoll PARAMS((void));
void p0astar PARAMS((void));
void errora PARAMS((void));
void p0checkreg PARAMS((void));
void errore PARAMS((void));
void p0checkrp PARAMS((void));
void p0expres PARAMS((void));
void p0binop PARAMS((void));
void p0exnum PARAMS((void));
void p0brack PARAMS((void));
void p0oprand PARAMS((void));
void p0excmbin PARAMS((void));
void p0exrsh PARAMS((void));
void p0exlsh PARAMS((void));
void p0exmod PARAMS((void));
void p0exadd PARAMS((void));
void p0exsub PARAMS((void));
void p0exand PARAMS((void));
void p0exor PARAMS((void));
void p0exmul PARAMS((void));
void p0exdiv PARAMS((void));
void p0exnot PARAMS((void));
void p0eoprnd PARAMS((void));
void p0combin PARAMS((int minflag));

#endif
