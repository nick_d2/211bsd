#ifndef _AS2_H
#define _AS2_H 1

#include "krcompat.h"

#define brlen 1024

#define X -2
#define M -1

#define outbuf ((short *)relp.data)

struct stream {
	char *append;
	char *limit;
	off_t offset;
	char data[02000];
};

extern intptr_t *adrptr; /* r5 */
extern int opcode; /* (sp) or 2(sp) if rvalue is present */
extern int rvalue; /* (sp) */
extern char reltp2[36];
extern char reltm2[36];
extern char relte2[36];
extern char *a_outp;
extern short *obufp;
extern int passno;
extern int outmod;
extern off_t *tseekp;
extern off_t *rseekp;
extern int siz[4];
extern off_t seek[7];
extern char brtab[brlen / 8];
extern int brtabp;
extern int brdelt;
extern struct symbol **fbbufp;
extern int defund;
extern int base[3];
extern int ibufc;
extern int overlaid;
extern intptr_t adrbuf[6];
extern struct symbol *xsymbol;
extern int errflg;
extern char argb[22];
extern int numval;
extern int maxtyp;
extern short *ibufp;
extern char *p0ibufp;
extern struct stream txtp;
extern struct stream relp;
extern int swapf;
extern int rlimit;
extern struct symbol **endtable;
extern off_t totalsz;

void pass1 PARAMS((void));
void saexit PARAMS((void));
int aexit PARAMS((void));
void filerr PARAMS((char *p));
void osymout PARAMS((void));
void nsymout PARAMS((void));
void putstring PARAMS((char *p));
void doreloc PARAMS((struct symbol *psymbol));
void setup PARAMS((void));
void outw PARAMS((int value, int flags));
void outb PARAMS((int value, int flags));
void pass1_2 PARAMS((void));
int checkeos PARAMS((void));
void fbadv PARAMS((int label));
void oset PARAMS((struct stream *pstream, off_t offset));
void p1putw PARAMS((struct stream *pstream, int word));
void flush PARAMS((struct stream *pstream));
void wrterr PARAMS((void));
void readop PARAMS((void));
void p1getw PARAMS((void));
int getw1 PARAMS((void));
void xpr PARAMS((void));
void opline PARAMS((void));
void opeof PARAMS((void));
void opl30 PARAMS((void));
void opl14 PARAMS((void));
void opl5 PARAMS((void));
void opl13 PARAMS((void));
void op2a PARAMS((void));
void op2b PARAMS((void));
void opl15 PARAMS((void));
void opl12 PARAMS((void));
void opl35 PARAMS((void));
void opl36 PARAMS((void));
void opl31 PARAMS((void));
void opl6 PARAMS((void));
void dobranch PARAMS((void));
void branch PARAMS((void));
void binstr PARAMS((void));
void errorb PARAMS((void));
void opl7 PARAMS((void));
void opl10 PARAMS((void));
void opl11 PARAMS((void));
void rinstr PARAMS((void));
void opl16 PARAMS((void));
#if 1 /* modifications for dec syntax */
void opldotword PARAMS((void));
#endif
void opl17 PARAMS((void));
void opl20 PARAMS((void));
void opl21 PARAMS((void));
void opl22 PARAMS((void));
void oplret PARAMS((void));
void opl23 PARAMS((void));
void opl25 PARAMS((void));
void opl26 PARAMS((void));
void opl27 PARAMS((void));
void opl32 PARAMS((void));
void addres PARAMS((void));
void addres1 PARAMS((void));
void getx PARAMS((void));
void alp PARAMS((void));
void amin PARAMS((void));
void adoll PARAMS((void));
void astar PARAMS((void));
void checkreg PARAMS((void));
void checkrp PARAMS((void));
int setbr PARAMS((int value));
int getbr PARAMS((void));
void expres PARAMS((void));
void expres1 PARAMS((void));
void binop PARAMS((void));
void exnum1 PARAMS((void));
void exnum PARAMS((void));
void brack PARAMS((void));
void oprand PARAMS((void));
void excmbin PARAMS((void));
void exrsh PARAMS((void));
void exlsh PARAMS((void));
void exmod PARAMS((void));
void exadd PARAMS((void));
void exsub PARAMS((void));
void exand PARAMS((void));
void exor PARAMS((void));
void exmul PARAMS((void));
void exdiv PARAMS((void));
void exnot PARAMS((void));
void eoprnd PARAMS((void));
void combin PARAMS((char *table));
int maprel PARAMS((int flags));

#endif

