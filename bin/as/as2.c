/* as2.c */

#include <stdio.h> /* temp */
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#ifdef pdp11
typedef int intptr_t;
#include <sys/file.h>
#define SEEK_SET L_SET
#endif
#include "as0.h"
#include "as2.h"

/* Sept 10, 1997 - fix coredump caused by using wrong error reporting */
/*	calling convention in three places. */

/*	.globl	_signal, _close, _lseek, _unlink, _umask, _chmod, __exit */
/*	.globl	_write, _read, _brk, _end, _open, _realloc, _fchmod */
/*	.globl	pass1, hshsiz, outmod, dot, dotdot, error */
/*	.globl	checkeos, curfb, savdot, ch, line, savop, inbuf, errflg */
/*	.globl	fbptr, fbtbl, symnum, hshtab, symblk, symleft, dotrel */
/*	.globl	symtab, aexit, overlaid, defund, a.outp, passno, filerr */
/*	.globl	wrterr, argb, curfb, nxtfb, usymtab */
/*	.globl	fin, fout, a.tmp1, ibufc, ibufp, obufp, outbuf, symbol */
/*	.globl	PSTENTSZ, SYMENTSZ, SYMBLKSZ, Newsym */

intptr_t *adrptr; /* r5 */
int opcode; /* (sp) or 2(sp) if rvalue is present */
int rvalue; /* (sp) */
#ifdef LISTING
FILE *argfp;
#endif

/* pass1: */
/*	mov	fout,fin		/ tmp file is now input file */
/*	mov	$666,-(sp)		/ mode */
/*	mov	$3001 ,-(sp)		/ O_WRONLY|O_CREAT|O_TRUNC */
/*	mov	a.outp,-(sp)		/ filename */
/*	jsr	pc,_open */
/*	add	$6,sp */
/*	mov	r0,fout			/ file descriptor any good? */
/*	bpl	1f			/ yes - br */
/*	mov	a.outp,-(sp) */
/*	jsr	pc,filerr */
/* 1: */

void pass1() {
	int symtot;
	struct symbol *psymbol;
	struct symbol **ppsymbol;
	int flags;

	fin = fout;
	fout = open(a_outp, O_WRONLY|O_CREAT|O_TRUNC, 0666);
	if (fout < 0)
		filerr(a_outp);

/* 'symnum' has number of symbols.  The hashtable from pass 0 is no */
/* longer needed - we can reuse it directly for 'usymtab' if there are less  */
/* than 'hshsiz' symbols.  If there are more than 'hshsiz' (currently */
/* 1500) symbols we have to realloc. */

/* The 'fb' table (usually 0 long) is appended to the 'usymtab' (4 byte */
/* entries per symbol). */

/*	mov	fbptr,r0 */
/*	sub	fbtbl,r0		/ # bytes in 'fb' table */
/*	asr	r0			/ convert to number of words */
/*	add	symnum,r0		/ add in number of symbols twice */
/*	add	symnum,r0		/ because we need 2 words per symbol */
/*	inc	r0			/ one more for terminator word */

	symtot = (fbptr - fbtbl) + symnum + 1;

/*	cmp	r0,$hshsiz		/ is hashtable big enough already? */
/*	blo	1f			/ yes -br */
/*	asl	r0			/ convert to bytes */
/*	mov	r0,-(sp) */
/*	mov	hshtab,-(sp) */
/*	jsr	pc,_realloc		/ hshtab = realloc(hshtab, r0) */
/*	mov	r0,hshtab */
/*	bne	1f */
/*	iot				/ should never happen */
/* 1: */

	if (symtot > hshsiz) {
		hshtab = (struct symbol **)realloc(hshtab, symtot * sizeof(struct symbol *));
		if (hshtab == NULL)
			nomem();
	}

/*	mov	hshtab,r1 */
/*	mov	usymtab,r2 */
/* 9: */
/*	mov	r2,symblk		/ save ptr to start of block */
/*	tst	(r2)+			/ skip link word */
/*	mov	$SYMBLKSZ,symleft	/ init amount left in block */

	ppsymbol = hshtab;
	symblk = usymtab;
	do {
		psymbol = symblk->data;
		symleft = SYMBLKSZ;

/* 1: */
/*	tst	(r2)			/ end of symbol table block */
/*	beq	4f			/ yes - br */
/*	add	$8.,symsiz		/ size of symbol table */
/*	tst	Newsym			/ are we doing new style? */
/*	bne	8f			/ yes - br */
/*	add	$4,symsiz		/ no, symbol table entries are bigger */
/* 8: */

		do {
			if (psymbol->name == NULL)
				break;
/* printf("%s\n", psymbol->name); */
			siz[3] += Newsym ? 8 : 12;

/*	mov	2(r2),r0		/ flags word */
/*	bic	$!37,r0 */
/*	cmp	r0,$2			/text */
/*	blo	2f */
/*	cmp	r0,$3			/data */
/*	bhi	2f */
/*	add	$31,r0			/mark "estimated" */
/*	mov	r0,(r1)+		/ store flags word */
/*	mov	4(r2),(r1)+		/ copy value word */
/*	br	3f */
/* 2: */
/*	clr	(r1)+ */
/*	clr	(r1)+ */
/* 3: */

			flags = psymbol->flags & 037;
			if (flags == FTEXT || flags == FDATA)
				psymbol->flags = flags + FESTTEXT - FTEXT;
			else {
				psymbol->flags = 0;
				psymbol->value = 0;
			}

/*	add	$SYMENTSZ,r2		/ skip to next symbol entry */
/*	sub	$SYMENTSZ,symleft	/ one symbol less in block */
/*	cmp	symleft,$SYMENTSZ	/ room for another symbol? */
/*	bge	1b			/ yes - br */

/* printf("0x%08x 0x%08x\n", (ppsymbol - hshtab), psymbol->number); */
			*ppsymbol++ = psymbol++;
			symleft--;
		} while (symleft);

/* 4: */
/*	mov	*symblk,r2		/ follow link to next block */
/*	bne	9b			/ if not at end */
/* 1: */

		symblk = symblk->next;
	} while (symblk);

/* The 'fb' table needs to be appended to the 'usymtab' table now */

/*	mov	fbtbl,r0 */
/*	mov	r1,fbbufp		/ save start of 'fb' table */
/* 1: */
/*	cmp	r0,fbptr		/ at end of table? */
/*	bhis	2f			/ yes - br */
/*	mov	(r0)+,r4 */
/*	add	$31,r4			/ "estimated" */
/*	mov	r4,(r1)+ */
/*	mov	(r0)+,(r1)+ */
/*	br	1b */
/* 2: */

	psymbol = fbtbl;
	fbbufp = ppsymbol;
	while (psymbol < fbptr) {
		psymbol->flags += FESTTEXT - FTEXT; /* FESTBSS ??? */
		*ppsymbol++ = psymbol++;
	}

/*	mov	r1,endtable */
/*	mov	$100000,(r1)+ */

	endtable = ppsymbol;
	*ppsymbol++ = NULL;

/*	mov	$savdot,r0		/ reset the 'psect' (text,data,bss) */
/*	clr	(r0)+			/ counters for the next pass */
/*	clr	(r0)+ */
/*	clr	(r0)+ */
/*	jsr	pc,setup		/ init fb stuff */

	savdot[0] = 0;
	savdot[1] = 0;
	savdot[2] = 0;
	setup();

/*	jsr	pc,pass1_2		/ do pass 1 */

	pass1_2();

/* prepare for pass 2 */
/*	inc	passno */
/*	cmp	outmod,$777 */
/*	beq	1f */
/*	jsr	pc,aexit */
/* not reached */
/* 1: */

#ifdef LISTING
	printf("pass 2\n");
#endif
	passno++;
	if (outmod != 0777)
		aexit();

/*	jsr	pc,setup */
/*	inc	bsssiz */
/*	bic	$1,bsssiz */
/*	mov	txtsiz,r1 */
/*	inc	r1 */
/*	bic	$1,r1 */
/*	mov	r1,txtsiz */
/*	mov	datsiz,r2 */
/*	inc	r2 */
/*	bic	$1,r2 */
/*	mov	r2,datsiz */

	setup();
	siz[0] = (siz[0] + 1) & ~1;
	siz[1] = (siz[1] + 1) & ~1;
	siz[2] = (siz[2] + 1) & ~1;

/*	mov	r1,r3 */
/*	mov	r3,datbase		/ txtsiz */
/*	mov	r3,savdot+2 */
/*	add	r2,r3 */
/*	mov	r3,bssbase		/ txtsiz+datsiz */
/*	mov	r3,savdot+4 */

	base[1] = siz[0];
	savdot[1] = base[1];
	base[2] = base[1] + siz[1];
	savdot[2] = base[2];

/*	clr	r0 */
/*	asl	r3 */
/*	adc	r0 */
/*	add	$20,r3 */
/*	adc	r0 */
/*	mov	r3,symseek+2		/ 2*txtsiz+2*datsiz+20 */
/*	mov	r0,symseek */

	seek[6] = (off_t)base[2] * 2 + 020;

/*	sub	r2,r3 */
/*	sbc	r0 */
/*	mov	r3,drelseek+2		/ 2*txtsiz+datsiz */
/*	mov	r0,drelseek */

	seek[4] = seek[6] - siz[1];

/*	sub	r1,r3 */
/*	sbc	r0 */
/*	mov	r3,trelseek+2		/ txtsiz+datsiz+20 */
/*	mov	r0,trelseek */

	seek[3] = seek[4] - siz[0];

/*	sub	r2,r3 */
/*	sbc	r0 */
/*	mov	r0,datseek */
/*	mov	r3,datseek+2		/ txtsiz+20 */

	seek[1] = seek[3] - siz[1];
	seek[0] = 020;

/*	mov	hshtab,r1 */
/* 1: */
/*	jsr	pc,doreloc */
/*	add	$4,r1 */
/*	cmp	r1,endtable */
/*	blo	1b */

	for (ppsymbol = hshtab; ppsymbol < endtable; ppsymbol++)
		doreloc(*ppsymbol);

/*	clr	r0 */
/*	clr	r1 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,oset */
/*	mov	trelseek,r0 */
/*	mov	trelseek+2,r1 */
/*	mov	$relp,-(sp) */
/*	jsr	pc,oset */

	oset(&txtp, 0L);
	oset(&relp, seek[3]);

/*	mov	$8.,r2 */
/*	mov	$txtmagic,r1 */
/* 1: */
/*	mov	(r1)+,r0 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */
/*	sob	r2,1b */

	p1putw(&txtp, 0407);
	p1putw(&txtp, siz[0]);
	p1putw(&txtp, siz[1]);
	p1putw(&txtp, siz[2]);
	p1putw(&txtp, siz[3]);
	p1putw(&txtp, 0);
	p1putw(&txtp, 0);
	p1putw(&txtp, 0);

/*	jsr	pc,pass1_2			/ do pass 2 */

	pass1_2();
#ifdef LISTING
	if (argfp)
		fclose(argfp);
#endif

/*polish off text and relocation */

/*	mov	$txtp,-(sp) */
/*	jsr	pc,flush */
/*	mov	$relp,-(sp) */
/*	jsr	pc,flush */

	flush(&txtp);
	flush(&relp);

/* append full symbol table */
/*	mov	symseek,r0 */
/*	mov	symseek+2,r1 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,oset */

	oset(&txtp, seek[6]);

/*	mov	usymtab,r2		/ pointer to first symbol block */
/*	mov	hshtab,r1		/ 'type' and 'value' array */
/*	tst	Newsym */
/*	beq	8f */
/*	jsr	pc,nsymout */
/*	br	9f */
/* 8: */
/*	jsr	pc,osymout */
/* 9: */

	if (Newsym)
		nsymout();
	else
		osymout();

/*	mov	$txtp,-(sp) */
/*	jsr	pc,flush */
/*	jsr	pc,aexit */
/* not reached */

	flush(&txtp);
	aexit();
}

/* saexit: */
/*	mov	pc,errflg */

void saexit() {
	errflg = 3;
	aexit();
}

/* aexit: */
/*	mov	$a.tmp1,-(sp)		/ unlink(a.tmp1) */
/*	jsr	pc,_unlink */

int aexit() {
#if 1
	unlink(a_tmp1);
#endif

/*	tst	errflg */
/*	bne	2f */

	if (errflg == 0) {

/*	clr	(sp) */
/*	jsr	pc,_umask */

/*	bic	r0,outmod		/ fchmod(fout, outmod&umask(0)) */
/*	mov	outmod,(sp) */
/*	mov	fout,-(sp) */
/*	jsr	pc,_fchmod */
/*	tst	(sp)+ */
/*	clr	(sp) */
/*	br	1f */

		fchmod(fout, outmod & ~umask(0));
		exit(0);
	}

/* 2: */
/*	mov	$2,(sp) */
/* 1: */
/*	jsr	pc,__exit		/ _exit(errflg ? 2 : 0) */

	exit(2);
}

/* filerr: */
/*	mov	2(sp),r0		/ filename string.  no need to clean */
/*	tst	-(sp)			/ stack, this routine goes to saexit. */
/*	mov	r0,-(sp) */
/*	mov	$1,-(sp) */
/* 1: */
/*	tstb	(r0)+ */
/*	bne	1b */
/*	sub	2(sp),r0 */
/*	dec	r0 */
/*	mov	r0,4(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */

void filerr(p) char *p; {
	write(1, p, strlen(p));

/*	mov	$2,-(sp)		/ write(1, "?\n", 2) */
/*	mov	$qnl,-(sp) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */
/*	tst	passno */
/*	bpl	saexit */
/*	rts	pc */

	write(1, "?\n", 2);
	if (passno >= 0)
		saexit();
}

/* osymout: */
/* 9: */
/*	mov	r2,symblk		/ save ptr to current sym block */
/*	tst	(r2)+			/ skip link word */
/*	mov	$SYMBLKSZ,symleft	/ space left in symbol block */
/* 1: */
/*	mov	(r2),r4			/ pointer to symbol name */
/*	beq	4f			/ end of block - br */

void osymout() {
	struct symbol **ppsymbol;
	struct symbol *psymbol;
	char *p;

	for (ppsymbol = hshtab; ppsymbol < endtable; ppsymbol++) {
		psymbol = *ppsymbol;
		p = psymbol->name;
		if (p == NULL)
			break; /* reached the start of the fbtab */

/*	mov	$8.,r5			/ max number to copy */
/*	mov	$symbol,r0		/ destination buffer */
/* 2: */
/*	movb	(r4),(r0)+		/ copy a byte */
/*	beq	6f */
/*	inc	r4			/ non null - bump source ptr */
/* 6: */
/*	sob	r5,2b */

		strncpy(symbol, p, 8);

/* Now put four words of symbol name to the object file */
/*	mov	$4,r5			/ number of words to do */
/*	mov	$symbol,r4 */
/* 6: */
/*	mov	(r4)+,r0		/ word (2 chars) of symbol name */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */
/*	sob	r5,6b */

		p1putw(&txtp, (symbol[0] & 0377) | ((symbol[1] & 0377) << 8));
		p1putw(&txtp, (symbol[2] & 0377) | ((symbol[3] & 0377) << 8));
		p1putw(&txtp, (symbol[4] & 0377) | ((symbol[5] & 0377) << 8));
		p1putw(&txtp, (symbol[6] & 0377) | ((symbol[7] & 0377) << 8));

/* values from 'hshtab' (parallel array to symbol table) are retrieved now, */
/* they take the place of the flags and value entries of the symbol table. */
/*	mov	(r1)+,r0 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */
/*	mov	(r1)+,r0 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */

		p1putw(&txtp, psymbol->flags);
		p1putw(&txtp, psymbol->value);

/*	add	$SYMENTSZ,r2		/ skip to next symbol */
/*	sub	$SYMENTSZ,symleft	/ one less symbol in block */
/*	cmp	symleft,$SYMENTSZ	/ room for another? */
/*	bge	1b			/ yes - br */
/* 4: */
/*	mov	*symblk,r2		/ no, follow link to next block */
/*	bne	9b			/ unless it's end of list */
/*	rts	pc */

	}
}

/* nsymout: */
/*	clr	totalsz */
/*	mov	$4,totalsz+2		/ string table min size is 4 */

void nsymout() {
	struct symbol **ppsymbol;
	struct symbol *psymbol;
	char *p;

	totalsz = 4;

/* 9: */
/*	mov	r2,symblk		/ save ptr to current symbol block */
/*	tst	(r2)+			/ skip link word */
/*	mov	$SYMBLKSZ,symleft	/ amount of space left in block */
/* 1: */
/*	mov	(r2),r4			/ pointer to symbol's string */
/*	beq	4f			/ end of block - br */

	for (ppsymbol = hshtab; ppsymbol < endtable; ppsymbol++) {
		psymbol = *ppsymbol;
		p = psymbol->name;
		if (p == NULL)
			break; /* reached the start of the fbtab */

/*	mov	totalsz,r0		/ now output the... */
/*	mov	$txtp,-(sp)		/ high order of the string index... */
/*	jsr	pc,putw			/ to the object file */
/*	mov	totalsz+2,r0 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw			/ and the low order word */
/* 2: */
/*	tstb	(r4)+			/ find the end of the string */
/*	bne	2b */
/*	sub	(r2),r4			/ compute length including the null */
/*	add	r4,totalsz+2		/ offset of next string */
/*	adc	totalsz */

		p1putw(&txtp, (int)(totalsz >> 16) & 0177777);
		p1putw(&txtp, (int)totalsz & 0177777);
		totalsz += strlen(p) + 1;

/*	mov	(r1)+,r0		/ 'type' word of symbol */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */
/*	mov	(r1)+,r0		/ 'value' word of symbol */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */

		p1putw(&txtp, psymbol->flags);
		p1putw(&txtp, psymbol->value);

/*	add	$SYMENTSZ,r2		/ advance to next symbol */
/*	sub	$SYMENTSZ,symleft	/ adjust amount left in symbol block */
/*	cmp	symleft,$SYMENTSZ	/ is there enough for another symbol? */
/*	bge	1b			/ yes - br */
/* 4: */
/*	mov	*symblk,r2		/ follow link to next symbol block */
/*	bne	9b			/ more - br */

	}

/*	mov	totalsz,r0		/ now output the string table length */
/*	mov	$txtp,-(sp)		/ high order word first */
/*	jsr	pc,putw */
/*	mov	totalsz+2,r0		/ followed by the low order */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */

	p1putw(&txtp, (int)(totalsz >> 16) & 0177777);
	p1putw(&txtp, (int)totalsz & 0177777);

/* Now write the strings out */

/*	mov	usymtab,r2		/ start at beginning of symbols */
/* 9: */
/*	mov	r2,symblk		/ save pointer to current block */
/*	tst	(r2)+			/ skip link word */
/*	mov	$SYMBLKSZ,symleft	/ amount left in block */
/* 1: */
/*	mov	(r2),r4			/ pointer to symbol's string */
/*	beq	4f			/ at end of block - br */

	for (ppsymbol = hshtab; ppsymbol < endtable; ppsymbol++) {
		psymbol = *ppsymbol;
		p = psymbol->name;
		if (p == NULL)
			break; /* reached the start of the fbtab */

/*	jsr	pc,putstring		/ write out the string */

		putstring(p);

/*	add	$SYMENTSZ,r2		/ advance to next symbol */
/*	sub	$SYMENTSZ,symleft	/ adjust amount left in block */
/*	cmp	symleft,$SYMENTSZ	/ enough for another symbol? */
/*	bge	1b			/ yes - br */
/* 4: */
/*	mov	*symblk,r2		/ move to next block of symbols */
/*	bne	9b			/ any left - br */

	}

/* probably not necessary but let us leave the file size on an even */
/* byte boundary. */

/*	bit	$1,totalsz+2		/ odd number of bytes in string table? */
/*	beq	5f			/ no - br */
/*	mov	symblk,r4		/ we know 'symblk' points to a null */
/*	jsr	pc,putstring		/ output a single null */
/* 5: */
/*	rts	pc */

	if (totalsz & 1) {
		putstring("");
	}
}

/* R4 has the address of a null terminated string to write to the output */
/* file.  The terminating null is included in the output.  This routine */
/* "inlines" the 'txtp seek structure' manipulation because the 'putw' */
/* routine was 1) not suitable to byte output and 2) symbol strings are */
/* only written using the 'txtp' (as opposed to 'relp' - relocation info) */
/* structure. */

/* putstring: */

void putstring(p) char *p; {
	while (1) {

/*	cmp	txtp,txtp+2		/ room for another byte? */
/*	bhis	1f			/ no - br */
/* 3: */
/*	movb	(r4),*txtp		/ put byte in buffer */
/*	inc	txtp			/ advance output position */
/*	tstb	(r4)+			/ did we just do the null? */
/*	bne	putstring		/ no - go again */
/*	rts	pc			/ yes - we're done, return */

		while (txtp.append < txtp.limit) {
			*txtp.append++ = *p;
			if (*p++ == 0)
				return;
		}

/* 1: */
/*	mov	r2,-(sp)		/ save r2 from being destroyed */
/*	mov	$txtp,-(sp)		/ flush buffered output and... */
/*	jsr	pc,flush		/ reset the pointers */
/*	mov	(sp)+,r2		/ restore symbol pointer */
/*	br	3b			/ go output a byte */

		flush(&txtp);
	}
}

/* doreloc: */
/*	movb	(r1),r0 */
/*	bne	1f */
/*	bisb	defund,(r1) */
/* 1: */

void doreloc(psymbol) struct symbol *psymbol; {
	int flags;

	flags = psymbol->flags;
	if (flags == 0)
		psymbol->flags = defund;

/*	bic	$!37,r0 */
/*	cmp	r0,$5 */
/*	bhis	1f */
/*	cmp	r0,$3 */
/*	blo	1f */
/*	beq	2f */

	flags &= 037;
	switch (flags) {

/*	add	bssbase,2(r1) */
/*	rts	pc */

	case FBSS:
		psymbol->value += base[2];
		break;

/* 2: */
/*	add	datbase,2(r1) */
/* 1: */
/*	rts	pc */

	case FDATA:
		psymbol->value += base[1];
		break;
	}
}

/* setup: */
/*	clr	dot */
/*	mov	$2,dotrel */
/*	mov	$..,dotdot */
/*	clr	brtabp */

void setup() {
	int label;

	pdot->value = 0;
	pdot->flags = FTEXT;
	pdotdot->value = 0;
	brtabp = 0;

/*	mov	$curfb,r4 */
/* 1: */
/*	clr	(r4)+ */
/*	cmp	r4,$curfb+40. */
/*	blo	1b */

	memset(curfb, 0, 10 * sizeof(intptr_t));
	memset(nxtfb, 0, 10 * sizeof(intptr_t));

/*	clr	r4 */
/* 1: */
/*	jsr	pc,fbadv */
/*	inc	r4 */
/*	cmp	r4,$10. */
/*	blt	1b */

	for (label = 0; label < 10; label++)
		fbadv(label);

/* just rewind /tmp/atm1xx rather than close and re-open */
/*	clr	-(sp) */
/*	clr	-(sp) */
/*	clr	-(sp) */
/*	mov	fin,-(sp) */
/*	jsr	pc,_lseek		/ lseek(fin, 0L, 0) */
/*	add	$8.,sp */

	lseek(fin, 0L, SEEK_SET);

/*	clr	ibufc */
/*	rts	pc */

	ibufc = 0;
}

/* outw: */
/*	cmp	dot-2,$4 */
/*	beq	9f */

void outw(value, flags) int value; int flags; {
	int relocate;

#ifdef LISTING
	if (passno)
		printf("0%06o: 0%06o (0%06o)\n", pdot->value, value & 0177777,
				flags);
#endif
	if (pdot->flags != FBSS) {

/*	bit	$1,dot */
/*	bne	1f */

		if ((pdot->value & 1) == 0) {

/*	add	$2,dot */
/*	tst	passno */
/*	beq	8f */

			pdot->value += 2;
			if (passno == 0)
				return;

/*	clr	-(sp) */
/*	rol	r3 */
/*	adc	(sp) */
/*	asr	r3			/ get relative pc bit */
/*	cmp	r3,$40 */
/*	bne	2f */

			relocate = (flags >> 15) & 1;
			flags &= 077777;
			if (flags == FGLOBAL) {

/* external references */
/*	mov	$666,outmod		/ make nonexecutable */
/*	mov	xsymbol,r3 */
/*	sub	hshtab,r3 */
/*	asl	r3 */
/*	bis	$4,r3			/ external relocation */
/*	br	3f */
/* 2: */

				outmod = 0666;
				flags = (xsymbol->number << 3) | 4;
			}

/*	bic	$40,r3			/ clear any ext bits */
/*	cmp	r3,$5 */
/*	blo	4f */
/*	cmp	r3,$33			/ est. text, data */
/*	beq	6f */
/*	cmp	r3,$34 */
/*	bne	7f */
/* 6: */
/*	mov	$'r,-(sp) */
/*	jsr	pc,error */
/* 7: */
/*	mov	$1,r3			/ make absolute */
/* 4: */

			else {
				flags &= ~FGLOBAL;
				if (flags > FBSS) {
					if (flags == FESTTEXT ||
							flags == FESTDATA)
						error('r');
					flags = FABS;
				}

/*	cmp	r3,$2 */
/*	blo	5f */
/*	cmp	r3,$4 */
/*	bhi	5f */
/*	tst	(sp) */
/*	bne	4f */
/*	add	dotdot,r2 */
/*	br	4f */
/* 5: */

				if (flags >= FTEXT && flags <= FBSS) {
					if (!relocate)
						value += pdotdot->value;
				}

/*	tst	(sp) */
/*	beq	4f */
/*	sub	dotdot,r2 */
/* 4: */

				else {
					if (relocate)
						value -= pdotdot->value;
				}

/*	dec	r3 */
/*	bpl	3f */
/*	clr	r3 */

				if (flags)
					flags--;
			}

/* 3: */
/*	asl	r3 */
/*	bis	(sp)+,r3 */

			flags = (flags << 1) | relocate;

/*	mov	r2,r0 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */
/*	mov	tseekp,r0 */
/*	add	$2,2(r0) */
/*	adc	(r0) */

			p1putw(&txtp, value);
			*tseekp += 2;

/*	mov	r3,r0 */
/*	mov	$relp,-(sp) */
/*	jsr	pc,putw */
/*	mov	rseekp,r0 */
/*	add	$2,2(r0) */
/*	adc	(r0) */

			p1putw(&relp, flags);
			*rseekp += 2;

/* 8: */
/*	rts	pc */

		}

/* 1: */
/*	mov	$'o,-(sp) */
/*	jsr	pc,error */
/*	clr	r3 */
/*	jsr	pc,outb */
/*	rts	pc */

		else {
			error('o');
			outb(value, 0);
		}

/* 9: */
/*	mov	$'x,-(sp) */
/*	jsr	pc,error */
/*	rts	pc */

	}
	else
		error('x');
}

/* outb: */
/*	cmp	dot-2,$4		/ test bss mode */
/*	beq	9b */

void outb(value, flags) int value; int flags; {
#ifdef LISTING
	if (passno)
		printf("0%06o:    0%03o (0%06o)\n", pdot->value, value & 0377,
				flags);
#endif
	if (pdot->flags == FBSS) {
		error('x');
		return;
 /* bug!! didn't increment dot's value */
	}

/*	cmp	r3,$1 */
/*	blos	1f */
/*	mov	$'r,-(sp) */
/*	jsr	pc,error */
/* 1: */

	if (flags > FABS)
		error('r');

/*	tst	passno */
/*	beq	2f */

	if (passno) {

/*	mov	r2,r0 */
/*	bit	$1,dot */
/*	bne	1f */

		if ((pdot->value & 1) == 0) {

/*	mov	$txtp,-(sp) */
/*	jsr	pc,putw */
/*	clr	r0 */
/*	mov	$relp,-(sp) */
/*	jsr	pc,putw */

			p1putw(&txtp, value);
			p1putw(&relp, 0);

/*	mov	tseekp,r0 */
/*	add	$2,2(r0) */
/*	adc	(r0) */
/*	mov	rseekp,r0 */
/*	add	$2,2(r0) */
/*	adc	(r0) */
/*	br	2f */

			*tseekp += 2;
			*rseekp += 2;
		}

/* 1: */
/*	mov	txtp,r0 */
/*	movb	r2,-1(r0) */

		else
			*(txtp.append - 1) = (char)value;
	}

/* 2: */
/*	inc	dot */
/*	rts	pc */

	pdot->value++;
}

/* pass 1 and 2 common code */

/* pass1_2: */
/*	jsr	pc,readop */
/*	cmp	r4,$5 */
/*	beq	2f */
/*	cmp	r4,$'< */
/*	beq	2f */
/*	jsr	pc,checkeos */
/*		br eal1 */

void pass1_2() {
	intptr_t tokensave;
	int flags;
	struct symbol *psymbol;
	int label;
#ifdef LISTING
	int argch;
#endif

	while (1) {
		readop();
		if (token != TNEWFILE && token != TSTRING) {
			if (checkeos())
				goto eal1;

/*	mov	r4,-(sp) */
/*	cmp	(sp),$1 */
/*	bne	1f */
/*	mov	$2,(sp) */
/*	jsr	pc,getw */
/*	mov	r4,numval */
/* 1: */

			tokensave = token;
			if (token == TABS) {
				tokensave = TABS2;
				p1getw();
				numval = token;
			}

/*	jsr	pc,readop */
/*	cmp	r4,$'= */
/*	beq	4f */
/*	cmp	r4,$': */
/*	beq	1f */
/*	mov	r4,savop */
/*	mov	(sp)+,r4 */
/* 2: */
/*	jsr	pc,opline */

			readop();
			if (token == TEQUAL)
				goto equal;
			if (token == TCOLON)
				goto colon;
			savop = token;
			token = tokensave;
		}
		opline();

/* dotmax: */
/*	tst	passno */
/*	bne	eal1 */
/*	movb	dotrel,r0 */
/*	asl	r0 */
/*	cmp	dot,txtsiz-4(r0) */
/*	bhi	8f */
/*	jmp	ealoop */
/* 8: */
/*	mov	dot,txtsiz-4(r0) */
/* eal1: */
/*	jmp	ealoop */

	dotmax:
		if (passno == 0) {
			flags = pdot->flags - FTEXT;
			if (siz[flags] < pdot->value)
				siz[flags] = pdot->value;
		}
	eal1:
		goto ealoop;

/* 1: */
/*	mov	(sp)+,r4 */
/*	cmp	r4,$200 */
/*	bhis	1f */
/*	cmp	r4,$2 */
/*	beq	3f */
/*	mov	$'x,-(sp) */
/*	jsr	pc,error */
/*	br	pass1_2 */
/* 1: */

	colon:
		if (tokensave >= 0 && tokensave < TASCII) {
			if (tokensave != TABS2) {
				error('x');
				continue;
			}
			goto digit;
		}

/*	tst	passno */
/*	bne	2f */
/*	movb	(r4),r0 */
/*	bic	$!37,r0 */
/*	beq	5f */
/*	cmp	r0,$33 */
/*	blt	6f */
/*	cmp	r0,$34 */
/*	ble	5f */
/* 6: */
/*	mov	$'m,-(sp) */
/*	jsr	pc,error */
/* 5: */

		psymbol = (struct symbol *)tokensave;
		if (passno == 0) {
			flags = psymbol->flags & 037;
			if (flags && (flags < FESTTEXT || flags > FESTDATA))
				error('m');

/*	bic	$37,(r4) */
/*	bis	dotrel,(r4) */
/*	mov	2(r4),brdelt */
/*	sub	dot,brdelt */
/*	mov	dot,2(r4) */
/*	br	pass1_2 */

			psymbol->flags = (psymbol->flags & ~037) | pdot->flags;
			brdelt = psymbol->value - pdot->value;
			psymbol->value = pdot->value;
			continue;
		}

/* 2: */
/*	cmp	dot,2(r4) */
/*	beq	pass1_2 */
/*	mov	$'p,-(sp) */
/*	jsr	pc,error */
/*	br	pass1_2 */

		if (psymbol->value != pdot->value)
			error('p');
#ifdef LISTING
		printf("0%06o: %s\n", psymbol->value & 0177777, psymbol->name);
#endif
		continue;

/* 3: */
/*	mov	numval,r4 */
/*	jsr	pc,fbadv */
/*	asl	r4 */
/*	mov	curfb(r4),r0 */
/*	movb	dotrel,(r0) */
/*	mov	2(r0),brdelt */
/*	sub	dot,brdelt */
/*	mov	dot,2(r0) */
/*	br	pass1_2 */

	digit:
		label = numval;
		fbadv(label);
		psymbol = *(struct symbol **)curfb[label];
		psymbol->flags = pdot->flags;
		brdelt = psymbol->value - pdot->value;
		psymbol->value = pdot->value;
#ifdef LISTING
		if (passno)
			printf("0%06o: %d\n", psymbol->value & 0177777, label);
#endif
		continue;

/* 4: */
/*	jsr	pc,readop */
/*	jsr	pc,expres */
/*	mov	(sp)+,r1 */
/*	cmp	r1,$dotrel		/test for dot */
/*	bne	1f */
/*	bic	$40,r3 */
/*	cmp	r3,dotrel		/ can't change relocation */
/*	bne	2f */

	equal:
		readop();
		expres();
		psymbol = (struct symbol *)tokensave;
		if (psymbol == pdot) {
			leftflags &= ~FGLOBAL;
/* printf("leftflags 0%o pdot->flags 0%o\n", leftflags, pdot->flags); */
			if (leftflags != pdot->flags)
				goto doterr;

/*	cmp	r3,$4			/ bss */
/*	bne	3f */
/*	mov	r2,dot */
/*	br	dotmax */
/* 3: */

			if (leftflags == FBSS) {
				pdot->value = leftvalue;
#ifdef LISTING
			if (passno)
				printf("0%06o: .\n", pdot->value & 0177777);
#endif
				goto dotmax;
			}

/*	sub	dot,r2 */
/*	bmi	2f */
/*	mov	r2,-(sp) */
/* 3: */
/*	dec	(sp) */
/*	bmi	3f */
/*	clr	r2 */
/*	mov	$1,r3 */
/*	jsr	pc,outb */
/*	br	3b */
/* 3: */
/*	tst	(sp)+ */
/*	br	dotmax */

			leftvalue -= pdot->value;
			if (leftvalue < 0)
				goto doterr;
			while (leftvalue) {
				leftvalue--;
				outb(0, FABS);
			}
			goto dotmax;

/* 2: */
/*	mov	$'.,-(sp) */
/*	jsr	pc,error */
/*	br	ealoop */
/* 1: */

		doterr:
			error('.');
			goto ealoop;
		}

/*	cmp	r3,$40 */
/*	bne	1f */
/*	mov	$'r,-(sp) */
/*	jsr	pc,error */
/* 1: */

		if (leftflags == FGLOBAL)
			error('r');

/*	bic	$37,(r1) */
/*	bic	$!37,r3 */
/*	bne	1f */
/*	clr	r2 */
/* 1: */

		psymbol->flags &= ~037;
		leftflags &= 037;
		if (leftflags == 0)
			leftvalue = 0;

/*	bisb	r3,(r1) */
/*	mov	r2,2(r1) */

		psymbol->flags |= leftflags;
		psymbol->value = leftvalue;
#ifdef LISTING
		if (passno)
			printf("0%06o: %s\n", psymbol->value & 0177777,
					psymbol->name);
#endif

/* ealoop: */
/*	cmp	r4,$'\n */
/*	beq	1f */
/*	cmp	r4,$'\e */
/*	bne	9f */
/*	rts	pc */
/* 1: */
/*	inc	line */
/* 9: */
/*	jmp	pass1_2 */

	ealoop:
/* printf("ealoop token = 0x%08x\n", token); */
		if (token == TNEWLINE) {
			line++;
#ifdef LISTING
			if (argfp)
				while ((argch = getc(argfp)) != EOF) {
					putchar(argch);
					if (argch == '\n')
						break;
				}
#endif
		}
		else if (token == TENDFILE)
			break;
	}
}

/* checkeos: */
/*	cmp	r4,$'\n */
/*	beq	1f */
/*	cmp	r4,$'; */
/*	beq	1f */
/*	cmp	r4,$'\e */
/*	beq	1f */
/*	add	$2,(sp) */
/* 1: */
/*	rts	pc */

int checkeos() {
	return token == TNEWLINE || token == TSEMICOLON || token == TENDFILE;
}

/* fbadv: */
/*	asl	r4 */
/*	mov	nxtfb(r4),r1 */
/*	mov	r1,curfb(r4) */
/*	bne	1f */
/*	mov	fbbufp,r1 */
/*	br	2f */
/* 1: */
/*	add	$4,r1 */

void fbadv(label) int label; {
	struct symbol **ppsymbol;
	struct symbol *psymbol;

	ppsymbol = (struct symbol **)nxtfb[label];
	curfb[label] = (intptr_t)ppsymbol;
	if (ppsymbol == NULL) {
		ppsymbol = fbbufp;
		goto entry;
	}
	do {
		ppsymbol++;

/* 2: */
/*	cmpb	1(r1),r4 */
/*	beq	1f */
/*	tst	(r1) */
/*	bpl	1b */
/* 1: */

	entry:
		psymbol = *ppsymbol;
	} while (psymbol && psymbol->number != label);

/*	mov	r1,nxtfb(r4) */
/*	asr	r4 */
/*	rts	pc */

	nxtfb[label] = (intptr_t)ppsymbol;
}

/* oset: */
/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */
/*	mov	6(sp),r3 */
/*	mov	r1,r2 */
/*	bic	$!1777,r1 */
/*	add	r3,r1 */
/*	add	$8,r1 */
/*	mov	r1,(r3)+		/ next slot */

void oset(pstream, offset) struct stream *pstream; off_t offset; {
	pstream->append = pstream->data + ((int)offset & 01777);

/*	mov	r3,r1 */
/*	add	$2006,r1 */
/*	mov	r1,(r3)+		/ buf max */

	pstream->limit = pstream->data + 02000;

/*	mov	r0,(r3)+ */
/*	mov	r2,(r3)+		/ seek addr */

	pstream->offset = offset;

/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */
/*	mov	(sp)+,(sp) */
/*	rts	pc */

}

/* putw: */
/*	mov	r1,-(sp) */
/*	mov	r2,-(sp) */
/*	mov	6(sp),r2 */
/*	mov	(r2)+,r1		/ slot */
/*	cmp	r1,(r2)			/ buf max */
/*	bhis	1f */
/*	mov	r0,(r1)+ */
/*	mov	r1,-(r2) */
/*	br	2f */
/* 1: */
/*	tst	(r2)+ */
/*	mov	r0,-(sp) */
/*	jsr	pc,flush1 */
/*	mov	(sp)+,r0 */
/*	mov	r0,*(r2)+ */
/*	add	$2,-(r2) */
/* 2: */
/*	mov	(sp)+,r2 */
/*	mov	(sp)+,r1 */
/*	mov	(sp)+,(sp) */
/* ret: */
/*	rts	pc */

void p1putw(pstream, word) struct stream *pstream; int word; {
/* printf("p1putw 0x%08x 0%06o\n", pstream, word); */
	if (pstream->append >= pstream->limit)
 /* note!! should be pstream->limit - 1, but it never makes any difference */
		flush(pstream);
	*pstream->append++ = (char)word;
	*pstream->append++ = (char)(word >> 8);
}

/* flush: */
/*	mov	2(sp),r2 */
/*	mov	(sp)+,(sp) */
/*	cmp	(r2)+,(r2)+ */
/* flush1: */
/*	clr	-(sp)			/ lseek(fout, (r2)L+, L_SET) */
/*	mov	2(r2),-(sp) */
/*	mov	(r2)+,-(sp) */
/*	tst	(r2)+ */
/*	mov	fout,-(sp) */
/*	jsr	pc,_lseek */
/*	add	$8.,sp */

void flush(pstream) struct stream *pstream; {
	char *start;
	int count;

	lseek(fout, pstream->offset, SEEK_SET);

/*	cmp	-(sp),-(sp)		/ write(fout, <buf>, <len>) */
/*	bic	$!1777,r1 */
/*	add	r2,r1			/ write address */

	start = pstream->data + ((int)pstream->offset & 01777);

/*	mov	r1,-(sp)		/ { <buf> } */
/*	mov	r2,r0 */
/*	bis	$1777,-(r2) */
/*	add	$1,(r2)			/ new seek addr */
/*	adc	-(r2) */
/*	cmp	-(r2),-(r2) */

	pstream->offset = (pstream->offset | 01777) + 1;

/*	sub	(r2),r1 */
/*	neg	r1 */
/*	mov	r1,2(sp)		/ count */
/*	mov	r0,(r2)			/ new next slot */

	count = pstream->append - start;
	pstream->append = pstream->data;

/*	mov	fout,-(sp) */
/*	mov	r1,6(sp)		/ protect r1 from library */
/*	jsr	pc,_write */
/*	add	$6,sp */
/*	mov	(sp)+,r1 */
/*	tst	r0 */
/*	bpl	ret */
/* fall thru to wrterr */

	if (write(fout, start, count) < 0)
		wrterr();
}

/* wrterr: */
/*	mov	$8f-9f,-(sp)		/ write(1, 9f, 8f-9f) */
/*	mov	$9f,-(sp) */
/*	mov	$1,-(sp) */
/*	jsr	pc,_write */
/*	add	$6,sp */
/*	jsr	pc,saexit */
/* not reached */

void wrterr() {
	write(1, "as: write error\n", 16);
	saexit();
}

/*	.data */
/* 9: */
/*	<as: write error\n> */
/* 8: */
/*	.even */
/*	.text */

/* readop: */
/*	mov	savop,r4 */
/*	beq	1f */
/*	clr	savop */
/*	rts	pc */
/* 1: */

void readop() {
	int word;

/* printf("readop() savop = 0x%08x\n", savop); */
	if (savop) {
		token = savop;
		savop = 0;
		return;
	}

/*	jsr	pc,getw1 */
/*	cmp	r4,$200 */
/*	blo	1f */
/*	cmp	r4,$4000 */
/*	blo	2f */
/*	sub	$4000,r4 */
/*	asl	r4 */
/*	asl	r4 */
/*	add	hshtab,r4 */
/*	rts	pc */
/* 2: */

	word = getw1();
	if (word >= 0 && word < TASCII)
		token = word;
	else {
		if (word >= 04000)
			token = (intptr_t)hshtab[word - 04000];

/* remove PST flag (1000) then multiply by PSTENTSZ.  In pass 0 the PST */
/* symbol number was divided by PSTENTSZ(to make it fit) - we now reverse */
/* that process. */
/*	mov	r5,-(sp) */
/*	mov	r4,r5 */
/*	sub	$1000,r5 */
/*	mul	$PSTENTSZ,r5 */
/*	mov	r5,r4 */
/*	mov	(sp)+,r5 */
/*	add	$dotrel,r4		/ point at dot's flag field */
/* 1: */
/*	rts	pc */

		else
			token = (intptr_t)(symtab + (word - 01000));
	}
}

/* getw: */
/*	mov	savop,r4 */
/*	beq	getw1 */
/*	clr	savop */
/*	rts	pc */

void p1getw() {
	if (savop) {
		token = savop;
		savop = 0;
		return;
	}
	token = getw1();
}

/* getw1: */
/*	dec	ibufc */
/*	bgt	1f */

int getw1() {
	ibufc--;
	if (ibufc <= 0) {

/*	mov	r1,-(sp)		/ protect r1 from library */
/*	mov	$1024.,-(sp)		/ read(fin, inbuf, 1024) */
/*	mov	$inbuf,-(sp) */
/*	mov	fin,-(sp) */
/*	jsr	pc,_read */
/*	add	$6,sp */
/*	mov	(sp)+,r1 */

		ibufc = read(fin, inbuf, 1024);
		if (ibufc <= 0)
			return TENDFILE;

/*	asr	r0 */
/*	mov	r0,ibufc */
/*	bgt	2f */
/*	mov	$4,r4 */
/*	sev */
/*	rts	pc */
/* 2: */
/*	mov	$inbuf,ibufp */
/* 1: */

		ibufc /= sizeof(short);
		ibufp = (short *)inbuf;
 /* note!! rather silly, ibufc doesn't get decremented through this path */
	}

/*	mov	*ibufp,r4 */
/*	add	$2,ibufp */
/*	rts	pc */

	return *ibufp++;
}

/* opline: */
/*	mov	r4,r0 */
/*	bmi	2f */
/*	cmp	r0,$177 */
/*	bgt	2f */
/*	cmp	r4,$5 */
/*	beq	opeof */
/*	cmp	r4,$'< */
/*	bne	xpr */
/*	jmp	opl17 */
/* xxpr: */
/*	tst	(sp)+ */
/* xpr: */
/*	jsr	pc,expres */
/*	jsr	pc,outw */
/*	rts	pc */
/* 2: */

void xpr() {
	expres();
	outw(leftvalue, leftflags);
}

void opline() {
	struct symbol *psymbol;
	int flags;

/* printf("opline() token 0x%08x\n", token); */
	if (token >= 0 && token < TASCII) {
		if (token == TNEWFILE)
			opeof();
		else if (token == TSTRING)
			opl17();
		else
			xpr();
		return;
	}

/*	movb	(r4),r0 */
/*	cmp	r0,$24			/reg */
/*	beq	xpr */
/*	cmp	r0,$33			/est text */
/*	beq	xpr */
/*	cmp	r0,$34			/ est data */
/*	beq	xpr */
/*	cmp	r0,$5 */
/*	blt	xpr */
/*	cmp	r0,$36 */
/*	bgt	xpr */

	psymbol = (struct symbol *)token;
	flags = psymbol->flags;
	if (flags <= FBSS || flags == FREGISTER || flags == FESTTEXT ||
			flags == FESTDATA || flags > FJCOND) {
		xpr();
		return;
	}

/*	mov	2(r4),-(sp) */
/*	mov	r0,-(sp) */
/*	jsr	pc,readop */
/*	mov	(sp)+,r0 */
/*	asl	r0 */
/*	mov	$adrbuf,r5 */
/*	clr	swapf */
/*	mov	$-1,rlimit */
/*	jmp	*1f-10.(r0) */

	opcode = psymbol->value;
	readop();
	adrptr = adrbuf;
	swapf = 0;
	rlimit = 0177777;
	switch (flags) {

/*	.data */
/* 1: */
/*	opl5 */

	case FMOVFO:
		opl5();
		return;

/*	opl6 */

	case FBRANCH:
		opl6();
		return;

/*	opl7 */

	case FJSR:
		opl7();
		return;

/*	opl10 */

	case FRTS:
		opl10();
		return;

/*	opl11 */

	case FSYSTRAP:
		opl11();
		return;

/*	opl12 */

	case FMOVF:
		opl12();
		return;

/*	opl13 */

	case FDOUBLE:
		opl13();
		return;

/*	opl14 */

	case FFLOP:
		opl14();
		return;

/*	opl15 */

	case FSINGLE:
		opl15();
		return;

/*	opl16 */

	case FDOTBYTE:
		opl16();
		return;

#if 1 /* modifications for dec syntax */
	case FDOTWORD:
		opldotword();
		return;
#endif

/*	opl17 */

	case FSTRING:
		opl17();
		return;

/*	opl20 */

	case FDOTEVEN:
		opl20();
		return;

/*	opl21 */

	case FDOTIF:
		opl21();
		return;

/*	opl22 */

	case FDOTENDIF:
		opl22();
		return;

/*	opl23 */

	case FDOTGLOBL:
		opl23();
		return;

/*	xxpr */

	case FREGISTER: /* can never get here */
		xpr();
		return;

/*	opl25 */

	case FDOTTEXT:
		opl25();
		return;

/*	opl26 */

	case FDOTDATA:
		opl26();
		return;

/*	opl27 */

	case FDOTBSS:
		opl27();
		return;

/*	opl30 */

	case FMULDIV:
		opl30();
		return;

/*	opl31 */

	case FSOB:
		opl31();
		return;

/*	opl32 */

	case FDOTCOMM:
		opl32();
		return;

/*	xxpr */
/*	xxpr */

	case FESTTEXT:
	case FESTDATA:
		xpr();
		return;

/*	opl35 */

	case FJBR:
		opl35();
		return;

/*	opl36 */

	case FJCOND:
		opl36();
		return;

/*	.text */

	}
}

/* opeof: */
/*	mov	$1,line */
/*	mov	$20,-(sp) */
/*	mov	$argb,r1 */
/* 1: */
/*	jsr	pc,getw */
/*	tst	r4 */
/*	bmi	1f */
/*	movb	r4,(r1)+ */
/*	dec	(sp) */
/*	bgt	1b */
/*	tstb	-(r1) */
/*	br	1b */
/* 1: */
/*	movb	$'\n,(r1)+ */
/*	clrb	(r1)+ */
/*	tst	(sp)+ */
/*	rts	pc */

void opeof() {
	int count;
	char *p;
#ifdef LISTING
	int argch;
#endif

	line = 1;
	count = 20;
	p = argb;
	while (1) {
		p1getw();
		if (token == -1)
			break;
		if (count) {
			count--;
			*p++ = (char)token;
		}
	}
#ifdef LISTING
	if (passno) {
		if (argfp)
			fclose(argfp);
		*p = 0;
		argfp = fopen(argb, "r");
		if (argfp)
			while ((argch = getc(argfp)) != EOF) {
				putchar(argch);
				if (argch == '\n')
					break;
			}
	}
#endif
	*p++ = '\n';
	*p = 0;
}

/* opl30:					/ mul, div etc */
/*	inc	swapf */
/*	mov	$1000,rlimit */
/*	br	opl13 */

void opl30() {
	swapf = 1;
	rlimit = 01000;
	opl13();
}

/* opl14:					/ flop freg,fsrc */
/*	inc	swapf */

void opl14() {
	swapf = 1;
	opl5();
}

/* opl5:					/ flop src,freg */
/*	mov	$400,rlimit */

void opl5() {
	rlimit = 0400;
	opl13();
}

/* opl13:					/double */
/*	jsr	pc,addres */

void opl13() {
	addres();
	op2a();
}

/* op2a: */
/*	mov	r2,-(sp) */
/*	jsr	pc,readop */

void op2a() {
	rvalue = leftvalue;
	readop();
	op2b();
}

/* op2b: */
/*	jsr	pc,addres */
/*	tst	swapf */
/*	beq	1f */
/*	mov	(sp),r0 */
/*	mov	r2,(sp) */
/*	mov	r0,r2 */
/* 1: */

void op2b() {
	int tempvalue;
	intptr_t *adrtmp;

	addres();
/* printf("op2b %d 0x%08x 0x%08x\n", swapf, rvalue, leftvalue); */
	if (swapf) {
		tempvalue = rvalue;
		rvalue = leftvalue;
		leftvalue = tempvalue;
	}

/*	swab	(sp) */
/*	asr	(sp) */
/*	asr	(sp) */
/*	cmp	(sp),rlimit */
/*	blo	1f */
/*	mov	$'x,-(sp) */
/*	jsr	pc,error */
/* 1: */

#if 1
 rvalue <<= 6;
#else
	rvalue = (rvalue & 0377) << 6; /* seems a bit stupid */
#endif
	if ((unsigned int)rvalue >= (unsigned int)rlimit)
 {
 printf("a 0x%08x 0x%08x\n", rvalue, rlimit);
		error('x');
 }

/*	bis	(sp)+,r2 */
/*	bis	(sp)+,r2 */
/*	clr	r3 */
/*	jsr	pc,outw */

	outw(leftvalue | rvalue | opcode, 0);

/*	mov	$adrbuf,r1 */
/* 1: */
/*	cmp	r1,r5 */
/*	bhis	1f */
/*	mov	(r1)+,r2 */
/*	mov	(r1)+,r3 */
/*	mov	(r1)+,xsymbol */
/*	jsr	pc,outw */
/*	br	1b */
/* 1: */
/*	rts	pc */

	adrtmp = adrbuf;
	while (adrtmp < adrptr) {
		leftvalue = *adrtmp++;
		leftflags = *adrtmp++;
		xsymbol = (struct symbol *)*adrtmp++;
		outw(leftvalue, leftflags);
	}
}

/* opl15:					/ single operand */
/*	clr	-(sp) */
/*	br	op2b */

void opl15() {
	rvalue = 0;
	op2b();
}

/* opl12:					/ movf */
/*	mov	$400,rlimit */
/*	jsr	pc,addres */
/*	cmp	r2,$4			/ see if source is fregister */
/*	blo	1f */
/*	inc	swapf */
/*	br	op2a */
/* 1: */
/*	mov	$174000,(sp) */
/*	br	op2a */

void opl12() {
	rlimit = 0400;
	addres();
	if (leftvalue >= 4)
		swapf = 1;
	else
		opcode = 0174000;
	op2a();
}

/* opl35:					/ jbr */
/* opl36:					/ jeq, jne, etc */
/*	jsr	pc,expres */
/*	tst	passno */
/*	bne	1f */
/*	mov	r2,r0 */
/*	jsr	pc,setbr */
/*	tst	r2 */
/*	beq	2f */
/*	cmp	(sp),$br */
/*	beq	2f */
/*	add	$2,r2 */
/* 2: */
/*	add	r2,dot			/ if doesn't fit */
/*	add	$2,dot */
/*	tst	(sp)+ */
/*	rts	pc */
/* 1: */
/*	jsr	pc,getbr */
/*	bcc	dobranch */
/*	mov	(sp)+,r0 */
/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */
/*	cmp	r0,$br */
/*	beq	2f */
/*	mov	$402,r2 */
/*	xor	r0,r2			/ flip cond, add ".+6" */
/*	mov	$1,r3 */
/*	jsr	pc,outw */
/* 2: */
/*	mov	$1,r3 */
/*	mov	$jmp+37,r2 */
/*	jsr	pc,outw */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */
/*	jsr	pc,outw */
/*	rts	pc */

void opl35() {
	expres();
	if (passno == 0)
		pdot->value += setbr(leftvalue) ? 4 : 2;
	else {
		if (getbr() == 0)
			dobranch();
		else {
			outw(0000137, FABS); /* jmp */
			outw(leftvalue, leftflags);
		}
	}
}

void opl36() {
	expres();
	if (passno == 0)
		pdot->value += setbr(leftvalue) ? 6 : 2;
	else {
		if (getbr() == 0)
			dobranch();
		else {
			outw(opcode ^ 0402, FABS); /* jne, jeq, etc .+6 */
			outw(0000137, FABS); /* jmp */
			outw(leftvalue, leftflags);
		}
	}
}

/* opl31:					/ sob */
/*	jsr	pc,expres */
/*	jsr	pc,checkreg */
/*	swab	r2 */
/*	asr	r2 */
/*	asr	r2 */
/*	bis	r2,(sp) */
/*	jsr	pc,readop */
/*	jsr	pc,expres */
/*	tst	passno */
/*	beq	3f */
/*	sub	dot,r2 */
/*	neg	r2 */
/*	mov	r2,r0 */
/*	cmp	r0,$-2 */
/*	blt	2f */
/*	cmp	r0,$175 */
/*	bgt	2f */
/*	add	$4,r2 */
/*	br	1f */

void opl31() {
	expres();
	checkreg();
	opcode |= leftvalue << 6;
	readop();
	expres();
	if (passno == 0)
		binstr();
	else {
		leftvalue = pdot->value - leftvalue;
		if (leftvalue < -2 || leftvalue > 0175)
			errorb();
		else {
			leftvalue += 4;
			branch();
		}
	}
}

/* opl6:					/branch */
/*	jsr	pc,expres */
/*	tst	passno */
/*	beq	3f */

void opl6() {
	expres();
	if (passno == 0)
		binstr();
	else
		dobranch();
}

/* dobranch: */
/*	sub	dot,r2 */
/*	mov	r2,r0 */
/*	cmp	r0,$-254. */
/*	blt	2f */
/*	cmp	r0,$256. */
/*	bgt	2f */

void dobranch() {
	leftvalue -= pdot->value;
	if (leftvalue < -254 || leftvalue > 256)
		errorb();
	else
		branch();
}

/* 1: */
/*	bit	$1,r2 */
/*	bne	2f */
/*	cmp	r3,dot-2		/ same relocation as . */
/*	bne	2f */
/*	asr	r2 */
/*	dec	r2 */
/*	bic	$177400,r2 */

void branch() {
	if (leftvalue & 1)
		errorb();
	else {
		leftvalue = ((leftvalue >> 1) - 1) & 0377;
		binstr();
	}
}

/* 3: */
/*	bis	(sp)+,r2 */
/*	clr	r3 */
/*	jsr	pc,outw */
/*	rts	pc */

void binstr() {
	outw(leftvalue | opcode, 0);
}

/* 2: */
/*	mov	$'b,-(sp) */
/*	jsr	pc,error */
/*	clr	r2 */
/*	br	3b */

void errorb() {
	error('b');
	leftvalue = 0;
	branch();
}

/* opl7:					/jsr */
/*	jsr	pc,expres */
/*	jsr	pc,checkreg */
/*	jmp	op2a */

void opl7() {
	expres();
	checkreg();
	op2a();
}

/* opl10:					/ rts */
/*	jsr	pc,expres */
/*	jsr	pc,checkreg */
/*	br	1f */

void opl10() {
	expres();
	checkreg();
	rinstr();
}

/* opl11:					/ sys */
/*	jsr	pc,expres */
/*	cmp	r2,$256. */
/*	bhis	0f */
/*	cmp	r3,$1 */
/*	ble	1f */
/* 0: */
/*	mov	$'a,-(sp) */
/*	jsr	pc,error */

void opl11() {
	expres();
	if (leftvalue < 0 || leftvalue >= 256)
		error('a');
	rinstr();
}

/* 1: */
/*	bis	(sp)+,r2 */
/*	jsr	pc,outw */
/*	rts	pc */

void rinstr() {
	outw(leftvalue | opcode, leftflags);
}

/* opl16:					/ .byte */
/*	jsr	pc,expres */
/*	jsr	pc,outb */
/*	cmp	r4,$', */
/*	bne	1f */
/*	jsr	pc,readop */
/*	br	opl16 */
/* 1: */
/*	tst	(sp)+ */
/*	rts	pc */

void opl16() {
	while (1) {
		expres();
		outb(leftvalue, leftflags);
		if (token != TCOMMA)
			break;
		readop();
	}
}

#if 1 /* modifications for dec syntax */
void opldotword() {
	while (1) {
		expres();
		outw(leftvalue, leftflags);
		if (token != TCOMMA)
			break;
		readop();
	}
}
#endif

/* opl17:					/ < (.ascii) */
/*	jsr	pc,getw */
/*	mov	$1,r3 */
/*	mov	r4,r2 */
/*	bmi	2f */
/*	bic	$!377,r2 */
/*	jsr	pc,outb */
/*	br	opl17 */
/* 2: */
/*	jsr	pc,getw */
/*	rts	pc */

void opl17() {
	while (1) {
		p1getw();
		if (token == -1)
			break;
		outb(token & 0377, FABS);
	}
	p1getw();
}

/* opl20:					/.even */
/*	bit	$1,dot */
/*	beq	1f */
/*	cmp	dot-2,$4 */
/*	beq	2f			/ bss mode */
/*	clr	r2 */
/*	clr	r3 */
/*	jsr	pc,outb */
/*	br	1f */
/* 2: */
/*	inc	dot */
/* 1: */
/*	tst	(sp)+ */
/*	rts	pc */

void opl20() {
	if (pdot->value & 1) {
		if (pdot->flags != FBSS)
			outb(0, 0);
		else
			pdot->value++;
	}
}

/* opl21:					/if */
/*	jsr	pc,expres */

void opl21() {
	expres();
}

/* opl22: */
/* oplret: */
/*	tst	(sp)+ */
/*	rts	pc */

void opl22() {
}

void oplret() {
}

/* opl23:					/.globl */
/*	cmp	r4,$200 */
/*	blo	1f */
/*	bisb	$40,(r4) */
/*	jsr	pc,readop */
/*	cmp	r4,$', */
/*	bne	1f */
/*	jsr	pc,readop */
/*	br	opl23 */
/* 1: */
/*	tst	(sp)+ */
/*	rts	pc */

void opl23() {
	struct symbol *psymbol;

	while (1) {
		if (token >= 0 && token < TASCII)
			break;
		psymbol = (struct symbol *)token;
		psymbol->flags |= FGLOBAL;
		readop();
		if (token != TCOMMA)
			break;
		readop();
	}
}

/* opl25:					/ .text, .data, .bss */
/* opl26: */
/* opl27: */
/*	inc	dot */
/*	bic	$1,dot */
/*	mov	r0,-(sp) */
/*	mov	dot-2,r1 */
/*	asl	r1 */
/*	mov	dot,savdot-4(r1) */
/*	tst	passno */
/*	beq	1f */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,flush */
/*	mov	$relp,-(sp) */
/*	jsr	pc,flush */
/*	mov	(sp),r2 */
/*	asl	r2 */
/*	add	$txtseek-[4*25],r2 */
/*	mov	r2,tseekp */
/*	mov	(r2),r0 */
/*	mov	2(r2),r1 */
/*	mov	$txtp,-(sp) */
/*	jsr	pc,oset */
/*	add	$trelseek-txtseek,r2 */
/*	mov	(r2),r0 */
/*	mov	2(r2),r1 */
/*	mov	r2,rseekp */
/*	mov	$relp,-(sp) */
/*	jsr	pc,oset */
/* 1: */
/*	mov	(sp)+,r0 */
/*	mov	savdot-[2*25](r0),dot */
/*	asr	r0 */
/*	sub	$25-2,r0 */
/*	mov	r0,dot-2		/ new . relocation */
/*	tst	(sp)+ */
/*	rts	pc */

void opl25() {
	pdot->value = (pdot->value + 1) & ~1;
	savdot[pdot->flags - FTEXT] = pdot->value;
	if (passno) {
		flush(&txtp);
		flush(&relp);
		tseekp = seek;
		oset(&txtp, seek[0]);
		rseekp = seek + 3;
		oset(&relp, seek[3]);
	}
	pdot->value = savdot[0];
	pdot->flags = FTEXT;
}

void opl26() {
	pdot->value = (pdot->value + 1) & ~1;
	savdot[pdot->flags - FTEXT] = pdot->value;
	if (passno) {
		flush(&txtp);
		flush(&relp);
		tseekp = seek + 1;
		oset(&txtp, seek[1]);
		rseekp = seek + 4;
		oset(&relp, seek[4]);
	}
	pdot->value = savdot[1];
	pdot->flags = FDATA;
}

void opl27() {
	pdot->value = (pdot->value + 1) & ~1;
	savdot[pdot->flags - FTEXT] = pdot->value;
	if (passno) { /* this is not really needed: */
		flush(&txtp);
		flush(&relp);
		tseekp = seek + 2;
		oset(&txtp, seek[2]);
		rseekp = seek + 5;
		oset(&relp, seek[5]);
	}
	pdot->value = savdot[2];
	pdot->flags = FBSS;
}

/* opl32: */
/*	cmp	r4,$200 */
/*	blo	1f */
/*	mov	r4,-(sp) */
/*	jsr	pc,readop */
/*	jsr	pc,readop */
/*	jsr	pc,expres */
/*	mov	(sp)+,r0 */
/*	bit	$37,(r0) */
/*	bne	1f */
/*	bis	$40,(r0) */
/*	mov	r2,2(r0) */
/* 1: */
/*	tst	(sp)+ */
/*	rts	pc */

void opl32() {
	struct symbol *psymbol;

	if (token >= 0 && token < TASCII)
		return;
	psymbol = (struct symbol *)token;
	readop();
	readop();
	expres();
	if ((psymbol->flags & 037) == 0) {
		psymbol->flags |= FGLOBAL;
		psymbol->value = leftvalue;
	}
}

/* addres: */
/*	clr	-(sp) */

void addres() {
/* printf("addres() token = 0x%08x\n", token); */
	indirect = 0; /* it has a different meaning on pass 1/2 */
	addres1();
}

/* 4: */
/*	cmp	r4,$'( */
/*	beq	alp */
/*	cmp	r4,$'- */
/*	beq	amin */
/*	cmp	r4,$'$ */
/*	beq	adoll */
/*	cmp	r4,$'* */
/*	bne	getx */
/*	jmp	astar */

void addres1() {
	switch (token) {
	case TLPAREN:
		alp();
		return;
	case TMIN:
		amin();
		return;
	case TDOLL:
		adoll();
		return;
	case TSTAR:
		astar();
		return;
	}
	getx();
}

/* getx: */
/*	jsr	pc,expres */
/*	cmp	r4,$'( */
/*	bne	2f */
/*	jsr	pc,readop */
/*	mov	r2,(r5)+ */
/*	mov	r3,(r5)+ */
/*	mov	xsymbol,(r5)+ */
/*	jsr	pc,expres */
/*	jsr	pc,checkreg */
/*	jsr	pc,checkrp */
/*	bis	$60,r2 */
/*	bis	(sp)+,r2 */
/*	rts	pc */
/* 2: */

void getx() {
	expres();
	if (token == TLPAREN) {
		readop();
		*adrptr++ = leftvalue;
		*adrptr++ = leftflags;
		*adrptr++ = (intptr_t)xsymbol;
		expres();
		checkreg();
		checkrp();
		leftvalue |= 060 | indirect;
	}

/*	cmp	r3,$24 */
/*	bne	1f */
/*	jsr	pc,checkreg */
/*	bis	(sp)+,r2 */
/*	rts	pc */
/* 1: */

	else if (leftflags == FREGISTER) {
		checkreg();
		leftvalue |= indirect;
	}

/*	mov	r3,-(sp) */
/*	bic	$40,r3 */
/*	mov	(sp)+,r3 */
/*	bis	$100000,r3 */
/*	sub	dot,r2 */
/*	sub	$4,r2 */
/*	cmp	r5,$adrbuf */
/*	beq	1f */
/*	sub	$2,r2 */
/* 1: */

	else {
		leftflags |= 0100000; /* relocate flag */
		leftvalue -= (pdot->value + 4);
		if (adrptr != adrbuf)
			leftvalue -= 2;

/*	mov	r2,(r5)+		/ index */
/*	mov	r3,(r5)+		/ index reloc. */
/*	mov	xsymbol,(r5)+		/ index global */
/*	mov	$67,r2			/ address mode */
/*	bis	(sp)+,r2 */
/*	rts	pc */

		*adrptr++ = leftvalue;
		*adrptr++ = leftflags;
		*adrptr++ = (intptr_t)xsymbol;
		leftvalue = 067 | indirect;
	}
}

/* alp: */
/*	jsr	pc,readop */
/*	jsr	pc,expres */
/*	jsr	pc,checkrp */
/*	jsr	pc,checkreg */
/*	cmp	r4,$'+ */
/*	beq	1f */

void alp() {
	readop();
	expres();
	checkrp();
	checkreg();
	if (token != TPLUS) {

/*	tst	(sp)+ */
/*	beq	2f */
/*	bis	$70,r2 */
/*	clr	(r5)+ */
/*	clr	(r5)+ */
/*	mov	xsymbol,(r5)+ */
/*	rts	pc */
/* 2: */

		if (indirect) {
			leftvalue |= 070;
			*adrptr++ = 0;
			*adrptr++ = 0;
			*adrptr++ = (intptr_t)xsymbol;
		}

/*	bis	$10,r2 */
/*	rts	pc */

		else
			leftvalue |= 010;

/* 1: */
/*	jsr	pc,readop */
/*	bis	$20,r2 */
/*	bis	(sp)+,r2 */
/*	rts	pc */

	}
	else {
		readop();
		leftvalue |= 020 | indirect;
	}
}

/* amin: */
/*	jsr	pc,readop */
/*	cmp	r4,$'( */
/*	beq	1f */
/*	mov	r4,savop */
/*	mov	$'-,r4 */
/*	br	getx */
/* 1: */
/*	jsr	pc,readop */
/*	jsr	pc,expres */
/*	jsr	pc,checkrp */
/*	jsr	pc,checkreg */
/*	bis	(sp)+,r2 */
/*	bis	$40,r2 */
/*	rts	pc */


void amin() {
	readop();
	if (token != TLPAREN) {
		savop = token;
		token = TMIN;
		getx();
	}
	else {
		readop();
		expres();
		checkrp();
		checkreg();
		leftvalue |= indirect | 040;
	}
}

/* adoll: */
/*	jsr	pc,readop */
/*	jsr	pc,expres */
/*	mov	r2,(r5)+ */
/*	mov	r3,(r5)+ */
/*	mov	xsymbol,(r5)+ */
/*	mov	(sp)+,r2 */
/*	bis	$27,r2 */
/*	rts	pc */

void adoll() {
	readop();
	expres();
	*adrptr++ = leftvalue;
	*adrptr++ = leftflags;
	*adrptr++ = (intptr_t)xsymbol;
	leftvalue = indirect | 027; /* means (pc)+ */
}

/* astar: */
/*	tst	(sp) */
/*	beq	1f */
/*	mov	$'*,-(sp) */
/*	jsr	pc,error */
/* 1: */
/*	mov	$10,(sp) */
/*	jsr	pc,readop */
/*	jmp	4b */

void astar() {
	if (indirect)
		error('*');
	indirect = 010;
	readop();
	addres1();
}

/* checkreg: */
/*	cmp	r2,$7 */
/*	bhi	1f */
/*	cmp	r1,$1 */
/*	blos	2f */
/*	cmp	r3,$5 */
/*	blo	1f */
/* 2: */
/*	rts	pc */
/* 1: */
/*	mov	$'a,-(sp) */
/*	jsr	pc,error */
/*	clr	r2 */
/*	clr	r3 */
/*	rts	pc */

void checkreg() {
	if (leftvalue > 7 || (leftflags >= FTEXT && leftflags <= FBSS)) {
		error('a');
		leftvalue = 0;
		leftflags = 0;
	}
}

/* checkrp: */
/*	cmp	r4,$') */
/*	beq	1f */
/*	mov	$'),-(sp) */
/*	jsr	pc,error */
/*	rts	pc */
/* 1: */
/*	jsr	pc,readop */
/*	rts	pc */

void checkrp() {
	if (token != TRPAREN)
		error(')');
	else
		readop();
}

/* setbr: */
/*	mov	brtabp,r1 */
/*	cmp	r1,$brlen */
/*	blt	1f */
/*	mov	$2,r2 */
/*	rts	pc */
/* 1: */

int setbr(value) int value; {
	int index;

	if (brtabp >= brlen)
		return 2;

/*	inc	brtabp */
/*	clr	-(sp) */
/*	sub	dot,r0 */
/*	ble	1f */
/*	sub	brdelt,r0 */
/* 1: */

	index = brtabp++;
	value -= pdot->value;
	if (value > 0)
		value -= brdelt;

/*	cmp	r0,$-254. */
/*	blt	1f */
/*	cmp	r0,$256. */
/*	ble	2f */
/* 1: */

	if (value < -254 || value > 256) {

/*	mov	r1,-(sp) */
/*	bic	$!7,(sp) */
/*	mov	$1,r0 */
/*	ash	(sp)+,r0 */
/*	ash	$-3,r1 */
/*	bisb	r0,brtab(r1) */
/*	mov	$2,(sp) */

		brtab[index >> 3] |= 1 << (index & 7);
		return 2;

/* 2: */
/*	mov	(sp)+,r2 */
/*	rts	pc */

	}
	return 0;
}

/* getbr: */
/*	mov	brtabp,r1 */
/*	cmp	r1,$brlen */
/*	blt	1f */
/*	sec */
/*	rts	pc */
/* 1: */

int getbr() {
	int index;

	if (brtabp >= brlen)
		return 1;

/*	mov	r1,-(sp) */
/*	bic	$!7,(sp) */
/*	neg	(sp) */
/*	inc	brtabp */
/*	ash	$-3,r1 */
/*	movb	brtab(r1),r1 */
/*	ash	(sp)+,r1 */
/*	ror	r1			/ 0-bit into c-bit */
/*	rts	pc */

	index = brtabp++;
	return brtab[index >> 3] & (1 << (index & 7));
}

/* expres: */
/*	clr	xsymbol */

void expres() {
	xsymbol = NULL;
	expres1();
}

/* expres1: */
/*	mov	r5,-(sp) */
/*	mov	$'+,-(sp) */
/*	clr	r2 */
/*	mov	$1,r3 */
/*	br	1f */

void expres1() {
	struct symbol *psymbol;

/* printf("expres()\n"); */
	optoken = TPLUS;
	opfound = 0;
	leftvalue = 0;
	leftflags = FABS;
	goto entry;

/* advanc: */
/*	jsr	pc,readop */
/* 1: */
/*	mov	r4,r0 */
/*	blt	6f */
/*	cmp	r0,$177 */
/*	ble	7f */
/* 6: */
/*	movb	(r4),r0 */
/*	bne	1f */
/*	tst	passno */
/*	beq	1f */
/*	mov	$'u,-(sp) */
/*	jsr	pc,error */
/* 1: */

	while (1) {
		readop();
	entry:
		rightflags = token;
		if (token < 0 || token >= TASCII) {
			psymbol = (struct symbol *)token;
			rightflags = psymbol->flags;
			if (rightflags == 0 && passno)
				error('u');

/*	tst	overlaid */
/*	beq	0f */

/* Bill Shannon's hack to the assembler to force globl text */
/* references to remain undefined so that the link editor may */
/* resolve them. This is necessary if a function is used as an */
/* arg in an overlay located piece of code, and the function is */
/* actually in another overlay. Elsewise, the assembler fix's up */
/* the reference before the link editor changes the globl refrence */
/* to the thunk. -wfj 5/80 */
/*	cmp	r0,$42			/ is it globl text ? */
/*	bne	0f			/ nope */
/*	mov	$40,r0			/ yes, treat it as undefined external */
/* 0: */

			if (overlaid && rightflags == (FTEXT | FGLOBAL))
				rightflags = FGLOBAL;

/*	cmp	r0,$40 */
/*	bne	1f */
/*	mov	r4,xsymbol */
/*	clr	r1 */
/*	br	oprand */
/* 1: */

			if (rightflags == FGLOBAL) {
				xsymbol = (struct symbol *)token;
				rightvalue = 0;
			}

/*	mov	2(r4),r1 */
/*	br	oprand */

			else
				rightvalue = psymbol->value;
			oprand();
			continue;
		}

/* 7: */
/*	cmp	r4,$141 */
/*	blo	1f */
/*	asl	r4 */
/*	mov	curfb-[2*141](r4),r0 */
/*	mov	2(r0),r1 */
/*	movb	(r0),r0 */
/*	br	oprand */
/* 1: */

		if (token >= TLABEL) {
			if (token < TLABEL + 10)
				psymbol = *(struct symbol **)
						curfb[token - TLABEL];
			else
				psymbol = *(struct symbol **)
						nxtfb[token - (TLABEL + 10)];
			rightvalue = psymbol->value;
			rightflags = psymbol->flags;
			oprand();
			continue;
		}

/*	mov	$esw1,r1 */
/* 1: */
/*	cmp	(r1)+,r4 */
/*	beq	1f */
/*	tst	(r1)+ */
/*	bne	1b */
/*	tst	(sp)+ */
/*	mov	(sp)+,r5 */
/*	rts	pc */
/* 1: */
/*	jmp	*(r1) */

		switch (token) {
		default:
			return;

/*	.data */
/* esw1: */
/*	'+;	binop */
/*	'-;	binop */
/*	'*;	binop */
/*	'/;	binop */
/*	'&;	binop */
/*	037;	binop */
/*	035;	binop */
/*	036;	binop */
/*	'%;	binop */

		case TPLUS:
		case TMIN:
		case TSTAR:
		case TSLASH:
		case TAND:
		case TOR:
		case TLSH:
		case TRSH:
		case TMOD:
			binop();
			break;

/*	'[;	brack */

		case TLBRACK:
			brack();
			break;

/*	'^;	binop */

		case TCARET:
			binop();
			break;

/*	1;	exnum */

		case TABS:
			exnum();
			break;

/*	2;	exnum1 */

		case TABS2:
			exnum1();
			break;

/*	'!;	binop */

		case TNOT:
			binop();
			break;

/*	200;	0 */
/*	.text */

		}
	}
}

/* binop: */
/*	cmpb	(sp),$'+ */
/*	beq	1f */
/*	mov	$'e,-(sp) */
/*	jsr	pc,error */
/* 1: */
/*	movb	r4,(sp) */
/*	br	advanc */


void binop() {
	if (optoken != TPLUS)
		error('e');
	optoken = token;
}

/* exnum1: */
/*	mov	numval,r1 */
/*	br	1f */

/* exnum: */
/*	jsr	pc,getw */
/*	mov	r4,r1 */
/* 1: */
/*	mov	$1,r0 */
/*	br	oprand */

void exnum1() {
/* printf("exnum1() numval = 0%o\n", numval); */
	rightvalue = numval;
	rightflags = FABS;
	oprand();
}

void exnum() {
	p1getw();
/* printf("exnum() token = 0%o\n", rightvalue); */
	rightvalue = token;
	rightflags = FABS;
	oprand();
}

/* brack: */
/*	mov	r2,-(sp) */
/*	mov	r3,-(sp) */
/*	jsr	pc,readop */
/*	jsr	pc,expres1 */
/*	cmp	r4,$'] */
/*	beq	1f */
/*	mov	$'],-(sp) */
/*	jsr	pc,error */
/* 1: */
/*	mov	r3,r0 */
/*	mov	r2,r1 */
/*	mov	(sp)+,r3 */
/*	mov	(sp)+,r2 */

void brack() {
	int tempvalue;
	int tempflags;
	int temptoken;

	tempvalue = leftvalue;
	tempflags = leftflags;
	temptoken = optoken;
	readop();
	expres1();
	if (token != TRBRACK)
		error(']');
	rightflags = leftflags;
	rightvalue = leftvalue;
	leftflags = tempflags;
	leftvalue = tempvalue;
	optoken = temptoken;
	oprand();
}

/* oprand: */
/*	mov	$exsw2,r5 */
/* 1: */
/*	cmp	(sp),(r5)+ */
/*	beq	1f */
/*	tst	(r5)+ */
/*	bne	1b */
/*	br	eoprnd */
/* 1: */
/*	jmp	*(r5) */


void oprand() {
	opfound = 1;
	switch (optoken) {
	default:
		eoprnd();
		return;

/*	.data */
/* exsw2: */
/*	'+; exadd */

	case TPLUS:
		exadd();
		return;

/*	'-; exsub */

	case TMIN:
		exsub();
		return;

/*	'*; exmul */

	case TSTAR:
		exmul();
		return;

/*	'/; exdiv */

	case TSLASH:
		exdiv();
		return;

/*	037; exor */

	case TOR:
		exor();
		return;

/*	'&; exand */

	case TAND:
		exand();
		return;

/*	035;exlsh */

	case TLSH:
		exlsh();
		return;

/*	036;exrsh */

	case TRSH:
		exrsh();
		return;

/*	'%; exmod */

	case TMOD:
		exmod();
		return;

/*	'^; excmbin */

	case TCARET:
		excmbin();
		return;

/*	'!; exnot */

	case TNOT:
		exnot();
		return;

/*	200;  0 */
/*	.text */

	}
}

/* excmbin: */
/*	mov	r0,r3 */
/*	br	eoprnd */

void excmbin() {
	leftflags = rightflags;
	eoprnd();
}

/* exrsh: */
/*	neg	r1 */
/*	beq	exlsh */
/*	inc	r1 */
/*	clc */
/*	ror	r2 */
/* exlsh: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	ash	r1,r2 */
/*	br	eoprnd */


void exrsh() {
	combin(relte2);
	leftvalue = ((unsigned int)leftvalue & 0177777) >> rightvalue;
	eoprnd();
}

void exlsh() {
	combin(relte2);
	leftvalue <<= rightvalue;
	eoprnd();
}

/* exmod: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	mov	r3,r0 */
/*	mov	r2,r3 */
/*	clr	r2 */
/*	div	r1,r2 */
/*	mov	r3,r2 */
/*	mov	r0,r3 */
/*	br	eoprnd */

void exmod() {
	int temp;

	combin(relte2);
	temp = rightvalue & 0177777;
	if (temp)
		leftvalue = (leftvalue & 0177777) % temp;
	else {
		error('0'); /* a nick innovation */
		leftvalue = 0;
	}
	eoprnd();
}

/* exadd: */
/*	mov	$reltp2,r5 */
/*	jsr	pc,combin */
/*	add	r1,r2 */
/*	br	eoprnd */


void exadd() {
	combin(reltp2);
	leftvalue += rightvalue;
	eoprnd();
}

/* exsub: */
/*	mov	$reltm2,r5 */
/*	jsr	pc,combin */
/*	sub	r1,r2 */
/*	br	eoprnd */

void exsub() {
	combin(reltm2);
	leftvalue -= rightvalue;
	eoprnd();
}

/* exand: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	com	r1 */
/*	bic	r1,r2 */
/*	br	eoprnd */

void exand() {
	combin(relte2);
	leftvalue &= rightvalue;
	eoprnd();
}

/* exor: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	bis	r1,r2 */
/*	br	eoprnd */

void exor() {
	combin(relte2);
	leftvalue |= rightvalue;
	eoprnd();
}

/* exmul: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	mul	r2,r1 */
/*	mov	r1,r2 */
/*	br	eoprnd */

void exmul() {
	combin(relte2);
	leftvalue *= rightvalue;
	eoprnd();
}

/* exdiv: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	mov	r3,r0 */
/*	mov	r2,r3 */
/*	clr	r2 */
/*	div	r1,r2 */
/*	mov	r0,r3 */
/*	br	eoprnd */

void exdiv() {
	int temp;

	combin(relte2);
	temp = rightvalue & 0177777;
	if (temp)
		leftvalue = (leftvalue & 0177777) / temp;
	else {
		error('0'); /* a nick innovation */
		leftvalue = 0;
	}
	eoprnd();
}

/* exnot: */
/*	mov	$relte2,r5 */
/*	jsr	pc,combin */
/*	com	r1 */
/*	add	r1,r2 */
/*	br	eoprnd */

void exnot() {
	combin(relte2);
	leftvalue += ~rightvalue;
	eoprnd();
}

/* eoprnd: */
/*	mov	$'+,(sp) */
/*	jmp	advanc */

void eoprnd() {
	optoken = TPLUS;
}

/* combin: */
/*	tst	passno */
/*	bne	combin1 */

void combin(table) char *table; {
	int global;
	int flags;

	if (passno == 0) {

/*	mov	r0,-(sp) */
/*	bis	r3,(sp) */
/*	bic	$!40,(sp) */
/*	bic	$!37,r0 */
/*	bic	$!37,r3 */
/*	cmp	r0,r3 */
/*	ble	1f */
/*	mov	r0,-(sp) */
/*	mov	r3,r0 */
/*	mov	(sp)+,r3 */
/* 1: */

		global = (rightflags | leftflags) & FGLOBAL;
		rightflags &= 037;
		leftflags &= 037;
		if (rightflags > leftflags) {
			flags = rightflags;
			rightflags = leftflags;
			leftflags = flags;
		}

/*	tst	r0 */
/*	beq	1f */
/*	cmp	r5,$reltm2 */
/*	bne	2f */
/*	cmp	r0,r3 */
/*	bne	2f */
/*	mov	$1,r3 */
/*	br	2f */
/* 1: */
/*	clr	r3 */
/* 2: */
/*	bis	(sp)+,r3 */
/*	rts	pc */

		if (rightflags) {
			if (table == reltm2 && rightflags == leftflags)
				leftflags = TABS;
		}
		else {
			leftflags = 0;
		}
		leftflags |= global;
	}

/* combin1: */
/*	mov	r1,-(sp) */
/*	clr	maxtyp */

	else {
		maxtyp = 0;

/*	jsr	pc,maprel */
/*	mov	r0,r1 */
/*	mul	$6,r1 */
/*	mov	r3,r0 */
/*	jsr	pc,maprel */
/*	add	r5,r0 */
/*	add	r1,r0 */
/*	movb	(r0),r3 */

		leftflags = table[maprel(rightflags) * 6 + maprel(leftflags)];

/*	bpl	1f */
/*	cmp	r3,$-1 */
/*	beq	2f */
/*	mov	$'r,-(sp) */
/*	jsr	pc,error */
/* 2: */
/*	mov	maxtyp,r3 */
/* 1: */

		if (leftflags < 0) {
			if (leftflags != M)
				error('r');
			leftflags = maxtyp;
		}

/*	mov	(sp)+,r1 */
/*	rts	pc */

	}
}

/* maprel: */
/*	cmp	r0,$40 */
/*	bne	1f */
/*	mov	$5,r0 */
/*	rts	pc */
/* 1: */
/*	bic	$!37,r0 */
/*	cmp	r0,maxtyp */
/*	blos	1f */
/*	mov	r0,maxtyp */
/* 1: */
/*	cmp	r0,$5 */
/*	blo	1f */
/*	mov	$1,r0 */
/* 1: */
/*	rts	pc */

int maprel(flags) int flags; {
	if (flags == FGLOBAL)
		return 5;
	flags &= 037;
	if (maxtyp < flags)
		maxtyp = flags;
	if (flags > FBSS)
		flags = FABS;
	return flags;
}

/*	.data */
/* X = -2 */
/* M = -1 */
/* reltp2: */
/*	.byte 0, 0, 0, 0, 0, 0 */
/*	.byte 0, M, 2, 3, 4,40 */
/*	.byte 0, 2, X, X, X, X */
/*	.byte 0, 3, X, X, X, X */
/*	.byte 0, 4, X, X, X, X */
/*	.byte 0,40, X, X, X, X */

char reltp2[36] = {
	0,  0, 0, 0, 0,  0,
	0,  M, 2, 3, 4,040,
	0,  2, X, X, X,  X,
	0,  3, X, X, X,  X,
	0,  4, X, X, X,  X,
	0,040, X, X, X,  X
};

/* reltm2: */
/*	.byte 0, 0, 0, 0, 0, 0 */
/*	.byte 0, M, 2, 3, 4,40 */
/*	.byte 0, X, 1, X, X, X */
/*	.byte 0, X, X, 1, X, X */
/*	.byte 0, X, X, X, 1, X */
/*	.byte 0, X, X, X, X, X */

char reltm2[36] = {
	0, 0, 0, 0, 0,  0,
	0, M, 2, 3, 4,040,
	0, X, 1, X, X,  X,
	0, X, X, 1, X,  X,
	0, X, X, X, 1,  X,
	0, X, X, X, X,  X
};

/* relte2: */
/*	.byte 0, 0, 0, 0, 0, 0 */
/*	.byte 0, M, X, X, X, X */
/*	.byte 0, X, X, X, X, X */
/*	.byte 0, X, X, X, X, X */
/*	.byte 0, X, X, X, X, X */
/*	.byte 0, X, X, X, X, X */

char relte2[36] = {
	0, 0, 0, 0, 0, 0,
	0, M, X, X, X, X,
	0, X, X, X, X, X,
	0, X, X, X, X, X,
	0, X, X, X, X, X,
	0, X, X, X, X, X
};

/* qnl:	<?\n> */
/* a.out:	<a.out\0> */
/*	.even */
/* a.outp:	a.out */

char *a_outp = "a.out";

/* obufp:	outbuf */

short *obufp = outbuf;

/* passno: -1 */

int passno = -1;

/* outmod:	0777 */

int outmod = 0777;

/* tseekp:	txtseek */

off_t *tseekp = seek;

/* rseekp:	trelseek */

off_t *rseekp = seek + 3;

/* txtmagic: */
/*	br	.+20 */
/* txtsiz:	.=.+2 */
/* datsiz:	.=.+2 */
/* bsssiz:	.=.+2 */
/* symsiz:	.=.+2 */
/*	.=.+6 */

int siz[4];

/* txtseek:0; 20 */
/* datseek:.=.+4 */
/*	.=.+4 */
/* trelseek:.=.+4 */
/* drelseek:.=.+4 */
/*	.=.+4 */
/* symseek:.=.+4 */

off_t seek[7];

/*	.bss */
/* brlen	= 1024. */
/* brtab:	.=.+[brlen\/8.] */

char brtab[brlen / 8];

/* brtabp:	.=.+2 */

int brtabp;

/* brdelt:	.=.+2 */

int brdelt;

/* fbbufp: .=.+2 */

struct symbol **fbbufp;

/* defund:	.=.+2 */

int defund;

/* datbase:.=.+2 */
/* bssbase:.=.+2 */

int base[3];

/* ibufc:	.=.+2 */

int ibufc;

/* overlaid: .=.+2 */

int overlaid;

/* adrbuf:	.=.+12. */

intptr_t adrbuf[6];

/* xsymbol:.=.+2 */

struct symbol *xsymbol;

/* errflg:	.=.+2 */

int errflg;

/* argb:	.=.+22. */

char argb[22];

#if 0
/* numval:	.=.+2 */

int numval;
#endif

/* maxtyp:	.=.+2 */

int maxtyp;

/* ibufp:	.=.+2 */

short *ibufp;
char *p0ibufp;

/* txtp:	.=.+8. */
/*	.=.+1024. */

struct stream txtp;

/* relp:	.=.+8. */
/* outbuf: */
/*	.=.+1024. */

struct stream relp;

/* swapf:	.=.+2 */

int swapf;

/* rlimit:	.=.+2 */

int rlimit;

/* endtable:.=.+2 */

struct symbol **endtable;

/* totalsz:			/ string table length */
/*	.=.+4 */
/*	.text */

off_t totalsz;

