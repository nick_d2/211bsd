#!/bin/sh
ROOT="`pwd |sed -e 's/\/bin\/as$//'`"
HOSTCC="cc -g -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib/ -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
INSTALL="$ROOT/scripts/install.sh --strip-program=/bin/true"
MANROFF="nroff -man"
mkdir --parents "$ROOT/cross/bin"
make CC="$HOSTCC" MANROFF="$MANROFF" SEPFLAG= && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
