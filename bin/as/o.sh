#!/bin/sh
ROOT="`pwd |sed -e 's/\/bin\/as$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
