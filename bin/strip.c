/*
 * Copyright (c) 1983 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if	defined(DOSCCS) && !defined(lint)
char copyright[] =
"@(#) Copyright (c) 1983 Regents of the University of California.\n\
 All rights reserved.\n";

static char sccsid[] = "@(#)strip.c	5.1.1 (2.11BSD GTE) 1/1/94";
#endif

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/file.h>
#include <unistd.h>

#ifdef CROSS
#include "cross/a.out.h"
#include "cross/sys/types.h"
#else
#include <a.out.h>
#include <sys/types.h>
#define cross_off_t off_t
#define cross_xexec xexec
#endif

struct	cross_xexec head;
int	status;

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

int main __P((int argc, char *argv[]));
void strip __P((char *name));

int main(argc, argv) int argc; char *argv[]; {
	register int i;

	signal(SIGHUP, SIG_IGN);
	signal(SIGINT, SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	for (i = 1; i < argc; i++) {
		strip(argv[i]);
		if (status > 1)
			break;
	}
	exit(status);
}

void strip(name) char *name; {
	register int f;
#ifdef CROSS
	int i;
#endif
	cross_off_t size;

	f = open(name, O_RDWR);
	if (f < 0) {
		fprintf(stderr, "strip: "); perror(name);
		status = 1;
		goto out;
	}
	if (read(f, (char *)&head, sizeof (head)) < 0)
		goto bad_read;
#ifdef CROSS
	head.e.a_magic = cross_read_int((char *)&head.e.a_magic);
	head.e.a_text = cross_read_uint((char *)&head.e.a_text);
	head.e.a_data = cross_read_uint((char *)&head.e.a_data);
	head.e.a_bss = cross_read_uint((char *)&head.e.a_bss);
	head.e.a_syms = cross_read_uint((char *)&head.e.a_syms);
	head.e.a_entry = cross_read_uint((char *)&head.e.a_entry);
	head.e.a_unused = cross_read_uint((char *)&head.e.a_unused);
	head.e.a_flag = cross_read_uint((char *)&head.e.a_flag);
	head.o.max_ovl = cross_read_int((char *)&head.o.max_ovl);
	for (i = 0; i < NOVL; ++i)
		head.o.ov_siz[i] = cross_read_uint((char *)(head.o.ov_siz + i));
#endif
	if (N_BADMAG(head.e)) {
	bad_read:
		printf("strip: %s not in a.out format\n", name);
		status = 1;
		goto out;
	}
	if ((head.e.a_syms == 0) && ((head.e.a_flag & 1) != 0))
		goto out;

	size = N_DATOFF(head) + head.e.a_data;
	head.e.a_syms = 0;
	head.e.a_flag |= 1;
	if (ftruncate(f, (off_t)size) < 0) {
		fprintf(stderr, "strip: "); perror(name);
		status = 1;
		goto out;
	}
	(void) lseek(f, (off_t)0, L_SET);
#ifdef CROSS
	cross_write_int((char *)&head.e.a_magic, head.e.a_magic);
	cross_write_uint((char *)&head.e.a_text, head.e.a_text);
	cross_write_uint((char *)&head.e.a_data, head.e.a_data);
	cross_write_uint((char *)&head.e.a_bss, head.e.a_bss);
	cross_write_uint((char *)&head.e.a_syms, head.e.a_syms);
	cross_write_uint((char *)&head.e.a_entry, head.e.a_entry);
	cross_write_uint((char *)&head.e.a_unused, head.e.a_unused);
	cross_write_uint((char *)&head.e.a_flag, head.e.a_flag);
#endif
	(void) write(f, (char *)&head.e, sizeof (head.e));
out:
	close(f);
}
