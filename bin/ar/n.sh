#!/bin/sh
ROOT="`pwd |sed -e 's/\/bin\/ar$//'`"
HOSTCC="cc -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib -DCROSS -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
HOSTLIBCROSS="-lcross"
INSTALL="$ROOT/scripts/install.sh --strip-program=/bin/true"
MANROFF="nroff -man"
mkdir --parents "$ROOT/cross/bin"
mkdir --parents "$ROOT/cross/usr/man/cat1"
mkdir --parents "$ROOT/cross/usr/man/cat5"
make CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF" SEPFLAG= && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
