/*
 *	@(#)ialloc.c	1.1 ialloc.c 3/4/87
 */

/*LINTLIBRARY*/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#ifndef alloc_t
#define alloc_t	unsigned
#endif /* !alloc_t */

#ifdef MAL
#define NULLMAL(x)	((x) == NULL || (x) == MAL)
#else /* !MAL */
#define NULLMAL(x)	((x) == NULL)
#endif /* !MAL */

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

char *imalloc __P((int n));
char *icalloc __P((int nelem, int elsize));
char *irealloc __P((char *pointer, int size));
char *icatalloc __P((char *old, char *new));
char *icpyalloc __P((char *string));
void ifree __P((char *p));

char *imalloc(n) int n; {
#ifdef MAL
	register char *	result;

	if (n == 0)
		n = 1;
	result = (char *)malloc((alloc_t) n);
	return (result == MAL) ? NULL : result;
#else /* !MAL */
	if (n == 0)
		n = 1;
	return (char *)malloc((alloc_t) n);
#endif /* !MAL */
}

char *icalloc(nelem, elsize) int nelem; int elsize; {
	if (nelem == 0 || elsize == 0)
		nelem = elsize = 1;
	return (char *)calloc((alloc_t) nelem, (alloc_t) elsize);
}

char *irealloc(pointer, size) char *pointer; int size; {
	if (NULLMAL(pointer))
		return imalloc(size);
	if (size == 0)
		size = 1;
	return (char *)realloc(pointer, (alloc_t) size);
}

char *icatalloc(old, new) char *old; char *new; {
	register char *	result;
	register int oldsize, newsize;

	oldsize = NULLMAL(old) ? 0 : strlen(old);
	newsize = NULLMAL(new) ? 0 : strlen(new);
	if ((result = irealloc(old, oldsize + newsize + 1)) != NULL)
		if (!NULLMAL(new))
			(void) strcpy(result + oldsize, new);
	return result;
}

char *icpyalloc(string) char *string; {
	return icatalloc((char *) NULL, string);
}

void ifree(p) char *p; {
	if (!NULLMAL(p))
		free(p);
}
