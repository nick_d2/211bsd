#!/bin/sh
ROOT="`pwd |sed -e 's/\/share\/zoneinfo$//'`"
CC="$ROOT/cross/bin/cc"
HOSTCC="cc -g -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib -DCROSS -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
HOSTLIBCROSS="-lcross"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
MANROFF="nroff -man"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" HOSTCC="$HOSTCC" HOSTLIBCROSS="-lcross" HOSTSEPFLAG= MANROFF="$MANROFF" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
