#!/bin/sh -e

ROOT="`pwd`"

HOSTCC="cc -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib -DCROSS -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
HOSTLIBCROSS="-lcross"
INSTALL="$ROOT/scripts/install.sh"
MANROFF="nroff -man"
MKDEP="$ROOT/scripts/mkdep.sh"

mkdir --parents "$ROOT/cross/bin"
mkdir --parents "$ROOT/cross/lib"
mkdir --parents "$ROOT/cross/usr/bin"
mkdir --parents "$ROOT/cross/usr/include"
mkdir --parents "$ROOT/cross/usr/lib"
mkdir --parents "$ROOT/cross/usr/man/cat1"
mkdir --parents "$ROOT/cross/usr/man/cat3"
mkdir --parents "$ROOT/cross/usr/man/cat5"
mkdir --parents "$ROOT/cross/usr/share/misc"
mkdir --parents "$ROOT/cross/usr/ucb"

echo
echo "making clean"
echo

make clean
make -C usr.lib/libcross clean

echo
echo "making depend"
echo

make -C bin MKDEP="$MKDEP" AAASUBDIR="ar as cc ld nm" AAASCRIPT= AAASTD= AAANSTD="size strip" AAASETUID= AAAOPERATOR= AAAKMEM= AAATTY= depend
#make -C lib/c2 MKDEP="$MKDEP" depend
#make -C lib/ccom MKDEP="$MKDEP" depend
#make -C lib/cpp MKDEP="$MKDEP" depend
make -C ucb MKDEP="$MKDEP" AAASUBDIR= AAACSHSCRIPT= AAASTD="mkstr unifdef xstr" AAANSTD="strcompact symcompact symdump symorder" AAASETUID= AAAKMEM= depend
# AAAMKDEP below suppresses problems due to empty file list
make -C usr.bin MKDEP="$MKDEP" AAAMKDEP="true" AAASUBDIR="lex ranlib yacc" AAASCRIPT="lorder mkdep" AAASRCS= AAASTD= AAANSTD= AAAKMEM= depend
#make -C usr.lib/libcross MKDEP="$MKDEP" depend
#make -C usr.lib/libvmf MKDEP="$MKDEP" depend

echo
echo "making libraries"
echo

make -C usr.lib/libcross CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF"
make -C usr.lib/libvmf CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF"

echo
echo "installing libraries"
echo

make -C usr.lib/libcross INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
make -C usr.lib/libvmf INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install

echo
echo "making"
echo

make -C bin CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF" CROSSPREFIX="\\\"$ROOT/cross\\\"" STAGEPREFIX="\\\"$ROOT/stage\\\"" SEPFLAG= LDFLAGS="-L$ROOT/cross/usr/lib" AAASUBDIR="ar as cc ld nm" AAASCRIPT= AAASTD= AAANSTD="size strip" AAASETUID= AAAOPERATOR= AAAKMEM= AAATTY=
make -C lib/c2 CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF" SEPFLAG=
make -C lib/ccom CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" HOSTCC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" HOSTLIBCROSS="$HOSTLIBCROSS" HOSTSEPFLAG= MANROFF="$MANROFF" LDC0FLAGS= LDC1FLAGS=
make -C lib/cpp CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF" STAGEPREFIX="\\\"$ROOT/stage\\\"" SEPFLAG=
make -C ucb CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF" SEPFLAG= CROSSPREFIX="\\\"$ROOT/cross\\\"" LOCALESTR="\\\"LC_ALL=C \\\"" AAASUBDIR= AAACSHSCRIPT= AAASTD="mkstr unifdef xstr" AAANSTD="strcompact symcompact symdump symorder" AAASETUID= AAAKMEM=
make -C usr.bin CC="$HOSTCC" LIBCROSS="$HOSTLIBCROSS" MANROFF="$MANROFF" SEPFLAG= CROSSCC="$ROOT/cross/bin/cc" CROSSNM="$ROOT/cross/bin/nm" CROSSPREFIX="\\\"$ROOT/cross\\\"" AAASUBDIR="lex ranlib yacc" AAASCRIPT="lorder mkdep" AAASRCS= AAASTD= AAANSTD= AAAKMEM=

echo
echo "installing"
echo

make -C bin INSTALL="$INSTALL" DESTDIR="$ROOT/cross" AAASUBDIR="ar as cc ld nm" AAASCRIPT= AAASTD= AAANSTD="size strip" AAASETUID= AAAOPERATOR= AAAKMEM= AAATTY= install
make -C lib/c2 INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
make -C lib/ccom INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
make -C lib/cpp INSTALL="$INSTALL" DESTDIR="$ROOT/cross" install
make -C ucb INSTALL="$INSTALL" DESTDIR="$ROOT/cross" AAASUBDIR= AAACSHSCRIPT= AAASTD="mkstr unifdef xstr" AAANSTD="strcompact symcompact symdump symorder" AAASETUID= AAAKMEM= install
# AAAINSTALL below suppresses problems due to empty file list
make -C usr.bin INSTALL="$INSTALL" DESTDIR="$ROOT/cross" AAAINSTALL="true" AAASUBDIR="lex ranlib yacc" AAASCRIPT="lorder mkdep" AAASRCS= AAASTD= AAANSTD= AAAKMEM= install
