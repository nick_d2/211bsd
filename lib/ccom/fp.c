#ifndef pdp11
#include <errno.h>
#include <stdlib.h>
#include "c1.h"

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 1
#endif

/* Floating point accumulators */

#if 0
/* PSW */

#define	PSW_V_C		0				/* condition codes */
#define PSW_V_V		1
#define PSW_V_Z		2
#define PSW_V_N 	3
#endif

/* FPS */

#define	FPS_V_C		0				/* condition codes */
#define FPS_V_V		1
#define FPS_V_Z		2
#define FPS_V_N 	3
#define FPS_V_T		5				/* truncate */
#define FPS_V_L		6				/* long */
#define FPS_V_D		7				/* double */
#define FPS_V_IC	8				/* ic err int */
#define FPS_V_IV	9				/* overflo err int */
#define FPS_V_IU	10				/* underflo err int */
#define FPS_V_IUV	11				/* undef var err int */
#define FPS_V_ID	14				/* int disable */
#define FPS_V_ER	15				/* error */

/* Floating point status register */

#define FPS_ER		(1u << FPS_V_ER)		/* error */
#define FPS_ID		(1u << FPS_V_ID)		/* interrupt disable */
#define FPS_IUV		(1u << FPS_V_IUV)		/* int on undef var */
#define FPS_IU		(1u << FPS_V_IU)		/* int on underflow */
#define FPS_IV		(1u << FPS_V_IV)		/* int on overflow */
#define FPS_IC		(1u << FPS_V_IC)		/* int on conv error */
#define FPS_D		(1u << FPS_V_D)			/* single/double */
#define FPS_L		(1u << FPS_V_L)			/* word/long */
#define FPS_T		(1u << FPS_V_T)			/* round/truncate */
#define FPS_N		(1u << FPS_V_N)
#define FPS_Z		(1u << FPS_V_Z)
#define FPS_V		(1u << FPS_V_V)
#define FPS_C		(1u << FPS_V_C)
#define FPS_CC		(FPS_N + FPS_Z + FPS_V + FPS_C)
#define FPS_RW		(FPS_ER + FPS_ID + FPS_IUV + FPS_IU + FPS_IV + \
			FPS_IC + FPS_D + FPS_L + FPS_T + FPS_CC)

/* Floating point exception codes */

#define FEC_OP		2				/* illegal op/mode */
#define FEC_DZRO	4				/* divide by zero */
#define FEC_ICVT	6				/* conversion error */
#define FEC_OVFLO	8				/* overflow */
#define FEC_UNFLO	10				/* underflow */
#define FEC_UNDFV	12				/* undef variable */

/* Floating point format, all assignments 32b relative */

#define FP_V_SIGN	(63 - 32)			/* high lw: sign */
#define FP_V_EXP	(55 - 32)			/* exponent */
#define FP_V_HB		FP_V_EXP			/* hidden bit */
#define FP_V_F0		(48 - 32)			/* fraction 0 */
#define FP_V_F1		(32 - 32)			/* fraction 1 */
#define FP_V_FROUND	(31 - 32)			/* f round point */
#define FP_V_F2		16				/* low lw: fraction 2 */
#define FP_V_F3		0				/* fraction 3 */
#define FP_V_DROUND	(-1)				/* d round point */
#define FP_M_EXP	0377
#define FP_SIGN		(1u << FP_V_SIGN)
#define FP_EXP		(FP_M_EXP << FP_V_EXP)
#define FP_HB		(1u << FP_V_HB)
#define FP_FRACH	((1u << FP_V_HB) - 1)
#define FP_FRACL	0xFFFFFFFF
#define FP_BIAS		0200				/* exponent bias */
#define FP_GUARD	3				/* guard bits */

#if 0
/* Data lengths */

#define _WORD	2
#define _LONG	4
#define _QUAD	8
#endif

/* Double precision operations on 64b quantities */

#define F_LOAD(qd,ac,ds) ds.h = ac.h; ds.l = (qd)? ac.l: 0
#define F_LOAD_P(qd,ac,ds) ds->h = ac.h; ds->l = (qd)? ac.l: 0
#define F_LOAD_FRAC(qd,ac,ds) ds.h = (ac.h & FP_FRACH) | FP_HB; \
	ds.l = (qd)? ac.l: 0
#define F_STORE(qd,sr,ac) ac.h = sr.h; if ((qd)) ac.l = sr.l
#define F_STORE_P(qd,sr,ac) ac.h = sr->h; if ((qd)) ac.l = sr->l
#define F_GET_FRAC_P(sr,ds) ds.l = sr->l; \
	ds.h = (sr->h & FP_FRACH) | FP_HB
#define F_ADD(s2,s1,ds) ds.l = (s1.l + s2.l) & 0xFFFFFFFF; \
	ds.h = (s1.h + s2.h + (ds.l < s2.l)) & 0xFFFFFFFF
#define F_SUB(s2,s1,ds) ds.h = (s1.h - s2.h - (s1.l < s2.l)) & 0xFFFFFFFF; \
	ds.l = (s1.l - s2.l) & 0xFFFFFFFF
#define F_LT(x,y) ((x.h < y.h) || ((x.h == y.h) && (x.l < y.l)))
#define F_LT_AP(x,y) (((x->h & ~FP_SIGN) < (y->h & ~FP_SIGN)) || \
	(((x->h & ~FP_SIGN) == (y->h & ~FP_SIGN)) && (x->l < y->l)))
#define F_LSH_V(sr,n,ds) \
	ds.h = (((n) >= 32)? (sr.l << ((n) - 32)): \
		(sr.h << (n)) | ((sr.l >> (32 - (n))) & and_mask[n])) \
		& 0xFFFFFFFF; \
	ds.l = ((n) >= 32)? 0: (sr.l << (n)) & 0xFFFFFFFF
#define F_RSH_V(sr,n,ds) \
	ds.l = (((n) >= 32)? (sr.h >> ((n) - 32)) & and_mask[64 - (n)]: \
		((sr.l >> (n)) & and_mask[32 - (n)]) | \
		(sr.h << (32 - (n)))) & 0xFFFFFFFF; \
	ds.h = ((n) >= 32)? 0: \
		((sr.h >> (n)) & and_mask[32 - (n)]) & 0xFFFFFFFF

/* For the constant shift macro, arguments must in the range [2,31] */

#define F_LSH_1(ds) ds.h = ((ds.h << 1) | ((ds.l >> 31) & 1)) & 0xFFFFFFFF; \
	ds.l = (ds.l << 1) & 0xFFFFFFFF
#define F_RSH_1(ds) ds.l = ((ds.l >> 1) & 0x7FFFFFFF) | ((ds.h & 1) << 31); \
	ds.h = ((ds.h >> 1) & 0x7FFFFFFF)
#define F_LSH_K(sr,n,ds) \
	ds.h = 	((sr.h << (n)) | ((sr.l >> (32 - (n))) & and_mask[n])) \
		& 0xFFFFFFFF; \
	ds.l = (sr.l << (n)) & 0xFFFFFFFF
#define F_RSH_K(sr,n,ds) \
	ds.l = 	(((sr.l >> (n)) & and_mask[32 - (n)]) | \
		(sr.h << (32 - (n)))) & 0xFFFFFFFF; \
	ds.h = 	((sr.h >> (n)) & and_mask[32 - (n)]) & 0xFFFFFFFF
#define F_LSH_GUARD(ds) F_LSH_K(ds,FP_GUARD,ds)
#define F_RSH_GUARD(ds) F_RSH_K(ds,FP_GUARD,ds)

#define GET_BIT(ir,n)	(((ir) >> (n)) & 1)
#define GET_SIGN(ir)	GET_BIT((ir), FP_V_SIGN)
#define GET_EXP(ir)	(((ir) >> FP_V_EXP) & FP_M_EXP)
#define GET_SIGN_L(ir)	GET_BIT((ir), 31)
#define GET_SIGN_W(ir)	GET_BIT((ir), 15)

int32_t FEC = 0;
int32_t FPS = FPS_D; /* default to double precision */

_DOUBLE zero_fac = { 0, 0 };
_DOUBLE one_fac = { 1, 0 };
_DOUBLE fround_fac = { (1u << (FP_V_FROUND + 32)), 0 };
_DOUBLE fround_guard_fac = { 0, (1u << (FP_V_FROUND + FP_GUARD)) };
_DOUBLE dround_guard_fac = { (1u << (FP_V_DROUND + FP_GUARD)), 0 };
_DOUBLE fmask_fac = { 0xFFFFFFFF, (1u << (FP_V_HB + FP_GUARD + 1)) - 1 };
static const uint32_t and_mask[33] = { 0,
	0x1, 0x3, 0x7, 0xF,
	0x1F, 0x3F, 0x7F, 0xFF,
	0x1FF, 0x3FF, 0x7FF, 0xFFF,
	0x1FFF, 0x3FFF, 0x7FFF, 0xFFFF,
	0x1FFFF, 0x3FFFF, 0x7FFFF, 0xFFFFF,
	0x1FFFFF, 0x3FFFFF, 0x7FFFFF, 0xFFFFFF,
	0x1FFFFFF, 0x3FFFFFF, 0x7FFFFFF, 0xFFFFFFF,
	0x1FFFFFFF, 0x3FFFFFFF, 0x7FFFFFFF, 0xFFFFFFFF };

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

static void tstfp11 __P((_DOUBLE *fsrc));
static void absfp11 __P((_DOUBLE *fsrc));
static void negfp11 __P((_DOUBLE *fsrc));
static void cmpfp11 __P((_DOUBLE *fac, _DOUBLE *fsrc));
static int32_t moviefp11 __P((int32_t val, _DOUBLE *fac));
static int32_t moveifp11 __P((_DOUBLE *val));
static int32_t moviffp11 __P((int32_t val, _DOUBLE *fac));
static int32_t movfifp11 __P((_DOUBLE *val, int32_t *dst));
static int32_t addfp11 __P((_DOUBLE *facp, _DOUBLE *fsrcp));
static int32_t mulfp11 __P((_DOUBLE *facp, _DOUBLE *fsrcp));
static void frac_mulfp11 __P((_DOUBLE *f1p, _DOUBLE *f2p));
static int32_t divfp11 __P((_DOUBLE *facp, _DOUBLE *fsrcp));
static int32_t roundfp11 __P((_DOUBLE *fptr));
static int32_t round_and_pack __P((_DOUBLE *facp, int32_t exp, _DOUBLE *fracp, int r));
static int32_t fpnotrap __P((int32_t code));

int fp_tst(val) _DOUBLE val; {
	tstfp11(&val);
	return (FPS & FPS_Z) != 0;
}

_DOUBLE fp_abs(val) _DOUBLE val; {
	absfp11(&val);
	return val;
}

_DOUBLE fp_neg(val) _DOUBLE val; {
	negfp11(&val);
	return val;
}

int fp_le(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	cmpfp11(&val0, &val1);
	return (FPS & FPS_N) == 0;
}

int fp_ge(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	cmpfp11(&val1, &val0);
	return (FPS & FPS_N) == 0;
}

int fp_gt(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	cmpfp11(&val0, &val1);
	return (FPS & FPS_N) != 0;
}

int fp_lt(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	cmpfp11(&val1, &val0);
	return (FPS & FPS_N) != 0;
}

_INT fp_double_to_int(val) _DOUBLE val; {
	int32_t res;

	if (movfifp11(&val, &res))
		abort();
	return (_INT)res;
}

_LONG fp_double_to_long(val) _DOUBLE val; {
	int32_t res;

	FPS |= FPS_L;
	if (movfifp11(&val, &res))
		abort();
	FPS &= ~FPS_L;
	return (_LONG)res;
}

_FLOAT fp_double_to_float(val) _DOUBLE val; {
	_FLOAT res;

	if (roundfp11(&val))
		abort();
	res.h = val.h;
	return res;
}

_DOUBLE fp_int_to_double(val) _INT val; {
	_DOUBLE res;

	if (moviffp11((int32_t)val << 16, &res))
		abort();
	return res;
}

_DOUBLE fp_long_to_double(val) _LONG val; {
	_DOUBLE res;

	FPS |= FPS_L;
	if (moviffp11((int32_t)val, &res))
		abort();
	FPS &= ~FPS_L;
	return res;
}

_DOUBLE fp_float_to_double(val) _FLOAT val; {
	_DOUBLE res;

	res.h = val.h;
	res.l = 0;
	return res;
}

_DOUBLE fp_add(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	if (addfp11(&val0, &val1))
		abort();
	return val0;
}

_DOUBLE fp_sub(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	negfp11(&val1);
	if (addfp11(&val0, &val1))
		abort();
	return val0;
}

_DOUBLE fp_mul(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	if (mulfp11(&val0, &val1))
		abort();
	return val0;
}

_DOUBLE fp_div(val0, val1) _DOUBLE val0; _DOUBLE val1; {
	if (divfp11(&val0, &val1))
		abort();
	return val0;
}

_DOUBLE fp_ldexp(val, exp) _DOUBLE val; _INT exp; {
	tstfp11(&val);
	if ((FPS & FPS_Z) == 0 && moviefp11(moveifp11(&val) + (int32_t)exp, &val)) {
		val.l = 0xFFFFFFFF;
		val.h = ((FPS << (31 - FPS_V_N)) | 0x7FFFFFFF) & 0xFFFFFFFF;
		errno = ERANGE;
	}
	return val;
}

/* cut-down routines by Nick, previously inline to instruction decode */
static void tstfp11(fsrc) _DOUBLE *fsrc; {
	FPS = FPS & ~FPS_CC;
	if (GET_SIGN (fsrc->h)) FPS = FPS | FPS_N;
	if (GET_EXP (fsrc->h) == 0) FPS = FPS | FPS_Z;
}

static void absfp11(fsrc) _DOUBLE *fsrc; {
	if (GET_EXP (fsrc->h) == 0) *fsrc = zero_fac;
	else fsrc->h = fsrc->h & ~FP_SIGN;
}

static void negfp11(fsrc) _DOUBLE *fsrc; {
	if (GET_EXP (fsrc->h) == 0) *fsrc = zero_fac;
	else fsrc->h = fsrc->h ^ FP_SIGN;
}

/* from PDP-11/70 processor manual:
 *   FC <- 0.
 *   FV <- 0.
 *   FZ <- 1 If (FSRC) - (AC) = 0, else FZ <- 0.
 *   FN <- 1 If (FSRC) - (AC) < 0, else FN <- 0.
 * (backwards compared to what I expected)
 */
static void cmpfp11(fac, fsrc) _DOUBLE *fac; _DOUBLE *fsrc; {
	if (GET_EXP (fsrc->h) == 0) *fsrc = zero_fac;
	if (GET_EXP (fac->h) == 0) *fac = zero_fac;
	if ((fsrc->h == fac->h) && (fsrc->l == fac->l)) {	/* equal? */
	    FPS = (FPS & ~FPS_CC) | FPS_Z;
	    return;  }
	FPS = (FPS & ~FPS_CC) | ((fsrc->h >> (FP_V_SIGN - FPS_V_N)) & FPS_N);
	if ((GET_SIGN (fsrc->h ^ fac->h) == 0) && (fac->h != 0) &&
	    F_LT ((*fsrc), (*fac))) FPS = FPS ^ FPS_N;
}

static int32_t moviefp11(val, fac) int32_t val; _DOUBLE *fac; {
	fac->h = (fac->h & ~FP_EXP) | (((val + FP_BIAS) & FP_M_EXP) << FP_V_EXP);
	if ((val > 0177) && (val <= 0177600)) {
	    if (val < 0100000) {
		if (fpnotrap (FEC_OVFLO)) *fac = zero_fac;
		return FPS_V;  }
	    if (fpnotrap (FEC_UNFLO)) *fac = zero_fac;  }
	return 0;
}

static int32_t moveifp11(val) _DOUBLE *val; {
	return (GET_EXP (val->h) - FP_BIAS) & 0177777;
}

static int32_t moviffp11(val, fac) int32_t val; _DOUBLE *fac; {
	int32_t i;
	int32_t exp, sign;

	fac->l = val;
	fac->h = 0;
	if (fac->l) {
	    if (sign = GET_SIGN_L (fac->l)) fac->l = (fac->l ^ 0xFFFFFFFF) + 1;
	    for (i = 0; GET_SIGN_L (fac->l) == 0; i++) fac->l = fac->l << 1;
	    exp = ((FPS & FPS_L)? FP_BIAS + 32: FP_BIAS + 16) - i;
	    fac->h = (sign << FP_V_SIGN) | (exp << FP_V_EXP) |
		((fac->l >> (31 - FP_V_HB)) & FP_FRACH);
	    fac->l = (fac->l << (FP_V_HB + 1)) & FP_FRACL;
	    if ((FPS & (FPS_D + FPS_T)) == 0) return roundfp11 (fac);  }
	return 0;
}

static int32_t movfifp11(val, dst) _DOUBLE *val; int32_t *dst; {
	int32_t i, qdouble, tolong;
	int32_t exp, sign;
	_DOUBLE fac, fsrc;
	static const uint32_t i_limit[2][2] =
		{ { 0x80000000, 0x80010000 }, { 0x80000000, 0x80000001 } };

	qdouble = FPS & FPS_D;
	sign = GET_SIGN (val->h);			/* get sign, */
	exp = GET_EXP (val->h);				/* exponent, */
	F_LOAD_FRAC (qdouble, (*val), fac);		/* fraction */
	if (FPS & FPS_L) {
	    tolong = 1;
	    i = FP_BIAS + 32;  }
	else {
	    tolong = 0;
	    i = FP_BIAS + 16;  }
	if (exp <= FP_BIAS) *dst = 0;
	else if (exp > i) {
	    *dst = 0;
	    fpnotrap (FEC_ICVT);
	    return FPS_V;  }
	F_RSH_V (fac, FP_V_HB + 1 + i - exp, fsrc);
	if (!tolong) fsrc.l = fsrc.l & ~0177777;
	if (fsrc.l >= i_limit[tolong][sign]) {
	    *dst = 0;
	    fpnotrap (FEC_ICVT);
	    return FPS_V;  }  
	*dst = fsrc.l;
	if (sign) *dst = -*dst;
	return 0;
}

/* Floating point add

   Inputs:
	facp	=	pointer to src1 (output)
	fsrcp	=	pointer to src2
   Outputs:
	ovflo	=	overflow variable
*/

static int32_t addfp11(facp, fsrcp) _DOUBLE *facp; _DOUBLE *fsrcp; {
int32_t facexp, fsrcexp, ediff;
_DOUBLE facfrac, fsrcfrac;

if (F_LT_AP (facp, fsrcp)) {				/* if !fac! < !fsrc! */
	facfrac = *facp;
	*facp = *fsrcp;					/* swap operands */
	*fsrcp = facfrac;  }
facexp = GET_EXP (facp->h);				/* get exponents */
fsrcexp = GET_EXP (fsrcp->h);
if (facexp == 0) {					/* fac = 0? */
	*facp = fsrcexp? *fsrcp: zero_fac;		/* result fsrc or 0 */
	return 0;  }
if (fsrcexp == 0) return 0;				/* fsrc = 0? no op */
ediff = facexp - fsrcexp;				/* exponent diff */
if (ediff >= 60) return 0;				/* too big? no op */
F_GET_FRAC_P (facp, facfrac);				/* get fractions */
F_GET_FRAC_P (fsrcp, fsrcfrac);
F_LSH_GUARD (facfrac);					/* guard fractions */
F_LSH_GUARD (fsrcfrac);
if (GET_SIGN (facp->h) != GET_SIGN (fsrcp->h)) {	/* signs different? */
	if (ediff) { F_RSH_V (fsrcfrac, ediff, fsrcfrac);  } /* sub, shf fsrc */
	F_SUB (fsrcfrac, facfrac, facfrac);		/* sub fsrc from fac */
	if ((facfrac.h | facfrac.l) == 0)  {     	/* result zero? */
	    *facp = zero_fac;				/* no overflow */
	    return 0;  }
	if (ediff <= 1) {				/* big normalize? */
	    if ((facfrac.h & (0x00FFFFFF << FP_GUARD)) == 0) {
		F_LSH_K (facfrac, 24, facfrac);
		facexp = facexp - 24;  }
	    if ((facfrac.h & (0x00FFF000 << FP_GUARD)) == 0) {
		F_LSH_K (facfrac, 12, facfrac);
		facexp = facexp - 12;  }
	    if ((facfrac.h & (0x00FC0000 << FP_GUARD)) == 0) {
		F_LSH_K (facfrac, 6, facfrac);
		facexp = facexp - 6;  }  }
	while (GET_BIT (facfrac.h, FP_V_HB + FP_GUARD) == 0) {
	    F_LSH_1 (facfrac);
	    facexp = facexp - 1;  }  }
else {	if (ediff) { F_RSH_V (fsrcfrac, ediff, fsrcfrac);  } /* add, shf fsrc */
	F_ADD (fsrcfrac, facfrac, facfrac);		/* add fsrc to fac */
	if (GET_BIT (facfrac.h, FP_V_HB + FP_GUARD + 1)) {
	    F_RSH_1 (facfrac);				/* carry out, shift */
	    facexp = facexp + 1;  }  }
return round_and_pack (facp, facexp, &facfrac, 1);
}

/* Floating point multiply

   Inputs:
	facp	=	pointer to src1 (output)
	fsrcp	=	pointer to src2
   Outputs:
	ovflo	=	overflow indicator
*/

static int32_t mulfp11(facp, fsrcp) _DOUBLE *facp; _DOUBLE *fsrcp; {
int32_t facexp, fsrcexp;
_DOUBLE facfrac, fsrcfrac;

facexp = GET_EXP (facp->h);				/* get exponents */
fsrcexp = GET_EXP (fsrcp->h);
if ((facexp == 0) || (fsrcexp == 0)) {			/* test for zero */
	*facp = zero_fac;
	return 0;  }
F_GET_FRAC_P (facp, facfrac);				/* get fractions */
F_GET_FRAC_P (fsrcp, fsrcfrac);
facexp = facexp + fsrcexp - FP_BIAS;			/* calculate exp */
facp->h = facp->h  ^ fsrcp->h;				/* calculate sign */
frac_mulfp11 (&facfrac, &fsrcfrac);			/* multiply fracs */

/* Multiplying two numbers in the range [.5,1) produces a result in the
   range [.25,1).  Therefore, at most one bit of normalization is required
   to bring the result back to the range [.5,1).
*/

if (GET_BIT (facfrac.h, FP_V_HB + FP_GUARD) == 0) {
	F_LSH_1 (facfrac);
	facexp = facexp - 1;  }
return round_and_pack (facp, facexp, &facfrac, 1);
}

/* Fraction multiply

   Inputs:
	f1p	=	pointer to multiplier (output)
	f2p	=	pointer to multiplicand fraction

   Note: the inputs are unguarded; the output is guarded.

   This routine performs a classic shift-and-add multiply.  The low
   order bit of the multiplier is tested; if 1, the multiplicand is
   added into the high part of the double precision result.  The
   result and the multiplier are both shifted right 1.

   For the 24b x 24b case, this routine develops 48b of result.
   For the 56b x 56b case, this routine only develops the top 64b
   of the the result.  Because the inputs are normalized fractions,
   the interesting part of the result is the high 56+guard bits.
   Everything shifted off to the right, beyond 64b, plays no part
   in rounding or the result.

   There are many possible optimizations in this routine: scanning
   for groups of zeroes, particularly in the 56b x 56b case; using
   "extended multiply" capability if available in the hardware.
*/

static void frac_mulfp11(f1p, f2p) _DOUBLE *f1p; _DOUBLE *f2p; {
_DOUBLE result, mpy, mpc;
int32_t i;

result = zero_fac;					/* clear result */
mpy = *f1p;						/* get operands */
mpc = *f2p;
F_LSH_GUARD (mpc);					/* guard multipicand */
if ((mpy.l | mpc.l) == 0) {				/* 24b x 24b? */
	for (i = 0; i < 24; i++) {
	    if (mpy.h & 1) result.h = result.h + mpc.h;
	    F_RSH_1 (result);
	    mpy.h = mpy.h >> 1;  }  }
else {	if (mpy.l != 0) {				/* 24b x 56b? */
	    for (i = 0; i < 32; i++) {
		if (mpy.l & 1) { F_ADD (mpc, result, result);  }
		F_RSH_1 (result);
		mpy.l = mpy.l >> 1;  }  }
	for (i = 0; i < 24; i++) {
	    if (mpy.h & 1) { F_ADD (mpc, result, result);  }
	    F_RSH_1 (result);
	    mpy.h = mpy.h >> 1;  }  }
*f1p = result;
return;
}

/* Floating point divide

   Inputs:
	facp	=	pointer to dividend (output)
	fsrcp	=	pointer to divisor
   Outputs:
	ovflo	=	overflow indicator

   Source operand must be checked for zero by caller!
*/

static int32_t divfp11(facp, fsrcp) _DOUBLE *facp; _DOUBLE *fsrcp; {
int32_t facexp, fsrcexp, i, count, qd;
_DOUBLE facfrac, fsrcfrac, quo;

fsrcexp = GET_EXP (fsrcp->h);				/* get divisor exp */
facexp = GET_EXP (facp->h);				/* get dividend exp */
if (facexp == 0) {					/* test for zero */
	*facp = zero_fac;				/* result zero */
	return 0;  }
F_GET_FRAC_P (facp, facfrac);				/* get fractions */
F_GET_FRAC_P (fsrcp, fsrcfrac);
F_LSH_GUARD (facfrac);					/* guard fractions */
F_LSH_GUARD (fsrcfrac);
facexp = facexp - fsrcexp + FP_BIAS + 1;		/* calculate exp */
facp->h = facp->h ^ fsrcp->h;				/* calculate sign */
qd = FPS & FPS_D;
count = FP_V_HB + FP_GUARD + (qd? 33: 1);		/* count = 56b/24b */

quo = zero_fac;
for (i = count; (i > 0) && ((facfrac.h | facfrac.l) != 0); i--) {
	F_LSH_1 (quo);					/* shift quotient */
	if (!F_LT (facfrac, fsrcfrac)) {		/* divd >= divr? */
	    F_SUB (fsrcfrac, facfrac, facfrac);		/* divd - divr */
	    if (qd) quo.l = quo.l | 1;			/* double or single? */
	    else quo.h = quo.h | 1;  }
	F_LSH_1 (facfrac);  }				/* shift divd */
if (i > 0) { F_LSH_V (quo, i, quo);  }			/* early exit? */

/* Dividing two numbers in the range [.5,1) produces a result in the
   range [.5,2).  Therefore, at most one bit of normalization is required
   to bring the result back to the range [.5,1).  The choice of counts
   and quotient bit positions makes this work correctly.
*/

if (GET_BIT (quo.h, FP_V_HB + FP_GUARD) == 0) {
	F_LSH_1 (quo);
	facexp = facexp - 1;  }
return round_and_pack (facp, facexp, &quo, 1);
}

/* Round (in place) floating point number to f_floating

   Inputs:
	fptr	=	pointer to floating number
   Outputs:
	ovflow	=	overflow
*/

static int32_t roundfp11(fptr) _DOUBLE *fptr; {
_DOUBLE outf;

outf = *fptr;						/* get argument */
F_ADD (fround_fac, outf, outf);				/* round */
if (GET_SIGN (outf.h ^ fptr->h)) {			/* flipped sign? */
	outf.h = (outf.h ^ FP_SIGN) & 0xFFFFFFFF;	/* restore sign */
	if (fpnotrap (FEC_OVFLO)) *fptr = zero_fac; 	/* if no int, clear */
	else *fptr = outf;				/* return rounded */
	return FPS_V;  }				/* overflow */
else {	*fptr = outf;					/* round was ok */
	return 0;  }					/* no overflow */
}

/* Round result of calculation, test overflow, pack

   Input:
	facp	=	pointer to result, sign in place
	exp	=	result exponent, right justified
	fracp	=	pointer to result fraction, right justified with
			guard bits
	r	=	round (1) or truncate (0)
   Outputs:
	ovflo	=	overflow indicator
*/

static int32_t round_and_pack(facp, exp, fracp, r) _DOUBLE *facp; int32_t exp; _DOUBLE *fracp; int r; {
_DOUBLE frac;

frac = *fracp;						/* get fraction */
if (r && ((FPS & FPS_T) == 0)) { 
	if (FPS & FPS_D) { F_ADD (dround_guard_fac, frac, frac);  }
	else { F_ADD (fround_guard_fac, frac, frac);  }
	if (GET_BIT (frac.h, FP_V_HB + FP_GUARD + 1)) {
	    F_RSH_1 (frac);
	    exp = exp + 1;  }  }
F_RSH_GUARD (frac);
facp->l = frac.l & FP_FRACL;
facp->h = (facp->h & FP_SIGN) | ((exp & FP_M_EXP) << FP_V_EXP) |
	(frac.h & FP_FRACH);
if (exp > 0377) {
	if (fpnotrap (FEC_OVFLO)) *facp = zero_fac;
	return FPS_V;  }
if ((exp <= 0) && (fpnotrap (FEC_UNFLO))) *facp = zero_fac;
return 0;
}

/* Process floating point exception

   Inputs:
	code	=	exception code
   Outputs:
	int	=	FALSE if interrupt enabled, TRUE if disabled
*/

static int32_t fpnotrap(code) int32_t code; {
static const int32_t test_code[] = { 0, 0, 0, FPS_IC, FPS_IV, FPS_IU, FPS_IUV };

if ((code >= FEC_ICVT) && (code <= FEC_UNDFV) &&
    ((FPS & test_code[code >> 1]) == 0)) return TRUE;
FPS = FPS | FPS_ER;
FEC = code;
/*FEA = (backup_PC - 2) & 0177777;*/
/*if ((FPS & FPS_ID) == 0) setTRAP (TRAP_FPE);*/
return FALSE;
}
#endif
