#ifndef pdp11
/*
 * Copyright (c) 1987 Regents of the University of California.
 * All rights reserved.  The Berkeley software License Agreement
 * specifies the terms and conditions for redistribution.
 */

#if defined(LIBC_SCCS) && !defined(lint)
static char sccsid[] = "@(#)atof.c	2.2 (Berkeley) 1/22/87";
#endif

/*
 *	C library - ascii to floating
 */

#include <ctype.h>
#include "c1.h"

/*#define HUGE 1.701411733192644270e38*/
#define LOGHUGE 39

_DOUBLE fp_atof(p) register char *p; {
	register int c;
	_DOUBLE fl, flexp, exp5;
	_DOUBLE big = { 0, 056200 << 16 }; /*= 72057594037927936.;*/  /*2^56*/
	_DOUBLE ten = { 0, 041040 << 16 }; /*= 10.;*/
	/*_DOUBLE fp_ldexp();*/
	int nd;
	register int eexp, exp, neg, negexp, bexp;

	neg = 1;
	while((c = *p++) == ' ')
		;
	if (c == '-')
		neg = -1;
	else if (c=='+')
		;
	else
		--p;

	exp = 0;
	/*fl = 0;*/ fl.l = 0; fl.h = 0;
	nd = 0;
	while ((c = *p++), isdigit(c)) {
		if (fp_lt(fl, big))
			fl = fp_add(fp_mul(ten, fl), fp_int_to_double(c-'0'));
		else
			exp++;
		nd++;
	}

	if (c == '.') {
		while ((c = *p++), isdigit(c)) {
			if (fp_lt(fl, big)) {
				fl = fp_add(fp_mul(ten, fl), fp_int_to_double(c-'0'));
				exp--;
			}
		nd++;
		}
	}

	negexp = 1;
	eexp = 0;
	if ((c == 'E') || (c == 'e')) {
		if ((c= *p++) == '+')
			;
		else if (c=='-')
			negexp = -1;
		else
			--p;

		while ((c = *p++), isdigit(c)) {
			eexp = 10*eexp+(c-'0');
		}
		if (negexp<0)
			eexp = -eexp;
		exp = exp + eexp;
	}

	negexp = 1;
	if (exp<0) {
		negexp = -1;
		exp = -exp;
	}


	if((nd+exp*negexp) < -LOGHUGE){
		/*fl = 0;*/ fl.l = 0; fl.h = 0;
		exp = 0;
	}
	/*flexp = 1;*/ flexp.l = 0; flexp.h = 040200 << 16;
	/*exp5 = 5;*/ exp5.l = 0; exp5.h = 040640 << 16;
	bexp = exp;
	for (;;) {
		if (exp&01)
			flexp = fp_mul(flexp, exp5);
		exp >>= 1;
		if (exp==0)
			break;
		exp5 = fp_mul(exp5, exp5);
	}
	if (negexp<0)
		fl = fp_div(fl, flexp);
	else
		fl = fp_mul(fl, flexp);
	fl = fp_ldexp(fl, negexp*bexp);
	if (neg<0)
		fl = fp_neg(fl);
	return(fl);
}
#endif
