/*
 * C code generator header
 */

#ifndef _C1_H
#define _C1_H 1

#include <stdio.h>
#include <setjmp.h>

#ifdef pdp11
typedef int _INT;
typedef long _LONG;
typedef unsigned int _UNSIGNED_INT;
typedef unsigned long _UNSIGNED_LONG;
typedef float _FLOAT;
typedef double _DOUBLE;
#else
#include <stdint.h>
typedef int16_t _INT;
typedef int32_t _LONG;
typedef uint16_t _UNSIGNED_INT;
typedef uint32_t _UNSIGNED_LONG;
typedef struct { uint32_t h; } _FLOAT;
typedef struct { uint32_t l; uint32_t h; } _DOUBLE;
#endif

#ifndef NULL
#define	NULL	0
#endif
#define	TNULL	(union tree *)NULL
#define	UNS(x)	((unsigned short)(x))

/*
 *  Tree node for unary and binary
 */
struct	tnode {
	int	op;
	int	type;
	int	degree;
	union	tree *tr1;
	union	tree *tr2;
};

/*
 * tree names for locals
 */
struct	tname {
	int	op;
	int	type;
	char	class;
	char	regno;
	int	offset;
	int	nloc;
};

/*
 * tree names for externals
 */
struct	xtname {
	int	op;
	int	type;
	char	class;
	char	regno;
	int	offset;
	char	*name;
};

/*
 * short constants
 */
struct	tconst {
	int	op;
	int	type;
	_INT	value;
};

/*
 * long constants
 */
struct	lconst {
	int	op;
	int	type;
	_LONG	lvalue;
};

/*
 * Floating constants
 */
struct	ftconst {
	int	op;
	int	type;
	_INT	value;
	_DOUBLE	fvalue;
};

/*
 * Node used for field assignments
 */
struct	fasgn {
	int	op;
	int	type;
	int	degree;
	union	tree *tr1;
	union	tree *tr2;
	int	mask;
};

union	tree {
	struct	tnode t;
	struct tname n;
	struct	xtname x;
	struct	tconst c;
	struct	lconst l;
	struct	ftconst f;
	struct	fasgn F;
};

struct	optab {
	char	tabdeg1;
	char	tabtyp1;
	char	tabdeg2;
	char	tabtyp2;
	char	*tabstring;
};

struct	table {
	int	tabop;
	struct	optab *tabp;
};

struct	instab {
	int	iop;
	char	*str1;
	char	*str2;
};

struct	swtab {
	int	swlab;
	int	swval;
};

extern char maprel[];
extern char notrel[];
extern int nreg;
extern int isn;
extern int line;
extern int nerror;			/* number of hard errors */
extern struct table cctab[];
extern struct table efftab[];
extern struct table regtab[];
extern struct table sptab[];
extern struct table lsptab[1];
extern char mov[];
extern char clr[];
extern char cmp[];
extern char tst[];
extern char add[];
extern char sub[];
extern char inc[];
extern char dec[];
extern char mul[];
extern char _div[];
extern char asr[];
extern char ash[];
extern char asl[];
extern char bic[];
extern char bic1[];
extern char bit[];
extern char bit1[];
extern char bis[];
extern char bis1[];
extern char xor[];
extern char neg[];
extern char com[];
extern char stdol[];
extern char ashc[];
extern char slmul[];
extern char sldiv[];
extern char slrem[];
extern char uldiv[];
extern char ulrem[];
extern char ualdiv[];
extern char ualrem[];
extern char ultof[];
extern char ulsh[];
extern char ualsh[];
extern char almul[];
extern char aldiv[];
extern char alrem[];
extern char udiv[];
extern char urem[];
extern char jeq[];
extern char jne[];
extern char jle[];
extern char jgt[];
extern char jlt[];
extern char jge[];
extern char jlos[];
extern char jhi[];
extern char jlo[];
extern char jhis[];
extern char nop[];
extern char jbr[];
extern char jpl[];
extern char jmi[];
extern char jmijne[];
extern char jmijeq[];
extern struct instab instab[];
extern struct instab branchtab[];
extern int opdope[];
extern char *opntab[];
extern int nstack;
extern int nfloat;
extern struct tname sfuncr;
extern char *funcbase;
extern char *curbase;
extern char *coremax;
extern struct tconst czero, cone;
extern long totspace;
extern int regpanic;		/* set when SU register alg. fails */
extern int panicposs;		/* set when there might be need for regpanic */
extern jmp_buf jmpbuf;
extern long ftell();
extern char *sbrk();
extern struct optab *match();
extern union tree *optim();
extern union tree *unoptim();
extern union tree *pow2();
extern union tree *tnode();
extern union tree *sdelay();
extern union tree *ncopy();
extern union tree *getblk();
extern union tree *strfunc();
extern union tree *isconstant();
extern union tree *tconst();
extern union tree *hardlongs();
extern union tree *lconst();
extern union tree *acommute();
extern union tree *lvfield();
extern union tree *paint();
extern long ftell();

/*
 * Some special stuff for long comparisons
 */
extern int xlab1, xlab2, xop, xzero;

/*
	operators
*/
#define	EOFC	0
#define	SEMI	1
#define	LBRACE	2
#define	RBRACE	3
#define	LBRACK	4
#define	RBRACK	5
#define	LPARN	6
#define	RPARN	7
#define	COLON	8
#define	COMMA	9
#define	FSEL	10
#define	FSELR	11
#define	FSELT	12
#define	FSELA	16
#define	ULSH	17
#define	ASULSH	18

#define	KEYW	19
#define	NAME	20
#define	CON	21
#define	STRING	22
#define	FCON	23
#define	SFCON	24
#define	LCON	25
#define	SLCON	26

#define	AUTOI	27
#define	AUTOD	28
#define	NULLOP	218
#define	INCBEF	30
#define	DECBEF	31
#define	INCAFT	32
#define	DECAFT	33
#define	EXCLA	34
#define	AMPER	35
#define	STAR	36
#define	NEG	37
#define	COMPL	38

#define	DOT	39
#define	PLUS	40
#define	MINUS	41
#define	TIMES	42
#define	DIVIDE	43
#define	MOD	44
#define	RSHIFT	45
#define	LSHIFT	46
#define	AND	47
#define	ANDN	55
#define	OR	48
#define	EXOR	49
#define	ARROW	50
#define	ITOF	51
#define	FTOI	52
#define	LOGAND	53
#define	LOGOR	54
#define	FTOL	56
#define	LTOF	57
#define	ITOL	58
#define	LTOI	59
#define	ITOP	13
#define	PTOI	14
#define	LTOP	15

#define	EQUAL	60
#define	NEQUAL	61
#define	LESSEQ	62
#define	LESS	63
#define	GREATEQ	64
#define	GREAT	65
#define	LESSEQP	66
#define	LESSP	67
#define	GREATQP	68
#define	GREATP	69

#define	ASPLUS	70
#define	ASMINUS	71
#define	ASTIMES	72
#define	ASDIV	73
#define	ASMOD	74
#define	ASRSH	75
#define	ASLSH	76
#define	ASAND	77
#define	ASOR	78
#define	ASXOR	79
#define	ASSIGN	80
#define	TAND	81
#define	LTIMES	82
#define	LDIV	83
#define	LMOD	84
#define	ASANDN	85
#define	LASTIMES 86
#define	LASDIV	87
#define	LASMOD	88

#define	QUEST	90
/* #define	MAX	93	not used; wanted macros in param.h */
#define	MAXP	94
/* #define	MIN	95	not used; wanted macros in param.h */
#define	MINP	96
#define	LLSHIFT	91
#define	ASLSHL	92
#define	SEQNC	97
#define	CALL1	98
#define	CALL2	99
#define	CALL	100
#define	MCALL	101
#define	JUMP	102
#define	CBRANCH	103
#define	INIT	104
#define	SETREG	105
#define	LOAD	106
#define	PTOI1	107
#define	ITOC	109
#define	RFORCE	110

/*
 * Intermediate code operators
 */
#define	BRANCH	111
#define	LABEL	112
#define	NLABEL	113
#define	RLABEL	114
#define	STRASG	115
#define	STRSET	116
#define	UDIV	117
#define	UMOD	118
#define	ASUDIV	119
#define	ASUMOD	120
#define	ULTIMES	121	/* present for symmetry */
#define	ULDIV	122
#define	ULMOD	123
#define	ULASTIMES 124	/* present for symmetry */
#define	ULASDIV	125
#define	ULASMOD	126
#define	ULTOF	127
#define	ULLSHIFT 128	/* << for unsigned long */
#define	UASLSHL	129	/* <<= for unsigned long */

#define	BDATA	200
#define	PROG	202
#define	DATA	203
#define	BSS	204
#define	CSPACE	205
#define	SSPACE	206
#define	SYMDEF	207
#define	SAVE	208
#define	RETRN	209
#define	EVEN	210
#define	PROFIL	212
#define	SWIT	213
#define	EXPR	214
#define	SNAME	215
#define	RNAME	216
#define	ANAME	217
#define	SETSTK	219
#define	SINIT	220
#define	GLOBAL	221
#define	C3BRANCH	222
#define	ASSEM	223

/*
 *	types
 */
#define	INT	0
#define	CHAR	1
#define	FLOAT	2
#define	DOUBLE	3
#define	STRUCT	4
#define	RSTRUCT	5
#define	LONG	6
#define	UNSIGN	7
#define	UNCHAR	8
#define	UNLONG	9
#define	VOID	10

#define	TYLEN	2
#define	TYPE	017
#define	XTYPE	(03<<4)
#define	PTR	020
#define	FUNC	040
#define	ARRAY	060

/*
	storage	classes
*/
#define	KEYWC	1
#define	MOS	10
#define	AUTO	11
#define	EXTERN	12
#define	STATIC	13
#define	REG	14
#define	STRTAG	15
#define	ARG	16
#define	OFFS	20
#define	XOFFS	21
#define	SOFFS	22

/*
	Flag	bits
*/

#define	BINARY	01
#define	LVALUE	02
#define	RELAT	04
#define	ASSGOP	010
#define	LWORD	020
#define	RWORD	040
#define	COMMUTE	0100
#define	RASSOC	0200
#define	LEAF	0400
#define	CNVRT	01000

#if 1 /* moved here from c12.c */
#define	LSTSIZ	20
struct acl {
	int nextl;
	int nextn;
	union tree *nlist[LSTSIZ];
	union tree *llist[LSTSIZ+1];
};
#endif

#ifndef __P
#ifdef __STDC__
#define __P(args) args
#else
#define __P(args) ()
#endif
#endif

/* c10.c */
int main __P((int argc, char *argv[]));
struct optab *match __P((union tree *tree, struct table *table, int nrleft, int nocvt));
int rcexpr __P((union tree *atree, struct table *atable, int reg));
int cexpr __P((register union tree *tree, struct table *table, int areg));
int reorder __P((union tree **treep, struct table *table, int reg));
int sreorder __P((union tree **treep, struct table *table, int reg, int recurf));
int delay __P((union tree **treep, struct table *table, int reg));
union tree *sdelay __P((union tree **ap));
union tree *paint __P((register union tree *tp, register int type));
union tree *ncopy __P((register union tree *p));
int chkleaf __P((register union tree *tree, struct table *table, int reg));
int comarg __P((register union tree *tree, int *flagp));
union tree *strfunc __P((register union tree *tp));
void doinit __P((register int type, register union tree *tree));
void movreg __P((int r0, int r1, union tree *tree));

/* c11.c */
int degree __P((register union tree *t));
void pname __P((register union tree *p, int flag));
void pbase __P((register union tree *p));
int xdcalc __P((register union tree *p, int nrleft));
int dcalc __P((register union tree *p, int nrleft));
int notcompat __P((register union tree *p, int ast, int deg, int op));
int prins __P((int op, int c, struct instab *itable, int lbl));
int collcon __P((register union tree *p));
int isfloat __P((register union tree *t));
int oddreg __P((register union tree *t, register int reg));
int arlength __P((int t));
void pswitch __P((struct swtab *afp, struct swtab *alp, int deflab));
void breq __P((int v, int l));
int sort __P((struct swtab *afp, struct swtab *alp));
int ispow2 __P((register union tree *tree));
union tree *pow2 __P((register union tree *tree));
void cbranch __P((union tree *atree, register int lbl, int cond, register int reg));
void branch __P((int lbl, int aop, int c));
void longrel __P((union tree *atree, int lbl, int cond, int reg));
int xlongrel __P((int f));
void label __P((int l));
void popstk __P((int a));
void werror __P((char *s));
void error __P((char *s, ...));
void psoct __P((int an));
void getree __P((void));
int geti __P((void));
void strasg __P((union tree *atp));
int decref __P((register int t));
int incref __P((register int t));

/* c12.c */
union tree *optim __P((register union tree *tree));
union tree *unoptim __P((register union tree *tree));
union tree *lvfield __P((register union tree *t));
union tree *acommute __P((register union tree *tree));
int sideeffects __P((register union tree *tp));
void distrib __P((struct acl *list));
void squash __P((union tree **p, union tree **maxp));
void _const __P((int op, register _INT *vp, _INT v, int type));
union tree *lconst __P((int op, register union tree *lp, register union tree *rp));
void insert __P((int op, register union tree *tree, register struct acl *list));
union tree *tnode __P((int op, int type, union tree *tr1, union tree *tr2));
union tree *tconst __P((int val, int type));
union tree *getblk __P((int size));
int islong __P((int t));
union tree *isconstant __P((register union tree *t));
union tree *hardlongs __P((register union tree *t));
int uns __P((union tree *tp));

#ifndef pdp11
/* fp.c */
int fp_tst __P((_DOUBLE val));
_DOUBLE fp_abs __P((_DOUBLE val));
_DOUBLE fp_neg __P((_DOUBLE val));
int fp_le __P((_DOUBLE val0, _DOUBLE val1));
int fp_ge __P((_DOUBLE val0, _DOUBLE val1));
int fp_gt __P((_DOUBLE val0, _DOUBLE val1));
int fp_lt __P((_DOUBLE val0, _DOUBLE val1));
_INT fp_double_to_int __P((_DOUBLE val));
_LONG fp_double_to_long __P((_DOUBLE val));
_FLOAT fp_double_to_float __P((_DOUBLE val));
_DOUBLE fp_int_to_double __P((_INT val));
_DOUBLE fp_long_to_double __P((_LONG val));
_DOUBLE fp_float_to_double __P((_FLOAT val));
_DOUBLE fp_add __P((_DOUBLE val0, _DOUBLE val1));
_DOUBLE fp_sub __P((_DOUBLE val0, _DOUBLE val1));
_DOUBLE fp_mul __P((_DOUBLE val0, _DOUBLE val1));
_DOUBLE fp_div __P((_DOUBLE val0, _DOUBLE val1));
_DOUBLE fp_ldexp __P((_DOUBLE val, _INT exp));

/* fp_atof.c */
_DOUBLE fp_atof __P((register char *p));
#endif

#endif
