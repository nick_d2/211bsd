#ifndef _CPP_H
#define _CPP_H 1

#ifdef FLEXNAMES
#define	NCPS	128
#else
#define	NCPS	8
#endif

#include <stdio.h>
#include "krcompat.h"

#define STDIN 0
#define STDOUT 1
#define STDERR 2
#define READ 0
#define WRITE 1
#define SALT '#'

extern char *pbeg,*pbuf,*pend;
extern char *outp,*inp;
extern char *newp;
extern char cinit;

/* some code depends on whether characters are sign or zero extended */
/*	#if '\377' < 0		not used here, old cpp doesn't understand */
#if 1 /* signed char is de facto standard now, prev: pdp11 | vax | mc68000 */
#define COFF 128
#else
#define COFF 0
#endif

# if gcos
#define ALFSIZ 512	/* alphabet size */
# else
#define ALFSIZ 256	/* alphabet size */
# endif
extern char macbit[ALFSIZ+11];
extern char toktyp[ALFSIZ];
#define BLANK 1
#define IDENT 2
#define NUMBR 3

/* a superimposed code is used to reduce the number of calls to the
 * symbol table lookup routine.  (if the kth character of an identifier
 * is 'a' and there are no macro names whose kth character is 'a'
 * then the identifier cannot be a macro name, hence there is no need
 * to look in the symbol table.)  'scw1' enables the test based on
 * single characters and their position in the identifier.  'scw2'
 * enables the test based on adjacent pairs of characters and their
 * position in the identifier.  scw1 typically costs 1 indexed fetch,
 * an AND, and a jump per character of identifier, until the identifier
 * is known as a non-macro name or until the end of the identifier.
 * scw1 is inexpensive.  scw2 typically costs 4 indexed fetches,
 * an add, an AND, and a jump per character of identifier, but it is also
 * slightly more effective at reducing symbol table searches.
 * scw2 usually costs too much because the symbol table search is
 * usually short; but if symbol table search should become expensive,
 * the code is here.
 * using both scw1 and scw2 is of dubious value.
 */
#define scw1 1
#define scw2 0

#if scw2
extern char t21[ALFSIZ],t22[ALFSIZ],t23[ALFSIZ+NCPS];
#endif

#if scw1
#define b0 1
#define b1 2
#define b2 4
#define b3 8
#define b4 16
#define b5 32
#define b6 64
#define b7 128
#endif

#define IB 1
#define SB 2
#define NB 4
#define CB 8
#define QB 16
#define WB 32
extern char fastab[ALFSIZ];
extern char slotab[ALFSIZ];
extern char *ptrtab;
#define isslo (ptrtab==(slotab+COFF))
#define isid(a)  ((fastab+COFF)[a]&IB)
#define isspc(a) (ptrtab[a]&SB)
#define isnum(a) ((fastab+COFF)[a]&NB)
#define iscom(a) ((fastab+COFF)[a]&CB)
#define isquo(a) ((fastab+COFF)[a]&QB)
#define iswarn(a) ((fastab+COFF)[a]&WB)

#define eob(a) ((a)>=pend)
#define bob(a) (pbeg>=(a))

# define cputc(a,b)	if(!flslvl) putc(a,b)

extern char buffer[NCPS+BUFSIZ+BUFSIZ+NCPS];

#ifdef pdp11
# define SBSIZE ((unsigned)0114130)	/* PDP compiler doesn't like 39024 */
extern short sbff[SBSIZE/2];
# define sbf ((char *)sbff)
#else /* !pdp11 */
# define SBSIZE 60000*(BUFSIZ/512)	/* Nick... std = 12000, wnj aug 1979 */
extern char sbf[SBSIZE];
#endif /* pdp11 */
extern char *savch;

# define DROP 0xFE	/* special character not legal ASCII or EBCDIC */
# define WARN DROP
# define SAME 0
# define MAXINC 10
# define MAXFRE 14	/* max buffers of macro pushback */
# define MAXFRM 31	/* max number of formals/actuals to a macro */

extern int mactop,fretop;
extern char *instack[MAXFRE],*bufstack[MAXFRE],*endbuf[MAXFRE];

extern int plvl;	/* parenthesis level during scan for macro actuals */
extern int maclin;	/* line number of macro call requiring actuals */
extern char *macfil;	/* file name of macro call requiring actuals */
extern char *macnam;	/* name of macro requiring actuals */
extern int maclvl;	/* # calls since last decrease in nesting level */
extern char *macforw;	/* pointer which must be exceeded to decrease nesting level */
extern int macdam;	/* offset to macforw due to buffer shifting */

#if tgp
extern int tgpscan;	/* flag for dump(); */
#endif

extern int passcom;	/* don't delete comments */

struct symtab {
	char	*name;
	char	*value;
};

extern int flslvl;	/* formerly static but now referenced in yylex.c */

void sayline PARAMS((int where));
void dump PARAMS((void));
char *refill PARAMS((register char *p));
char *cotoken PARAMS((register char *p));
char *skipbl PARAMS((register char *p));
char *unfill PARAMS((register char *p));
char *doincl PARAMS((register char *p));
int equfrm PARAMS((register char *a, register char *p1, register char *p2));
char *dodef PARAMS((char *p));
char *control PARAMS((register char *p));
char *savestring PARAMS((register char *start, register char *finish));
struct symtab *stsym PARAMS((register char *s));
struct symtab *ppsym PARAMS((char *s));
void vpperror PARAMS((char *s, va_list ap));
void pperror PARAMS((char *s, ...));
void yyerror PARAMS((char *s, ...));
void ppwarn PARAMS((char *s, ...));
struct symtab *lookup PARAMS((char *namep, int enterf));
struct symtab *slookup PARAMS((register char *p1, register char *p2, int enterf));
char *subst PARAMS((register char *p, struct symtab *sp));
char *trmdir PARAMS((register char *s));
char *strdex PARAMS((char *s, int c));
int yywrap PARAMS((void));
int main PARAMS((int argc, char *argv[]));
int yylex PARAMS((void));
int yyparse PARAMS((void));

#endif
