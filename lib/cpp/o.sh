#!/bin/sh
ROOT="`pwd |sed -e 's/\/lib\/cpp$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
YACC="$ROOT/cross/usr/bin/yacc"
mkdir --parents "$ROOT/stage/lib"
make CC="$CC" YACC="$YACC" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
