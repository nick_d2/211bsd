#!/bin/sh
ROOT="`pwd |sed -e 's/\/lib\/libc\/gen$//'`"
AR="$ROOT/cross/bin/ar"
AS="$ROOT/cross/bin/as"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=/bin/true"
MANROFF="nroff -man"
CPP="$ROOT/cross/lib/cpp"
LD="$ROOT/cross/bin/ld"
LORDER="$ROOT/cross/usr/bin/lorder"
MKDEP="$ROOT/cross/usr/bin/mkdep"
RANLIB="$ROOT/cross/usr/bin/ranlib"
make CC="$HOSTCC" MANROFF="$MANROFF" MKDEP="$MKDEP" depend && \
make AR="$AR" AS="$AS" CC="$HOSTCC" MANROFF="$MANROFF" CPP="$CPP" LD="$LD" LORDER="$LORDER" LOCALE="LC_ALL=C"
