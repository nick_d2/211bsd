#!/bin/sh
ROOT="`pwd |sed -e 's/\/lib\/libc\/pdp$//'`"
mkdir --parents "$ROOT/stage/lib"
#make CC="$ROOT/cross/bin/cc" MKDEP="$ROOT/cross/usr/bin/mkdep" depend
make AR="$ROOT/cross/bin/ar" AS="$ROOT/cross/bin/as" CC="$ROOT/cross/bin/cc" CPP="$ROOT/cross/lib/cpp" LD="$ROOT/cross/bin/ld" LORDER="$ROOT/cross/usr/bin/lorder" RANLIB="$ROOT/cross/usr/bin/ranlib" DESTDIR="$ROOT/stage"
