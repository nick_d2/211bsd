#!/bin/sh -e

ROOT="`pwd`"

AR="$ROOT/cross/bin/ar"
AS="$ROOT/cross/bin/as"
CC="$ROOT/cross/bin/cc"
CHGRP="$ROOT/scripts/chgrp.sh"
CHMOD="$ROOT/scripts/chmod.sh"
CHOWN="$ROOT/scripts/chown.sh"
CPP="$ROOT/cross/lib/cpp"
HOSTCC="cc -I$ROOT/cross/usr/include -L$ROOT/cross/usr/lib -DCROSS -Wall -Wno-char-subscripts -Wno-deprecated-declarations -Wno-format -Wno-maybe-uninitialized -Wno-parentheses -Wno-unused-result"
HOSTLIBCROSS="-lcross"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
LD="$ROOT/cross/bin/ld"
LORDER="$ROOT/cross/usr/bin/lorder"
MKDEP="$ROOT/cross/usr/bin/mkdep"
RANLIB="$ROOT/cross/usr/bin/ranlib"
SIZE="$ROOT/cross/bin/size"
STRCOMPACT="$ROOT/cross/usr/ucb/strcompact"
SYMCOMPACT="$ROOT/cross/usr/ucb/symcompact"
SYMORDER="$ROOT/cross/usr/ucb/symorder"

mkdir --parents "$ROOT/stage/usr/include"
mkdir --parents "$ROOT/stage/usr/lib"

echo
echo "making clean"
echo

make -C sys/GENERIC clean

echo
echo "installing include"
echo

# this is needed before make depend, since "cc -M" accesses it
make -C include CHGRP="$CHGRP" CHMOD="$CHMOD" CHOWN="$CHOWN" INSTALL="$INSTALL" SHARED=copies SYSDIR="$ROOT/sys" DESTDIR="$ROOT/stage" install

echo
echo "making depend"
echo

make -C lib/libkern CC="$CC" MKDEP="$MKDEP" depend
make -C sys/GENERIC I="$ROOT/stage/usr/include" CC="$CC" MKDEP="$MKDEP" STAGEDIR="$ROOT/stage" depend

echo
echo "making libkern"
echo

make -C lib/libkern AR="$AR" AS="$AS" CC="$CC" CPP="$CPP" LD="$LD" LORDER="$LORDER"

echo
echo "install libkern"
echo

make -C lib/libkern RANLIB="$RANLIB" INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install

echo
echo "making kernel"
echo

make -C sys/GENERIC I="$ROOT/stage/usr/include" AS="$AS" CC="$CC" CPP="$CPP" HOSTCC="$HOSTCC" HOSTLIBCROSS="$HOSTLIBCROSS" HOSTSEPFLAG= LD="$LD" SIZE="$SIZE" STRCOMPACT="$STRCOMPACT" SYMCOMPACT="$SYMCOMPACT" SYMORDER="$SYMORDER"

echo
echo "installing kernel"
echo

make -C sys/GENERIC INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
