#!/usr/bin/env python3

import sys

a = sys.stdin.buffer.read()
i = 0
while i < len(a):
  l = int.from_bytes(a[i:i + 4], byteorder = 'little')
  sys.stderr.write('{0:08x} {1:08x} {2:08x}\n'.format(i, len(a), l))
  i += 4
  b = a[i:i + l]
  sys.stdout.buffer.write(b)
  i += l
  assert l == int.from_bytes(a[i:i + 4], byteorder = 'little')
  i += 4
  if l == 0:
    break
