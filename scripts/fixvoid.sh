#!/bin/sh
sed -n \
-e 's/\([^:]*\).*warning: control reaches end of non-void function \[-Wreturn-type\]$/s\/^int \1\/void \1\//p' \
-e 's/\([^:]*\).*warning: ‘return’ with no value, in function returning non-void$/s\/^int \1\/void \1\//p'
