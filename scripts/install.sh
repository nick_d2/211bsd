#!/bin/sh

strip=
stripper=strip
mode=
owner=
group=
while test $# -ge 1
do
  case "$1" in
  -c)
    shift
    ;;
  -s)
    strip=1
    shift
    ;;
  --strip-program=*)
    stripper="`echo $1 |sed -e 's/--strip-program=//'`"
    shift
    ;;
  -m)
    mode="$2"
    shift 2
    ;;
  -o)
    owner="$2"
    shift 2
    ;;
  -g)
    group="$2"
    shift 2
    ;;
  -*)
    echo "invalid option: $1"
    exit 1
    ;;
  *)
    break
    ;;
  esac
done

if test $# -le 1
then
  echo "usage: $0 [-c] [-s] [-m mode] [-o owner] [-g group] files dest"
  exit 1
fi

for dest in "$@"
do
  :
done

if test $# -ge 3 && ! test -d "$dest"
then
  echo "not directory: $dest"
  exit 1
fi

errs=0
if test -d "$dest"
then
  while test $# -ge 2
  do
    if cp $1 $dest
    then
      if test -n "$strip" && ! "$stripper" "$dest/$1"
      then
        errs=1
      fi
      base="`basename $1`"
      if test -n "$mode"
      then
        echo "$mode" >"$dest/$base.MODE"
      else
        rm -f "$dest/$base.MODE"
      fi
      if test -n "$owner"
      then
        echo "$owner" >"$dest/$base.OWNER"
      else
        rm -f "$dest/$base.OWNER"
      fi
      if test -n "$group"
      then
        echo "$group" >"$dest/$base.GROUP"
      else
        rm -f "$dest/$base.GROUP"
      fi
    else
      errs=1
    fi
    shift
  done
else
  if cp $1 $dest
  then
    if test -n "$strip" && ! "$stripper" "$dest"
    then
      errs=1
    fi
    if test -n "$mode"
    then
      echo "$mode" >"$dest.MODE"
    else
      rm -f "$dest.MODE"
    fi
    if test -n "$owner"
    then
      echo "$owner" >"$dest.OWNER"
    else
      rm -f "$dest.OWNER"
    fi
    if test -n "$group"
    then
      echo "$group" >"$dest.GROUP"
    else
      rm -f "$dest.GROUP"
    fi
  else
    errs=1
  fi
fi
exit $errs
