#!/bin/sh
for i in `find . -name Makefile -print`
do
  echo $i
  for j in AR C2 CHGRP CHMOD CHOWN CPP LD LEX LORDER MANROFF MKDEP MKSTR INSTALL HOSTCC RANLIB UNIFDEF SIZE STRCOMPACT SYMCOMPACT SYMORDER XSTR YACC
  do
    if grep -q "\${$j}" $i && ! grep -q "^$j[ 	]*=" $i
    then
      (echo "$j=`echo $j |sed -e 'y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/'`"; cat $i) >a
      mv $i $i.bak
      mv a $i
    fi
  done
done
