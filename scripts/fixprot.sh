#!/bin/sh
cproto $1 |sed -ne '2,$p' >a
sed -e 's/^\(.*\)(\(.*\));$/\1 PARAMS((\2));/' <a >b
sed -e 's/((/(( /' -e 's/,/;/g' -e 's/))/;))/' -e 's/(( void;))/(())/' -e 's/^\(.*[^a-zA-Z_]\)\?\([a-zA-Z_0-9]\+\) PARAMS((\(.*\)));$/s\/^\2(\\(.*\\))\\( *{\\)\\?\/\1\2(\\1)\3 {\//' <b >c
cp $1 $1.bak
sed -f c -i $1
diff --unified $1.bak $1
