#!/usr/bin/env python3

import sys

a = sys.stdin.buffer.read()
i = 0
while i < len(a):
  l = min(0x400, len(a) - i)
  sys.stderr.write('{0:08x} {1:08x} {2:08x}\n'.format(i, len(a), l))
  sys.stdout.buffer.write(l.to_bytes(4, byteorder = 'little'))
  sys.stdout.buffer.write(a[i:i + l])
  i += l
  sys.stdout.buffer.write(l.to_bytes(4, byteorder = 'little'))
l = 0
sys.stdout.buffer.write(l.to_bytes(4, byteorder = 'little'))
sys.stdout.buffer.write(l.to_bytes(4, byteorder = 'little'))
