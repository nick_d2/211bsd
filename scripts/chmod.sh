#!/bin/sh

if test $# -le 1
then
  echo "usage: $0 mode dest"
  exit 1
fi

mode="$1"
shift

errs=0
while test $# -ge 1
do
  if ! test -e "$1"
  then
    echo "does not exist: $1"
    errs=1
  fi
  echo "$mode" >"$1.MODE"
  shift
done
exit $errs
