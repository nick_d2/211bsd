#!/bin/sh
find . -name '*.MODE' -print |while read i
do
  chmod `cat $i` `echo $i |sed -e 's/\.MODE$//'`
done
find . -name '*.OWNER' -print |while read i
do
  chown `cat $i` `echo $i |sed -e 's/\.OWNER$//'`
done
find . -name '*.GROUP' -print |while read i
do
  chgrp `cat $i` `echo $i |sed -e 's/\.GROUP$//'`
done
