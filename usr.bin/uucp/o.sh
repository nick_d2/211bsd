#!/bin/sh
ROOT="`pwd |sed -e 's/\/usr.bin\/uucp$//'`"
AR="$ROOT/cross/bin/ar"
CC="$ROOT/cross/bin/cc"
RANLIB="$ROOT/cross/usr/bin/ranlib"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
mkdir --parents "$ROOT/stage/usr/bin"
make AR="$AR" CC="$CC" RANLIB="$RANLIB" STAGEDIR="$ROOT/stage" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
