#!/bin/sh
ROOT="`pwd |sed -e 's/\/usr\.bin$//'`"
mkdir --parents "$ROOT/cross/usr/bin"
make CC="$ROOT/cross/bin/cc" NM="$ROOT/cross/bin/nm" lorder mkdep && \
install lorder -m 755 "$ROOT/cross/usr/bin"
install mkdep -m 755 "$ROOT/cross/usr/bin"
