#!/bin/sh
ROOT="`pwd |sed -e 's/\/usr.bin\/lex$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
YACC="$ROOT/cross/usr/bin/yacc"
mkdir --parents "$ROOT/stage/usr/bin"
mkdir --parents "$ROOT/stage/usr/libdata/lex"
make CC="$CC" YACC="$YACC" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
