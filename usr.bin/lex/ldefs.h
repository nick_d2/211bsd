/*	ldefs.c	4.1	83/08/11	*/

#ifndef _LDEFS_H
#define _LDEFS_H 1

#include <stdio.h>
#include <stdlib.h>
#include "krcompat.h"

#define cfree(a, b, c) free(a);

#ifdef pdp11
typedef int intptr_t;
#else
#include <stdint.h>
#endif

# define PP 1
# ifdef unix

# define CWIDTH 7
# define CMASK 0177
# define ASCII 1
# endif

# ifdef gcos
# define CWIDTH 9
# define CMASK 0777
# define ASCII 1
# endif

# ifdef ibm
# define CWIDTH 8
# define CMASK 0377
# define EBCDIC 1
# endif

# ifdef ASCII
# define NCH 128
# endif

# ifdef EBCDIC
# define NCH 256
# endif


# define TOKENSIZE 1000
# define DEFSIZE 40
# define DEFCHAR 1000
# define STARTCHAR 100
# define STARTSIZE 256
# define CCLSIZE 1000
# ifdef SMALL
# define TREESIZE 600
# define NTRANS 1500
# define NSTATES 300
# define MAXPOS 1500
# define NOUTPUT 1500
# endif

# ifndef SMALL
# define TREESIZE 1000
# define NSTATES 500
# define MAXPOS 2500
# define NTRANS 2000
# define NOUTPUT 3000
# endif
# define NACTIONS 100
# define ALITTLEEXTRA 30

# define RCCL NCH+90
# define RNCCL NCH+91
# define RSTR NCH+92
# define RSCON NCH+93
# define RNEWE NCH+94
# define FINAL NCH+95
# define RNULLS NCH+96
# define RCAT NCH+97
# define STAR NCH+98
# define PLUS NCH+99
# define QUEST NCH+100
# define DIV NCH+101
# define BAR NCH+102
# define CARAT NCH+103
# define S1FINAL NCH+104
# define S2FINAL NCH+105

# define DEFSECTION 1
# define RULESECTION 2
# define ENDSECTION 5
# define TRUE 1
# define FALSE 0

# define PC 1
# define PS 1

# ifdef DEBUG
# define LINESIZE 110
extern int yydebug;
extern int debug;		/* 1 = on */
extern int charc;
# endif

# ifndef DEBUG
# define freturn(s) s
# endif

extern int sargc;
extern char **sargv;
extern char buf[520];
extern int ratfor;		/* 1 = ratfor, 0 = C */
extern int yyline;		/* line number of file */
extern int sect;
extern int eof;
extern int lgatflg;
extern int divflg;
extern int funcflag;
extern int pflag;
extern int casecount;
extern int chset;	/* 1 = char set modified */
extern FILE *fin, *fout, *fother, *errorf;
extern int fptr;
extern char *ratname, *cname;
extern int prev;	/* previous input character */
extern int pres;	/* present input character */
extern int peek;	/* next input character */
extern int *name;
extern intptr_t *left;
extern intptr_t *right;
extern int *parent;
extern char *nullstr;
extern int tptr;
extern char pushc[TOKENSIZE];
extern char *pushptr;
extern char slist[STARTSIZE];
extern char *slptr;
extern char **def, **subs, *dchar;
extern char **sname, *schar;
extern char *ccl;
extern char *ccptr;
extern char *dp, *sp;
extern int dptr, sptr;
extern char *bptr;		/* store input position */
extern char *tmpstat;
extern int count;
extern int **foll;
extern int *nxtpos;
extern int *positions;
extern int *gotof;
extern int *nexts;
extern char *nchar;
extern int **state;
extern int *sfall;		/* fallback state num */
extern char *cpackflg;		/* true if state has been character packed */
extern int *atable, aptr;
extern int nptr;
extern char symbol[NCH];
extern char cindex[NCH];
extern int xstate;
extern int stnum;
extern int ctable[];
extern int ZCH;
extern int ccount;
extern char match[NCH];
extern char extra[NACTIONS];
extern char *pcptr, *pchar;
extern int pchlen;
extern int nstates, maxpos;
extern int yytop;
extern int report;
extern int ntrans, treesize, outsize;
extern long rcount;
extern int optim;
extern int *verify, *advance, *stoff;
extern int scon;
extern char *psave;

/* header.c */
void phead1 PARAMS((void));
void chd1 PARAMS((void));
void rhd1 PARAMS((void));
void phead2 PARAMS((void));
void chd2 PARAMS((void));
void ptail PARAMS((void));
void ctail PARAMS((void));
void rtail PARAMS((void));
void statistics PARAMS((void));
/* lmain.c */
int main PARAMS((int argc, char **argv));
void get1core PARAMS((void));
void free1core PARAMS((void));
void get2core PARAMS((void));
void free2core PARAMS((void));
void get3core PARAMS((void));
char *myalloc PARAMS((int a, int b));
void yyerror PARAMS((char *s));
/* sub1.c */
char *getl PARAMS((char *p));
int space PARAMS((int ch));
int digit PARAMS((int c));
void error PARAMS((char *s, ...)) NORETURN;
void warning PARAMS((char *s, ...));
int _index PARAMS((int a, char *s));
int alpha PARAMS((int c));
int printable PARAMS((int c));
void lgate PARAMS((void));
void scopy PARAMS((char *s, char *t));
int siconv PARAMS((char *t));
int slength PARAMS((char *s));
int scomp PARAMS((char *x, char *y));
int ctrans PARAMS((char **ss));
void cclinter PARAMS((int sw));
int usescape PARAMS((int c));
int lookup PARAMS((char *s, char **t));
int cpyact PARAMS((void));
int gch PARAMS((void));
int mn2 PARAMS((int a, int d, int c));
int mn1 PARAMS((int a, intptr_t d));
int mn0 PARAMS((int a));
void munput PARAMS((int t, intptr_t p));
int dupl PARAMS((int n));
/* sub2.c */
void cfoll PARAMS((int v));
void add PARAMS((int **array, int n));
void follow PARAMS((int v));
void first PARAMS((int v));
void cgoto PARAMS((void));
void nextstate PARAMS((int s, int c));
int notin PARAMS((int n));
void packtrans PARAMS((int st, char *tch, int *tst, int cnt, int tryit));
int member PARAMS((int d, char *t));
void acompute PARAMS((int s));
void mkmatch PARAMS((void));
void layout PARAMS((void));
void rprint PARAMS((int *a, char *s, int n));
void shiftr PARAMS((int *a, int n));
void upone PARAMS((int *a, int n));
void bprint PARAMS((char *a, char *s, int n));
void padd PARAMS((int **array, int n));
/* y.tab.c */
int yyparse PARAMS((void));
int yylex PARAMS((void));

#endif
