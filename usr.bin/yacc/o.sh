#!/bin/sh
ROOT="`pwd |sed -e 's/\/usr.bin\/yacc$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
mkdir --parents "$ROOT/stage/usr/bin"
mkdir --parents "$ROOT/stage/usr/share/misc"
make CC="$CC" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
