#!/bin/sh
ROOT="`pwd |sed -e 's/\/usr.bin\/lint$//'`"
CC="$ROOT/cross/bin/cc"
INSTALL="$ROOT/scripts/install.sh --strip-program=\"$ROOT/cross/bin/strip\""
MKSTR="$ROOT/cross/usr/ucb/mkstr"
XSTR="$ROOT/cross/usr/ucb/xstr"
YACC="$ROOT/cross/usr/bin/yacc"
mkdir --parents "$ROOT/stage/usr/bin"
mkdir --parents "$ROOT/stage/usr/libexec/lint"
mkdir --parents "$ROOT/stage/usr/share/lint"
make CC="$CC" MKSTR="$MKSTR" XSTR="$XSTR" YACC="$YACC" && \
make INSTALL="$INSTALL" DESTDIR="$ROOT/stage" install
