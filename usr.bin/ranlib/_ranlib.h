#ifndef __RANLIB_H
#define __RANLIB_H 1

#include <stdio.h>

#ifndef __P
#ifdef __STDC__
#define __P(params) params
#else
#define __P(params) ()
#endif
#endif

/* build.c */
int build __P((void));
int sgets __P((char *buf, int n, register FILE *fp));
/* misc.c */
int tmp __P((void));
void *emalloc __P((int len));
char *rname __P((char *path));
void badfmt __P((void));
void error __P((char *name));
/* touch.c */
int touch __P((void));
void settime __P((int afd));
/* ranlib.c */
int main __P((int argc, char **argv));
void usage __P((void));

#endif
