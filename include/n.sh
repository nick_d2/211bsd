#!/bin/sh
ROOT="`pwd |sed -e 's/\/include$//'`"
mkdir --parents "$ROOT/stage/usr/include"
make SHARED=copies DESTDIR="$ROOT/stage" SYSDIR="$ROOT/sys" install
